/*!
 * core.js
 * Objeto container de objetos singleton
 *
 * Joselmo Cardozo
 */

(function() {
    define([
        "libs/jsuri-1.1.1",
        "libs/shortcut",
        "helper/loading"
    ], function(jsuri, shortcut, loading) {
        var singleton = function () {
            return {
                uri: jsuri.parse(document.URL),
                shortcut: shortcut,
                context: _context,
                loading: loading
            }
        };
        return singleton();
    });
})();