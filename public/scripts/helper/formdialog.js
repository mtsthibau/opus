/*!
 * formdialog.js
 * Extende AJS.Dialog para implementar um formulário que pode ser carregado a
 * partir do modal criado pelo Dialog.
 *
 * Joselmo Cardozo
 */

define([
    "helper/core"
], function (core) {
    var
        defaults = {
            width: 540,
            height: 300,
            title: null,
            targetUrl: null,
            redirectTo: null,
            submitText: "Update",
            onUnSuccessfulSubmit: function () {},
            onSuccessfulSubmit: function () {},
            onDialogFinished: function () {},
            submitAjaxOptions: {
                type: "post",
                data: {}
            }
        },
        ajsDialog = null,
        options = {},
        reset = function() {
            ajsDialog = null;
            options = {};
        },
        load = function() {
            AJS.$.ajax({
                type: options.submitAjaxOptions.type,
                url: options.targetUrl,
                data: options.submitAjaxOptions.data,
                error: function() {
                    processError();
                },
                success: function(data, textStatus, obj) {
                    if (ajsDialog.popup.element != null) {
                        processForm(data);
                    }
                }
            });
        },
        processError = function() {
            ajsDialog.addHeader("Falha na Comunicação");

            var div = AJS.$("<div/>").addClass("ajaxerror");

            AJS.$("<div/>").addClass("aui-message error").html('<span class="aui-icon icon-error"></span>'
                + '<p>Falha na comunicação com o servidor. Esta pode ser uma falha temporária ou o servidor '
                + 'pode estar desligado.</p><p>Feche esta caixa de diálogo e pressione atualizar em seu '
                + 'navegador</p>').appendTo(div);

            ajsDialog.addPanel("FormContent", div);
            ajsDialog.addCancel('Fechar', function(ajsDialog) {
                ajsDialog.remove();
            });

            core.loading.hide();
            ajsDialog.show();
            ajsDialog.updateHeight();
        },
        addLoadingIcon = function() {
            var span = AJS.$("<span>...</span>")
                .addClass('aui-icon aui-icon-wait')
                .css('display', 'none')
                .css('margin-right', '14px');
            AJS.$(".dialog-button-panel button").before(span);
        },

        processForm = function(data) {

            ajsDialog.addHeader(options.title);
            ajsDialog.addPanel("FormContent", data);

            if (options.submitText) {
                ajsDialog.addSubmit(options.submitText, function(ajsDialog) {
                    var form = AJS.$(ajsDialog.page[0].body.context).find('form');

                    var ajaxOptions = {
                        beforeSubmit: function() {
                            AJS.$(".dialog-button-panel").find('.aui-icon-wait').show();
                            ajsDialog.disable();
                        },
                        success: function(responseText, statusText, xhr, $form) {
                            if (xhr.status == 200 && typeof responseText == "object" &&
                                responseText.success == true) {
                                if (typeof responseText.redirectTo != "undefined") {
                                    window.location = responseText.redirectTo;
                                }
                                else if (options.redirectTo) {
                                    window.location = options.redirectTo;
                                }
                                else if ($form.find('[name=redirectTo]')) {
                                    window.location = $form.find('[name=redirectTo]').val();
                                }
                                else {
                                    ajsDialog.remove();
                                }
                            }
                            else {
                                $form.parent().html(responseText);
                                AJS.$(".dialog-button-panel").find('.aui-icon-wait').hide();
                                ajsDialog.enable();
                                ajsDialog.updateHeight();
                            }
                        }
                    };

                    AJS.$(form).ajaxForm(ajaxOptions);
                    form.submit();
                });
            }

            ajsDialog.addCancel('Cancelar', function(ajsDialog) {
                ajsDialog.remove();
            });

            addLoadingIcon();
            core.loading.hide();

            ajsDialog.show();
            ajsDialog.updateHeight();
        },

        init = function(settings) {
            core.loading.show();
            reset();
            options = AJS.$.extend({}, defaults, settings);
            ajsDialog = new AJS.Dialog({
                width: options.width,
                height: options.height,
                id: options.id
            });
            load();
        };

    return {
        show: function(settings) {
            init(settings);
        }
    }
});