/*!
 * helper.editablefield.js
 * Cria inputs editáveis
 *
 * Joselmo Cardozo
 */

define([
    'helper/messages'
], function(messages) {
    function EditableField() {

        this.element = null;
        this.default_value = null;
        this.defaults = {
            source: {},
            editor: false,
            _token: AJS.$("meta[name=csrf-token]").attr("content")
        };

        this.init = function(el, settings) {
            this.element = AJS.$(el);
            this.defaults = AJS.$.extend({}, this.defaults, settings);

            this._el().init();
            this._el().start();
        };

        this._define_default_value = function() {
            if (this._is_select()) {
                var _op = this._field().find('option:selected');

                var icon = '';
                if (_op.attr('data-icon')) {
                    icon = "<img src='"+ _op.attr('data-icon') +"' width='16'>";
                }

                this.default_value = AJS.$("<span>"+ icon +" "+ _op.html() +"</span>");

                this.element.attr("data-pk", _op.val());
            }
            else {
                this.default_value = AJS.$.trim(this._field().val());
            }
        };

        this._field = function() {
            return AJS.$(this.element.find('[name='+ this.element.attr('id') +']'));
        };

        this._is_select = function() {
            return (this.element.attr("data-type") == "select");
        };

        this._is_textarea = function() {
            return (this.element.attr("data-type") == "textarea");
        };

        this._form = function()
        {
            var self = this;
            return {
                bind: function() {
                    self.element.find('button[type=cancel]').click(function(e) {
                        e.preventDefault();
                        self._el().start();
                    });

                    self.element.find('button[type=submit]').click(function(e) {
                        e.preventDefault();
                        self._form().save();
                    });

                    self.element.find('form').submit(function(e) {
                        e.preventDefault();
                        self.element.find('button[type=submit]').click();
                    });

                    if (self.defaults.editor) {
                        self.element.find('.redactor-text').redactor({
                            lang: 'pt_br',
                            fixed: true,
                            toolbarFixedBox: true,
                            airButtons: ['formatting', '|', 'bold', 'italic', 'deleted', '|',
                                'unorderedlist', 'orderedlist', 'outdent', 'indent', '|',
                                'image', 'video', 'file', 'table', 'link', '|', '|', 'alignment', '|', 'horizontalrule'],
                            plugins: ['fontcolor'],
                            imageUpload: '/changelog/upload',
                            clipboardUploadUrl: '/changelog/upload'
                        });
                    }
                },

                unbind: function()
                {
                    self.element.find('button').unbind('click');
                },

                save: function()
                {
                    self._el().saving();

                    AJS.$.ajax({
                        type: 'post',
                        url: self.element.attr('data-action'),
                        data: {
                            'field': self._field().attr('name'),
                            'value': self._field().val(),
                            '_token': self.defaults._token
                        },
                        error: function() {
                            messages.error(
                                'Desculpe, houve uma falha na comunicação com o Servidor.',
                                'Feche esta caixa de diálogo e atualize a página. Se problema persistir ' +
                                'contate um administrador'
                            );
                        },
                        success: function(data, textStatus, obj) {
                            self._define_default_value();

                            if (data.success) {
                                self._el().start();
                            }
                            else {
                                self._el().edit();
                            }
                        }
                    });
                }
            };
        };

        this._el = function()
        {
            var self = this;

            return {
                init: function() {
                    self.default_value = AJS.$.trim(self.element.html());
                },

                reset: function() {
                    self._form().unbind();
                    self.element.empty();
                    self.element.removeClass('active');
                    self.element.removeClass('saving');
                    self.element.removeClass('inactive');
                    self.element.removeClass('editable-field');
                    self.element.removeAttr('title');
                },

                start: function()
                {
                    self._el().reset();

                    self.element.addClass('inactive');
                    self.element.addClass('editable-field');
                    self.element.attr('title', 'Clique para editar');

                    self.element.append(self.default_value);
                    self.element.append(self._tpl.edit_icon());

                    self.element.find('.icon-edit-sml').bind('click', function() {
                        self._el().edit();
                    });
                },

                edit: function()
                {
                    self._el().reset();

                    self.element.addClass('active');
                    self.element.addClass('editable-field');

                    var field;
                    if (self.element.attr("data-type") == 'select') {
                        field = AJS.$('<select class="select"></select>');

                        AJS.$.each(self.defaults.source, function(i, option) {
                            var $option = AJS.$("<option>"+ option.name +"</option>")
                                .attr("value", option.id)
                                .attr("data-icon", option.icon);

                            if (option.id == self.element.attr('data-pk')) {
                                $option.attr("selected", "selected");
                            }

                            field.append($option);
                        });
                    }
                    else if (self.element.attr("data-type") == 'textarea') {
                        field = AJS.$('<textarea class="textarea">'+ self.default_value +'</textarea>');

                        if (self.defaults.editor) {
                            field.addClass('redactor-text');
                        }
                    }
                    else {
                        field = AJS.$('<input type="text" value="'+ self.default_value +'" class="text long-field">');
                    }

                    field.attr('name', self.element.attr('id'));

                    var $tpl_ed = AJS.template.load("ef-form-edit").fillHtml({
                        field: field.prop('outerHTML')
                    });

                    self.element.append(AJS.$($tpl_ed.toString()));
                    self.element.find('input').focus();
                    self._form().bind();
                },
                saving: function()
                {
                    self.element.addClass('saving');
                    self.element.find('input, textarea').attr('disabled', 'disabled');
                }
            };
        };

        this._tpl = {
            edit_icon: function() {
                return AJS.$('<span class="overlay-icon icon icon-edit-sml">');
            }
        };
    }

    return {
        'new': function(el, settings) {
            var $obj = new EditableField();
            $obj.init(el, settings);
            return $obj;
        }
    }
});