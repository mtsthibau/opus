/*!
 * dialog.js
 * Extende AJS.Dialog para implementar algumas melhorias, como simples diálogos
 * modais e diálogos que tem seu conteúdo carregado a partir de uma url.
 *
 * Joselmo Cardozo
 */

define(function() {
    var
        defaults = {
            idDialog: null,
            width: 540,
            height: 300,
            title: null,
            prompt: false,
            message: null,
            contentUrl: null
        },
        ajsDialog = null,
        options = {},
        reset = function() {
            ajsDialog = null;
            options = {};
        },
        load = function() {
            AJS.$.ajax({
                type: 'get',
                url: options.contentUrl,
                success: function(data, textStatus, obj) {
                    if (ajsDialog.popup.element != null) {
                        ajsDialog.addPage();

                        ajsDialog.addHeader(options.title);
                        ajsDialog.addPanel("FormContent", data);

                        AJS.$.each(options.buttons, function(i, button) {
                            var buttonType = (button.type === 'link') ? addLink : addButton;
                            buttonType(button.label, button.callback, true);
                        });

                        ajsDialog.gotoPage(1);
                        ajsDialog.updateHeight();
                    }
                }
            });
        },
        init = function(settings) {
            reset();
            options = AJS.$.extend({}, defaults, settings);

            ajsDialog = new AJS.Dialog({
                width: options.width,
                height: options.height,
                id: options.id
            });

            if (options.contentUrl) {
                ajsDialog.addPanel("Loading", "<p style='text-align:center;margin-top:100px;'><img src='/images/ajaxloadingm.gif'></p>");

                ajsDialog.addCancel('Cancelar', function(ajsDialog) {
                    ajsDialog.remove();
                });

                ajsDialog.show();
                load();
            }
            else {
                ajsDialog.addHeader(options.title);
                var message = "<p>"+ options.message +"</p>";

                if (options.prompt) {
                    message += '' +
                        '<form class="aui" onsubmit="javascript:AJS.$(\'button.button-panel-button\').click();return false;"><div class="field-group">' +
                        '<label for="prompt-field">Minutos<span class="aui-icon icon-required"> required</span></label>' +
                        '<input class="text" type="number" min="0" step="1" pattern="\d+" id="prompt-field" maxlength="3" value="0">' +
                        '<span class="error" id="prompt-field-error" style="display: none;">Por favor, digite um número inteiro válido.</span>' +
                        '</div></form>';
                }

                ajsDialog.addPanel(null, message, "panel-body");
            }
        },
        is_int = function(value){
            return (parseFloat(value) == parseInt(value)) && !isNaN(value);
        },
        addButton = function(label, callback, callback_return) {
            ajsDialog.addButton(label, function(ajsDialog) {
                if (options.prompt) {
                    if (is_int(AJS.$("#prompt-field").val()) && AJS.$("#prompt-field").val() >= 0) {
                        callback_return = AJS.$("#prompt-field").val();
                    }
                    else {
                        AJS.$("#prompt-field-error").show();
                        ajsDialog.updateHeight();
                        return false;
                    }
                }
                ajsDialog.remove();
                if (typeof callback === "function") {
                    callback(callback_return, ajsDialog);
                }
            });
        },
        addLink = function(label, callback, callback_return) {
            ajsDialog.addLink(label, function(ajsDialog) {
                ajsDialog.remove();
                if (typeof callback === "function") {
                    callback(callback_return, ajsDialog);
                }
            });
        };

    return {
        alert: function(title, message, callback) {
            init({title: title, message: message});
            addButton("Ok", callback, true);
            ajsDialog.show();
            ajsDialog.updateHeight();
        },
        confirm: function(title, message, callback) {
            init({title: title, message: message});
            addButton("Continuar", callback, true);
            addLink("Cancelar", callback, false);
            ajsDialog.show();
            ajsDialog.updateHeight();
        },
        prompt: function(title, message, callback) {
            init({title: title, message: message, prompt: true});
            addButton("Continuar", callback, true);
            addLink("Cancelar", callback, false);
            ajsDialog.show();
            ajsDialog.updateHeight();
        },
        dialog: function(settings) {
            init(settings);
            AJS.$.each(settings.buttons, function(i, button) {
                var buttonType = (button.type === 'link') ? addLink : addButton;
                buttonType(button.label, button.callback, true);
            });
            ajsDialog.show();
            ajsDialog.updateHeight();
        },
        remote: function(settings) {
            init(settings);
        }
    }
});