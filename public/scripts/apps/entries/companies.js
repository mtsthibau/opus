/*!
 *Mascara para campos do formulário
 *Matheus Thibau
 */

$(document).ready(function(){
       $("#cnpj").mask("99.999.999/9999-99");
       $("#cep").mask("99999-999");
       $("#telefone").mask("(99)9999-9999");
       $("#celular").mask("(99)9999-9999");
       $("#cpf").mask("999999999-99");
       $("#data").mask("99/99/9999");
});
