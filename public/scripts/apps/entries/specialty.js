/*!
 * backend.users.js
 * Funções da página de usuários no backend
 *
 * Joselmo Cardozo
 */
(function() {
    require([], function() {
    
        
        $('[name=btn-del-specialty]').click(function(){
            var nome = $(this).attr('data-nome');

            var msg = 'Tem certeza que gostaria de remover a especialidade ' + nome + '?';
            if (confirm(msg)){
                window.location = $(this).attr('data-url');
            }
        });
    
        var check_errors = function(errors) {
            $.each(errors, function(k, v) {
                var $p = $('<p></p>').addClass('text-danger').html(v);
                $('[data-input='+ k +']').parent().append($p);
            });
        };

        $('form[name=form-add-specialty]').submit(function(e) {
            e.preventDefault();
            e.stopPropagation();

            var self = $(this);
            self.find('.text-danger').remove();

            $.ajax({
                type: 'post',
                url: $(self).attr('action'),
                data: {
                    '_token': $('meta[name=csrf-token]').attr('content'),
                    'codigo': self.find('[data-input=codigo]').val(),
                    'nome': self.find('[data-input=nome]').val()
                  
                },
                success: function(response) {
                    if (!response.success) {
                        check_errors(response.errors);
                    }
                    else {
                        window.location = response.redirectTo;
                    }
                }
            });
        });
        
        $('[name=btn-edit-specialty]').click(function() {
            var self = $(this);
            var $modal = $("#modal-edit-specialty");

            $modal.find('[data-input=id]').val(self.attr('data-id'));
            $modal.find('[data-input=codigo]').val(self.attr('data-codigo'));
            $modal.find('[data-input=nome]').val(self.attr('data-nome'));
          
            $modal.modal('show');
       });

        $('form[name=form-edit-specialty]').submit(function(e) {
            e.preventDefault();
            e.stopPropagation();

            var self = $(this);
            self.find('.text-danger').remove();

            $.ajax({
                type: 'post',
                url: $(self).attr('action'),
                data: {
                    '_token': $('meta[name=csrf-token]').attr('content'),
                    'codigo': self.find('[data-input=codigo]').val(),
                    'nome': self.find('[data-input=nome]').val(),
                    'id': self.find('[data-input=id]').val()

                },
                success: function(response) {
                    if (!response.success) {
                        check_errors(response.errors);
                    }
                    else {
                        window.location = response.redirectTo;
                    }
                }
            });
        });

    });
})();