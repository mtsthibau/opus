/*!
 * backend.users.js
 * Funções da página de usuários no backend
 *
 * Joselmo Cardozo
 */
(function() {
    require([], function() {
    
        
        $('[name=btn-del-exam]').click(function(errors){
            var exam = $(this).attr('data-exam');
            
            var msg = 'Tem certeza que gostaria de remover o exame ' + exam + '?';
            
            if (confirm(msg)){
                window.location = $(this).attr('data-url');
            }
              
        });
        
        
          
        
    
        var check_errors = function(errors) {
            $.each(errors, function(k, v) {
                var $p = $('<p></p>').addClass('text-danger').html(v);
                $('[data-input='+ k +']').parent().append($p);                                          
            });
        };

        $('form[name=form-add-exam]').submit(function(e) {
            e.preventDefault();
            e.stopPropagation();

            var self = $(this);
            self.find('.text-danger').remove();

            $.ajax({
                type: 'post',
                url: $(self).attr('action'),
                data: {
                    '_token': $('meta[name=csrf-token]').attr('content'),
                    'codigo': self.find('[data-input=codigo]').val(),
                    'exam': self.find('[data-input=exam]').val(),
                    'description': self.find('[data-input=description]').val()
                    
                },
                success: function(response) {
                    if (!response.success) {
                        check_errors(response.errors);
                    }
                    else {
                        window.location = response.redirectTo;
                    }
                }
            });
        });
        
       $('[name=btn-edit-exam]').click(function() {
            var self = $(this);
            var $modal = $("#modal-edit-exam");

            $modal.find('[data-input=codigo]').val(self.attr('data-codigo'));
            $modal.find('[data-input=id]').val(self.attr('data-id'));
            $modal.find('[data-input=exam]').val(self.attr('data-exam'));
            $modal.find('[data-input=description]').val(self.attr('data-description'));
          
            $modal.modal('show');
        });
         $('form[name=form-edit-exam]').submit(function(e) {
            e.preventDefault();
            e.stopPropagation();

            var self = $(this);
            self.find('.text-danger').remove();

            $.ajax({
                type: 'post',
                url: $(self).attr('action'),
                data: {
                    '_token': $('meta[name=csrf-token]').attr('content'),
                    'codigo': self.find('[data-input=codigo]').val(),
                    'exam': self.find('[data-input=exam]').val(),
                    'description': self.find('[data-input=description]').val(),
                    'id': self.find('[data-input=id]').val()

                },
                success: function(response) {
                    if (!response.success) {
                        check_errors(response.errors);
                    }
                    else {
                        window.location = response.redirectTo;
                    }
                }
            });
        });
    });
})();