/*!
 * Relacionamento de Collaborators e Jobs
 * Matheus Thibau
 */
(function() {
    require([], function() {
    
        
        $('[name=btn-del-job]').click(function(){
            var job = $(this).attr('data-job');

            var msg = 'Tem certeza que gostaria de remover o cargo?';
            if (confirm(msg)){
                window.location = $(this).attr('data-url');
            }
        });
    
        var check_errors = function(errors) {
            $.each(errors, function(k, v) {
                var $p = $('<p></p>').addClass('text-danger').html(v);
                $('[data-input='+ k +']').parent().append($p);
            });
        };

        $('form[name=form-add-job]').submit(function(e) {
            e.preventDefault();
            e.stopPropagation();

            var self = $(this);
            self.find('.text-danger').remove();

            $.ajax({
                type: 'post',
                url: $(self).attr('action'),
                data: {
                    '_token': $('meta[name=csrf-token]').attr('content'),
                    'id': self.find('[data-input=id]').val(),
                    'date': self.find('[data-input=date]').val(),
                    'job': self.find('[data-input=job]').val()                  
                },
                success: function(response) {
                    if (!response.success) {
                        check_errors(response.errors);
                    }
                    else {
                        window.location = response.redirectTo;
                    }
                }
            });
        });
    });
})();