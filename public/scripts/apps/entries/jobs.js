/*!
 * backend.users.js
 * Funções da página de usuários no backend
 *
 * Joselmo Cardozo
 */
(function() {
    require([], function() {
    
        
        $('[name=btn-del-job]').click(function(){
            var job = $(this).attr('data-job');

            var msg = 'Tem certeza que gostaria de remover o cargo ' + job + '?';
            if (confirm(msg)){
                window.location = $(this).attr('data-url');
            }
        });
    
        var check_errors = function(errors) {
            $.each(errors, function(k, v) {
                var $p = $('<p></p>').addClass('text-danger').html(v);
                $('[data-input='+ k +']').parent().append($p);
            });
        };

        $('form[name=form-add-job]').submit(function(e) {
            e.preventDefault();
            e.stopPropagation();

            var self = $(this);
            self.find('.text-danger').remove();

            $.ajax({
                type: 'post',
                url: $(self).attr('action'),
                data: {
                    '_token': $('meta[name=csrf-token]').attr('content'),
                    'codigo': self.find('[data-input=codigo]').val(),
                    'job': self.find('[data-input=job]').val(),
                    'function': self.find('[data-input=function]').val()
                  
                },
                success: function(response) {
                    if (!response.success) {
                        check_errors(response.errors);
                    }
                    else {
                        window.location = response.redirectTo;
                    }
                }
            });
        });
        
        $('[name=btn-edit-job]').click(function() {
            var self = $(this);
            var $modal = $("#modal-edit-job");

            $modal.find('[data-input=codigo]').val(self.attr('data-codigo'));
            $modal.find('[data-input=job]').val(self.attr('data-job'));
            $modal.find('[data-input=function]').val(self.attr('data-function'));
            $modal.find('[data-input=id]').val(self.attr('data-id'));

          
            $modal.modal('show');
       });

        $('form[name=form-edit-job]').submit(function(e) {
            e.preventDefault();
            e.stopPropagation();

            var self = $(this);
            self.find('.text-danger').remove();

            $.ajax({
                type: 'post',
                url: $(self).attr('action'),
                data: {
                    '_token': $('meta[name=csrf-token]').attr('content'),
                    'codigo': self.find('[data-input=codigo]').val(),
                    'job': self.find('[data-input=job]').val(),
                    'function': self.find('[data-input=function]').val(),
                    'id': self.find('[data-input=id]').val()

                },
                success: function(response) {
                    if (!response.success) {
                        check_errors(response.errors);
                    }
                    else {
                        window.location = response.redirectTo;
                    }
                }
            });
        });

    });
})();