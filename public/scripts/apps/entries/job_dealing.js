/*!
 * backend.users.js
 * Funções da página de usuários no backend
 *
 * Joselmo Cardozo
 */
(function() {
    require([], function() {
    
        
        $('[name=btn-del-dealing]').click(function(){
            var dealing = $(this).attr('data-dealing');

            var msg = 'Tem certeza que gostaria de remover o procedimento?';
            if (confirm(msg)){
                window.location = $(this).attr('data-url');
            }
        });
    
        var check_errors = function(errors) {
            $.each(errors, function(k, v) {
                var $p = $('<p></p>').addClass('text-danger').html(v);
                $('[data-input='+ k +']').parent().append($p);
            });
        };

        $('form[name=form-add-dealing]').submit(function(e) {
            e.preventDefault();
            e.stopPropagation();

            var self = $(this);
            self.find('.text-danger').remove();

            $.ajax({
                type: 'post',
                url: $(self).attr('action'),
                data: {
                    '_token': $('meta[name=csrf-token]').attr('content'),
                    'id': self.find('[data-input=id]').val(),
                    'dealing': self.find('[data-input=dealing]').val()                  
                },
                success: function(response) {
                    if (!response.success) {
                        check_errors(response.errors);
                    }
                    else {
                        window.location = response.redirectTo;
                    }
                }
            });
        });
    });
})();