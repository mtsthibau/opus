/*!
 *validacão de forms de relacionamentos de exames com as empresas
 */
(function() {
    require([], function() {
    
        
        $('[name=btn-del-exam]').click(function(){
            var exam = $(this).attr('data-exam');
            
            var msg = 'Tem certeza que gostaria de remover o exame ?';
            
            if (confirm(msg)){
                window.location = $(this).attr('data-url');
            }
            
        });
    
        var check_errors = function(errors) {
            $.each(errors, function(k, v) {
                var $p = $('<p></p>').addClass('text-danger').html(v);
                $('[data-input='+ k +']').parent().append($p);                                          
            });
        };
               
        $('form[name=form-add-exam]').submit(function(e) {
            e.preventDefault();
            e.stopPropagation();

            var self = $(this);
            self.find('.text-danger').remove();

            $.ajax({
                type: 'post',
                url: $(self).attr('action'),
                data: {
                    '_token': $('meta[name=csrf-token]').attr('content'),
                    'id': self.find('[data-input=id]').val(),
                    'price': self.find('[data-input=price]').val(),
                    'exam': self.find('[data-input=exam]').val()                    
                },
                success: function(response) {
                    if (!response.success) {
                        check_errors(response.errors);
                    }
                    else {
                        window.location = response.redirectTo;
                    }
                }
            });
        });
        
       $('[name=btn-edit-exam]').click(function() {
            var self = $(this);
            var $modal = $("#modal-edit-exam");

            $modal.find('[data-input=id]').val(self.attr('data-id'));
            $modal.find('[data-input=price]').val(self.attr('data-price'));
            $modal.find('[data-input=exam]').val(self.attr('data-exam'));
       
          
            $modal.modal('show');
        });
         $('form[name=form-edit-exam]').submit(function(e) {
            e.preventDefault();
            e.stopPropagation();

            var self = $(this);
            self.find('.text-danger').remove();

            $.ajax({
                type: 'post',
                url: $(self).attr('action'),
                data: {
                    '_token': $('meta[name=csrf-token]').attr('content'),
                    'id': self.find('[data-input=id]').val(),
                    'exam': self.find('[data-input=exam]').val(),
                    'price': self.find('[data-input=price]').val()
                    
                },
                success: function(response) {
                    if (!response.success) {
                        check_errors(response.errors);
                    }
                    else {
                        window.location = response.redirectTo;

                    }
                }
               
            });
        });
    });
})();