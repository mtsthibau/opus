/*!
 * validação forms de relacionamento de procedimentos com a empresa
 */
(function() {
    require([], function() {
    
        
        $('[name=btn-del-dealing]').click(function(){
            var exam = $(this).attr('data-dealing');
            
            var msg = 'Tem certeza que gostaria de remover o procedimento ?';
            
            if (confirm(msg)){
                window.location = $(this).attr('data-url');
            }
            
        });
    
        var check_errors = function(errors) {
            $.each(errors, function(k, v) {
                var $p = $('<p></p>').addClass('text-danger').html(v);
                $('[data-input='+ k +']').parent().append($p);                                          
            });
        };

        $('form[name=form-add-dealing]').submit(function(e) {
            e.preventDefault();
            e.stopPropagation();

            var self = $(this);
            self.find('.text-danger').remove();

            $.ajax({
                type: 'post',
                url: $(self).attr('action'),
                data: {
                    '_token': $('meta[name=csrf-token]').attr('content'),
                    'id': self.find('[data-input=id]').val(),
                    'dealing': self.find('[data-input=dealing]').val(),
                    'price': self.find('[data-input=price]').val(),
                    
                    
                },
                success: function(response) {
                    if (!response.success) {
                        check_errors(response.errors);
                    }
                    else {
                        window.location = response.redirectTo;
                    }
                }
            });
        });
        
       $('[name=btn-edit-dealing]').click(function() {
            var self = $(this);
            var $modal = $("#modal-edit-dealing");

            $modal.find('[data-input=id]').val(self.attr('data-id'));
            $modal.find('[data-input=price]').val(self.attr('data-price'));
            $modal.find('[data-input=dealing]').val(self.attr('data-dealing'));
          
            $modal.modal('show');
            
        });
         $('form[name=form-edit-dealing]').submit(function(e) {
            e.preventDefault();
            e.stopPropagation();

            var self = $(this);
            self.find('.text-danger').remove();

            $.ajax({
                type: 'post',
                url: $(self).attr('action'),
                data: {
                    '_token': $('meta[name=csrf-token]').attr('content'),
                    'id': self.find('[data-input=id]').val(),
                    'price': self.find('[data-input=price]').val()
                },
                
                success: function(response) {
                    if (!response.success) {
                        check_errors(response.errors);
                    }
                    else {
                        window.location = response.redirectTo;
                    }
                }
            });
        });
    });
})();