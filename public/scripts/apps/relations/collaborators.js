/*!
 *validacão de forms de relacionamentos de empresas com colaboradores
 */
(function() {
    require([], function() {
    
        
        $('[name=btn-del-collaborator]').click(function(){
            var collaborator = $(this).attr('data-collaborator');
            
            var msg = 'Tem certeza que gostaria de remover o colaborador ?';
            
            if (confirm(msg)){
                window.location = $(this).attr('data-url');
            }
            
        });
    
        var check_errors = function(errors) {
            $.each(errors, function(k, v) {
                var $p = $('<p></p>').addClass('text-danger').html(v);
                $('[data-input='+ k +']').parent().append($p);                                          
            });
        };
               
        $('form[name=form-add-collaborator]').submit(function(e) {
            e.preventDefault();
            e.stopPropagation();

            var self = $(this);
            self.find('.text-danger').remove();

            $.ajax({
                type: 'post',
                url: $(self).attr('action'),
                data: {
                    '_token': $('meta[name=csrf-token]').attr('content'),
                    'id': self.find('[data-input=id]').val(),
                    'collaborator_id': self.find('[data-input=collaborator_id]').val(),
                    'date': self.find('[data-input=date]').val()                    
                },
                success: function(response) {
                    if (!response.success) {
                        check_errors(response.errors);
                    }
                    else {
                        window.location = response.redirectTo;
                    }
                }
            });
        });
        
       $('[name=btn-edit-collaborator]').click(function() {
            var self = $(this);
            var $modal = $("#modal-edit-collaborator");

            $modal.find('[data-input=id]').val(self.attr('data-id'));
            $modal.find('[data-input=date]').val(self.attr('data-date'));
            $modal.find('[data-input=nome]').val(self.attr('data-nome'));
            $modal.find('[data-input=collaborator]').val(self.attr('data-collaborator'));
       
          
            $modal.modal('show');
        });
         $('form[name=form-edit-collaborator]').submit(function(e) {
            e.preventDefault();
            e.stopPropagation();

            var self = $(this);
            self.find('.text-danger').remove();

            $.ajax({
                type: 'post',
                url: $(self).attr('action'),
                data: {
                    '_token': $('meta[name=csrf-token]').attr('content'),
                    'id': self.find('[data-input=id]').val(),
                    'nome': self.find('[data-input=nome]').val(),
                    'collaborator': self.find('[data-input=collaborator]').val(),
                    'date': self.find('[data-input=date]').val()
                    
                },
                success: function(response) {
                    if (!response.success) {
                        check_errors(response.errors);
                    }
                    else {
                        window.location = response.redirectTo;
                    }
                }
            });
        });
    });
})();