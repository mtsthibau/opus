/*!
 * user.login-inline.js
 * Funções do formulário de autenticação
 *
 * Joselmo Cardozo
 */

define(function() {
    var
        onLoad = function(){
            var iusername = $('[name=username]');
            if (iusername.val() == "") {
                iusername.focus();
            }
            else {
                AJS.$('[name=password]').focus();
            }
            $("#inline-login form").ajaxForm({
                success: function(responseText, statusText, xhr, $form) {
                    if (xhr.status == 200 && typeof responseText == "object" &&
                        responseText.success == true) {
                        window.location = $form.find('[name=returnUrl]').val();
                    }
                    else {
                        $form.parent().html(responseText);
                    }
                }
            });
        };

    return {
        load: onLoad
    }
});