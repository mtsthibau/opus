/*!
 * backend.users.js
 * Funções da página de usuários no backend
 *
 * Joselmo Cardozo
 */

(function() {
    require([], function() {

        $('#add-user').click(function() {
            $("#modal-add-user").modal();
        });

        $('form[name=form-new-user]').submit(function(e) {
            e.preventDefault();
        });

        var proccessGroupsModal = function($modal, $element) {
            $modal.find('select option').remove();
            var groups = $element.attr('data-groups').split(',');

            $.each(_context.groups, function(i, group) {
                var $option = $("<option></option>")
                    .html(group.name)
                    .attr('value', group.id);

                if (groups.indexOf(group.id.toString()) != -1) {
                    $option.appendTo("#groupsToLeave");
                }
                else {
                    $option.appendTo("#groupsToJoin");
                }
            });
        };

        $('[name=btn-group-user]').click(function() {
            var $pai = $(this).parent();
            var $modal = $("#group-modal");

            // Preenchemos o título do modal com o nome do usuário
            $modal.find('h4.modal-title').html($pai.attr('data-name'));

            // Preenchemos o ID do usuário que terá seus grupos alterados
            $modal.find('input[name=username]').val($pai.attr('data-username'));

            proccessGroupsModal($modal, $pai);
            $modal.modal('show');
        });

    });
})();