/*!
 * groups.js
 * Funções da página de grupos de usuários no backend
 *
 * Joselmo Cardozo
 */

(function() {
    require([], function() {
        $("[name=bt_set_permission]").click(function() {
            $("form[name=set_permission]").submit();
        });
    });
})();