/*!
 * backend.users.js
 * Funções da página de usuários no backend
 *
 * Joselmo Cardozo
 */

(function() {
    require([], function() {

        var check_errors = function(errors) {
            $.each(errors, function(k, v) {
                var $p = $('<p></p>').addClass('text-danger').html(v);
                $('[data-input='+ k +']').parent().append($p);
            });
        };

        $('[name=btn-edit-user]').click(function() {
            var self = $(this);
            var $modal = $("#modal-edit-user");

            $modal.find('[data-input=username]').val(self.attr('data-username'));
            $modal.find('[data-input=email]').val(self.attr('data-email'));
            $modal.find('[data-input=fullname]').val(self.attr('data-fullname'));
            $modal.find('[data-input=user_id]').val(self.attr('data-id'));

            if (self.attr('data-active') == '1') {
                $modal.find('[data-input=active]').attr('checked', 'checked');
            }
            else {
                $modal.find('[data-input=active]').removeAttr('checked');
            }

            $modal.modal('show');
        });

        $('form[name=form-edit-user]').submit(function(e) {
            e.preventDefault();
            e.stopPropagation();

            var self = $(this);
            self.find('.text-danger').remove();

            $.ajax({
                type: 'post',
                url: self.attr('action'),
                data: {
                    '_token': $('meta[name=csrf-token]').attr('content'),
                    'user_id': self.find('[data-input=user_id]').val(),
                    'username': self.find('[data-input=username]').val(),
                    'email': self.find('[data-input=email]').val(),
                    'fullname': self.find('[data-input=fullname]').val(),
                    'active': self.find('[data-input=active]').is(':checked')
                },
                success: function(response) {
                    if (!response.success) {
                        check_errors(response.errors);
                    }
                    else {
                        window.location = response.redirectTo;
                    }
                }
            });
        });

        $('#add-user').click(function() {
            $("#modal-add-user").modal();
        });

        $('form[name=form-new-user]').submit(function(e) {
            e.preventDefault();
            e.stopPropagation();

            var self = $(this);
            self.find('.text-danger').remove();

            $.ajax({
                type: 'post',
                url: $(self).attr('action'),
                data: {
                    '_token': $('meta[name=csrf-token]').attr('content'),
                    'username': self.find('[data-input=username]').val(),
                    'password': self.find('[data-input=password]').val(),
                    'confirm': self.find('[data-input=confirm]').val(),
                    'email': self.find('[data-input=email]').val(),
                    'fullname': self.find('[data-input=fullname]').val()
                },
                success: function(response) {
                    if (!response.success) {
                        check_errors(response.errors);
                    }
                    else {
                        window.location = response.redirectTo;
                    }
                }
            });
        });

        var proccessGroupsModal = function($modal, $element) {
            $modal.find('select option').remove();
            var groups = $element.attr('data-groups').split(',');

            $.each(_context.groups, function(i, group) {
                var $option = $("<option></option>")
                    .html(group.name)
                    .attr('value', group.id);

                if (groups.indexOf(group.id.toString()) != -1) {
                    $option.appendTo("#groupsToLeave");
                }
                else {
                    $option.appendTo("#groupsToJoin");
                }
            });
        };

        $('[name=btn-group-user]').click(function() {
            var $pai = $(this).parent();
            var $modal = $("#group-modal");

            // Preenchemos o título do modal com o nome do usuário
            $modal.find('h4.modal-title').html($pai.attr('data-name'));

            // Preenchemos o ID do usuário que terá seus grupos alterados
            $modal.find('input[name=username]').val($pai.attr('data-username'));

            proccessGroupsModal($modal, $pai);
            $modal.modal('show');
        });

    });
})();