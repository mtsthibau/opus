
	$(document).ready(function() {
	
		var date = new Date();
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();
		
      

		$('#calendar').fullCalendar({
           
            
           
            height: 500,
            
            monthNames:['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho',
                        'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                        
            monthNamesShort:['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],

            dayNames:['Domingo', 'Segunda', 'Terça', 'Quarta','Quinta', 'Sexta', 'Sábado'],
            
            dayNamesShort:['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
                        
            
            header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
            buttonText: {
                today: 'hoje',
                month: 'mês',
                week: 'semana',
                day: 'dia'
            },
			editable: true,
			events: [
				{
					title: 'Evento Semanal',
					start: new Date(y, m, 1)
				},
				{
					title: 'Semana de Exames da Empresa X',
					start: new Date(y, m, d-8),
					end: new Date(y, m, d-3)
				},
				{
					id: 999,
					title: 'Atividade Dividida',
					start: new Date(y, m, d+4, 16, 0),
					allDay: false
				},
				{
					id: 999,
					title: 'Atividade Dividida',
					start: new Date(y, m, d+11, 16, 0),
					allDay: false
				},
				{
					title: ' Reunião',
					start: new Date(y, m, d, 10, 30),
					allDay: false
				},
				{
					title: 'Veja os exames do próximo mês (link)',
					start: new Date(y, m, 28),
					end: new Date(y, m, 29),
					url: 'http://google.com/'
				}
			],
       
        
          dayClick: function(date, allDay, jsEvent, view) {

        

        // change the day's background color just for fun
        $(this).css('background-color', 'red');

    },
      eventClick: function(calEvent, jsEvent, view) {

        alert('Event: ' + calEvent.title);
        alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
        alert('View: ' + view.name);

        // change the border color just for fun
        $(this).css('border-color', 'red');

    },
        eventClick: function(event) {
        if (event.url) {
            window.open(event.url);
            return false;
        }
    }

		
        }
        
        
        );     
		
	});

    
    
    
    
    