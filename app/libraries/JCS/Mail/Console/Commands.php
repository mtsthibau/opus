<?php

namespace JCS\Mail\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption,
    Symfony\Component\Console\Input\InputArgument;
use JCS\Mail\Models\Mail,
    JCS\Mail\Models\Queue,
    JCS\Mail\Models\QueueEmptyException,
    JCS\Mail\Models\QueueWithErrorException,
    JCS\Mail\Models\QueueAlreadyWorkingException;


class Commands extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'jcs:mail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Manipula o envio de email';

    /**
     * Create a new command instance.
     *
     * @return \JCS\Mail\Console\Commands
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Executa o envio de emails a partir da Queue
     *
     * @param Queue $queue
     * @return void
     */
    private function send($queue)
    {
        try {
            $this->info('Executing queue '. $queue->id .'...');
            $queue->dispatch();
        }
        catch(QueueEmptyException $e) {
            $queue->delete();
            $this->info('Queue '. $queue->id .' is done. Deleted.');
        }
        catch(QueueAlreadyWorkingException $e) {
            $this->info('Queue '. $queue->id .' is already running.');
        }
        catch(QueueWithErrorException $e) {
            $this->info('Queue '. $queue->id .' is done. But errors occurred.');
        }
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        switch ($this->argument('mode'))
        {
            case 'send':
                $opQueue = $this->option('queue');
                if (!$opQueue) {
                    $this->info('Option --queue=ID should be informed.');
                    return;
                }

                $queue = Queue::find($opQueue);
                if (!$queue) {
                    $this->info('Queue '. $opQueue .' not found.');
                    return;
                }

                $this->send($queue);
                break;

            case 'repair':
                $this->repair();
                break;

            case 'clear':
                $this->clear();
                break;

            default:
                $this->showInfo();
        }
    }

    /**
     * Busca por falhas no envio e tenta reenviá-los.
     *
     * Para tentar reparar todas queues:
     *     php artisan jcs:mail --repair
     *
     * @return void
     */
    private function repair()
    {
        $queues = Queue::distinct()->select('mail_queues.id as id')
            ->where('mail_queues.working', '=', false)
            ->join('mail_items', 'mail_items.queue_id', '=', 'mail_queues.id')
            ->where('mail_items.status', '=', Mail::STATUS_ERROR)
            ->get();

        if (count($queues)) {
            foreach ($queues as $queue) {
                $this->send(Queue::find($queue->id));
            }
        }
        else {
            $this->info('No mail queue to be repaired.');
        }
    }

    /**
     * Comando que removerá todos os emails/queues da fila de envio
     *
     * @return void
     */
    private function clear()
    {
        $this->showInfo();
        $this->info('');
        if ($this->confirm('Confirms removing of all the mail queues? [y/N] ', false))
        {
            Mail::all()->each(function($item) {
                $item->delete();
            });
            Queue::all()->each(function($queue) {
                $queue->delete();
            });
            $this->info('Queue sending emails emptied.');
        }
    }

    /**
     * Printa no console informações sobre uma ou todas as queues que ainda estão
     * no banco de dados.
     *
     * Para buscar um resumo de todas as queues utilize o comando:
     *     php artisan jcs:mail
     *
     * Para buscar informacoes de uma queue específica e os emails que ainda estão
     * esperando para serem enviados utilize o comando abaixo trocando ID pela
     * id da queue:
     *     php artisan jcs:mail --queue=ID
     *
     * Nota: É possível buscar a ID de todas as queues utilizando o comando
     * sem enviar a opção --queue como no primeiro exemplo informado acima.
     *
     * @return void
     */
    private function showInfo()
    {
        $opQueue = $this->option('queue');

        if ($opQueue) {
            $queue = Queue::find($opQueue);

            if ($queue)
            {
                $this->printInfoHeader();
                $this->printQueueRow($queue);
                $this->printEmailsRow($queue);
            }
            else {
                $this->info('Queue '. $opQueue .' not found.');
            }
        }
        else {
            $queues = Queue::all();
            if (count($queues))
            {
                $this->printInfoHeader();
                foreach ($queues as $queue)
                {
                    $this->printQueueRow($queue);
                }
            }
            else {
                $this->info('No queue found.');
            }
        }
    }

    /**
     * Printa no console a lista de emails da queue em forma de tabela
     *
     * @param Queue $queue
     * @return void
     */
    private function printEmailsRow(Queue $queue)
    {
        $this->info('          --------------------------------------------------------');
        foreach ($queue->mails()->orderBy('email', 'asc')->get() as $mail) {
            $this->info('          '. $mail->email);
        }
    }

    /**
     * Exibe uma linha com algumas informacoes da queue em forma de tabela
     *
     * @param Queue $queue
     * @return void
     */
    private function printQueueRow(Queue $queue)
    {
        $this->info(sprintf('%s%s%s%s%s%s%s',
            $queue->id,
            str_repeat(' ', 12 - strlen(((string) $queue->id))),
            ($queue->working) ? 'true ' : 'false',
            str_repeat(' ', 5),
            $queue->mails()->count(),
            str_repeat(' ', 12 - strlen(((string) $queue->mails()->count()))),
            $queue->subject
        ));
    }

    /**
     * Printa no console o cabeçalho da tabela.
     *
     * @return void
     */
    private function printInfoHeader()
    {
        $this->info('ID        | Working | Emails    | Subject');
        $this->info('------------------------------------------------------------------');
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('mode', InputArgument::OPTIONAL, 'Modo de execucao do comando.')
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('queue', null, InputOption::VALUE_OPTIONAL, 'ID da queue para execucao.', null)
        );
    }
}