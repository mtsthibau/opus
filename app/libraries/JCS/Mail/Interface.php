<?php

namespace JCS\Mail;

use Illuminate\Database\Eloquent\Collection;
use Symfony\Component\Process\Exception\RuntimeException;

/**
 * AddressSourceInterface é uma interface que as fontes de emails precisam implementar.
 *
 * interface AddressSourceInterface
 * @package JCS\Mail
 */
interface AddressSourceInterface
{
    public function __construct($source);
    public function get();
}

/**
 * Gera uma fonte de emails apartir de uma lista de grupos (Role)
 *
 * Class GroupSource
 * @package JCS\Mail
 */
class GroupSource implements AddressSourceInterface
{
    private $source = null;

    public function __construct($source)
    {
        if (false === $source instanceof Collection) {
            throw new RuntimeException('invalid type of mail source');
        }
        $this->source = $source;
    }

    public function get()
    {
        $mails = array();
        foreach ($this->source as $group)
        {
            foreach ($group->users as $user)
            {
                if (false === in_array($user->email, $mails))
                {
                    array_push($mails, $user->email);
                }
            }
        }
        return $mails;
    }
}

/**
 * Gera uma fonte de emails apartir de uma outra lista
 *
 * Class GroupSource
 * @package JCS\Mail
 */
class ArraySource implements AddressSourceInterface
{
    private $source = null;

    public function __construct($source)
    {
        if (false === is_array($source)) {
            throw new RuntimeException('invalid type of mail source');
        }
        $this->source = $source;
    }

    public function get()
    {
        $mails = array();
        foreach ($this->source as $mail)
        {
            if (false === in_array($mail, $mails))
            {
                array_push($mails, $mail);
            }
        }
        return $mails;
    }
}