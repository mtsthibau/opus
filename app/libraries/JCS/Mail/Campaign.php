<?php

namespace JCS\Mail;

use JCS\Mail\Models\Mail,
    JCS\Mail\Models\Queue;


class Campaign {

    /**
     * Objeto Queue
     *
     * @var Queue
     */
    protected $queue = null;

    /**
     * Lista de emails que sera adicionada à Queue
     *
     * @var array
     */
    protected $mails = array();

    /**
     * Modo de envio da mensagem
     *
     * @var integer
     */
    protected $mode = null;

    /**
     * Lista com informacoes do assunto/corpo da mensagem
     *
     * @var array
     */
    protected $message = array(
        'subject' => null,
        'message' => null,
        'content_type' => 'plain'
    );

    /**
     * Adiciona emails
     *
     * @param AddressSourceInterface $source
     * @return Campaign $this
     */
    public function addAddress(AddressSourceInterface $source)
    {
        $this->mails = array_merge($this->mails, $source->get());

        return $this;
    }

    /**
     * Define o assunto a mensagem e o tipo de mensagem que será enviada.
     *
     * @param string $subject
     * @param string $body
     * @param string $content_type
     * @return Campaign $this
     * @throws \InvalidArgumentException
     */
    public function setMessage($subject, $body, $content_type)
    {
        if (false === is_string($subject)) {
            throw new \InvalidArgumentException("invalid mail subject string");
        }

        if (false === is_string($body)) {
            throw new \InvalidArgumentException("invalid mail body string");
        }

        if (false === in_array($content_type, array('plain', 'html'))) {
            throw new \InvalidArgumentException("invalid mail content type");
        }

        $this->message = array(
            'subject' => $subject,
            'body' => $body,
            'content_type' => $content_type
        );

        return $this;
    }

    /**
     * Define o modo de envio da mensagem, podendo ser single, com cópia e com cópia oculta.
     *
     * @param int $mode
     * @return Campaign $this
     * @throws \InvalidArgumentException
     */
    public function setMode($mode)
    {
        if (false === in_array((int) $mode, Queue::getModes())) {
            throw new \InvalidArgumentException("invalid mail mode");
        }
        $this->mode = $mode;

        return $this;
    }

    /**
     * Armazena a mensagem no banco de dados
     *
     * @return void
     */
    public function save()
    {
        $this->queue = new Queue;
        $this->queue->subject = $this->message['subject'];
        $this->queue->message = $this->message['body'];
        $this->queue->content_type = $this->message['content_type'];
        $this->queue->mode = $this->mode;
        $this->queue->save();

        foreach ($this->mails as $mail) {
            $objm = new Mail;
            $objm->queue_id = $this->queue->id;
            $objm->email = $mail;
            $objm->save();
        }
    }

    /**
     * Envia um sinal para o model Queue iniciar o envio das mensagens
     *
     * @return void
     * @throws \RuntimeException
     */
    public function execute()
    {
        if (is_null($this->queue)) {
            throw new \RuntimeException('you must save the queue before running');
        }

        $this->queue->execute('send');
    }

    /**
     * Envia um sinal para o model Queue iniciar o reenvio dos emails que estão com
     * status de falha no envio.
     *
     * @return void
     */
    public static function repair()
    {
        $queue = new Queue;
        $queue->execute('repair');
    }
}