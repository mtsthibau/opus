<?php

namespace JCS\Auth\Models;


class Permission extends \Eloquent
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_permissions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = array('name', 'description', 'scope_id', 'tag');

    /**
     * Roles
     *
     * @return object
     */
    public function roles()
    {
        return $this->belongsToMany('JCS\Auth\Models\Role', 'user_permission_role');
    }

    /**
     * Scope
     *
     * @return object
     */
    public function scope()
    {
        return $this->belongsTo('JCS\Auth\Models\PermissionScope', 'scope_id');
    }
}