<?php

namespace JCS\Auth\Models;


class Role extends \Eloquent
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_roles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = array('name', 'description', 'level');

    /**
     * Users
     *
     * @return object
     */
    public function users()
    {
        return $this->belongsToMany('User','user_role_user');
    }

    /**
     * Permissions
     *
     * @return object
     */
    public function permissions()
    {
        return $this->belongsToMany('JCS\Auth\Models\Permission', 'user_permission_role');
    }

    /**
     * Can the Group do something
     *
     * @param  array|string $permissions Single permission or an array or permissions
     * @return boolean
     */
    public function can($permissions)
    {
        $permissions = !is_array($permissions)
            ? array($permissions)
            : $permissions;

        $valid = false;
        foreach ($this->permissions as $permission)
        {
            if (in_array($permission->tag, $permissions))
            {
                $valid = true;
                break;
            }
        }

        return $valid;
    }
}