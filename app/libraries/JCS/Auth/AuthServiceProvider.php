<?php

namespace JCS\Auth;

use Illuminate\Support\ServiceProvider,
    Illuminate\Hashing\BcryptHasher,
    Illuminate\Auth\Guard;


class AuthServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        \Auth::extend('jcs', function()
        {
            return new Guard(
                new AuthUserProvider(
                    new BcryptHasher,
                    \Config::get('auth.model')
                ),
                \App::make('session.store')
            );
        });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }
}