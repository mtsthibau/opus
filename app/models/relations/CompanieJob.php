<?php


class CompanieJob extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'companie_job';

    /**
     * Retorna Companie da relação'
     *
     * @return Companie
     */
    public function companie()
    {
        return $this->belongsTo('Companie', 'companie_id');
    } 
    
    /**
     * Retorna Job da relação'
     *
     * @return Job
     */
    public function job()
    {
        return $this->belongsTo('Job', 'job_id');
    }
}