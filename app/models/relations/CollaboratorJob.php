<?php


class CollaboratorJob extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'collaborators_job';

    /**
     * Retorna Collaborador da relação'
     *
     * @return Companie
     */
    public function collaborators()
    {
        return $this->belongsTo('Collaborator', 'collaborator_id');
    } 
    
      public function job()
    {
        return $this->belongsTo('Job', 'job_id');
    }
     
}