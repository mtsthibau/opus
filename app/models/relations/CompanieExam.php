<?php


class CompanieExam extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'companie_exam';

    /**
     * Retorna Companie da relação'
     *
     * @return Companie
     */
    public function companie()
    {
        return $this->belongsTo('Companie', 'companie_id');
    } 
    
    /**
     * Retorna Exam da relação'
     *
     * @return Exam
     */
    public function exam()
    {
        return $this->belongsTo('Exam', 'exam_id');
    }
}