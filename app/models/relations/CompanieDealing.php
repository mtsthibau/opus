<?php


class CompanieDealing extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'companie_dealing';

    /**
     * Retorna Companie da relação'
     *
     * @return Companie
     */
    public function companie()
    {
        return $this->belongsTo('Companie', 'companie_id');
    } 
    
    /**
     * Retorna Exam da relação'
     *
     * @return Exam
     */
    public function dealing()
    {
        return $this->belongsTo('Dealing', 'dealing_id');
    }
}