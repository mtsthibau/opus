<?php


class CompanieCollaborators extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'companie_collaborators';

   
    public function companie()
    {
        return $this->belongsTo('Companie', 'companie_id');
    } 
    
      public function collaborator()
    {
        return $this->belongsTo('Collaborator', 'collaborator_id');
    }
     
}