<?php


class Collaborator extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'collaborators';
       
       public static function boot()
    {
        parent::boot();
        parent::deleting(function($model) {
            $model->jobs()->delete();
        });
    }
           
    public function jobs()
    {
        return $this->hasMany('CollaboratorJob', 'collaborator_id');
    } 
    
}