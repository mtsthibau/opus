<?php


class Professional extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'professionals';
}