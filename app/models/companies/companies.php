<?php


class Companie extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'companies';
    
    /**
     * Retorna Companie da relação'
     *
     * @return Companie
     */
    public function exams()
    {
        return $this->hasMany('CompanieExam', 'companie_id');
    }
    
        public function dealings()
    {
        return $this->hasMany('CompanieDealing', 'companie_id');
    } 
     
    public function jobs()
    {
        return $this->hasMany('CompanieJob', 'companie_id');
    }  
    
    public function collaborators()
    {
        return $this->hasMany('CompanieCollaborators', 'companie_id');
    } 
}