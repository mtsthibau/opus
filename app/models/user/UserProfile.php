<?php


class UserProfile extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_profile';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = array('id', 'user_id', 'avatar', 'fullname');

    /**
     * Retorna o Usuario pertencente a este Profile
     *
     * @return User
     */
    public function user()
    {
        return $this->belongsTo('User', 'user_id');
    }
}