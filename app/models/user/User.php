<?php


class User extends JCS\Auth\Models\User
{

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::created(function($model)
        {
            $profile = new UserProfile;
            $profile->user_id = $model->id;
            $profile->save();
        });
    }

    /**
     * Retorna o profile do usuario.
     *
     * @return UserProfile
     */
    public function profile()
    {
        return $this->hasOne('UserProfile', 'user_id');
    }
}
