<?php


class JobExam extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'job_exam';
    
     public function exam()
    {
        return $this->belongsTo('Exam', 'exam_id');
    } 
    
     public function job()
    {
        return $this->belongsTo('Job', 'job_id');
    } 
  
    
    
}