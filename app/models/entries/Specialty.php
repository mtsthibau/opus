<?php


class Specialty extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'specialty';
}