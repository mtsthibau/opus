<?php


class Dealing extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'dealing';
    
    public static function boot()
    {
        parent::boot();
        static::deleting(function($model) {
            JobDealing::where('dealing_id', '=', $model->id)->delete();
            $model->companies()->delete();
        });
    }
    
    
    
    public function companies()
    {
        return $this->hasMany('CompanieDealing', 'dealing_id');
    }

    public function jobs()
    {
        return $this->hasMany('JobDealing', 'dealing_id');
    }
    
}