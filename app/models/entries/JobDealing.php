<?php


class JobDealing extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'job_dealing';
      
    public function dealing()
    {
        return $this->belongsTo('Dealing', 'dealing_id');
    } 
     public function job()
    {
        return $this->belongsTo('Job', 'job_id');
    } 
    
}