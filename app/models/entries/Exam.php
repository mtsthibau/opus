<?php

class Exam extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'exams';
    
    //para exclusão de exames que contem uma relação com empresa
    public static function boot()
    {
        parent::boot();
        static::deleting(function($model) {
            JobExam::where('exam_id', '=', $model->id)->delete();
            $model->companies()->delete();
        });
    }
    
    /**
     * Retorna Companie da relação'
     *
     * @return Companie
     */
    public function companies()
    {
        return $this->hasMany('CompanieExam', 'exam_id');
    }
}