<?php


class Job extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'jobs';
   
   
   public static function boot()
    {
        parent::boot();
        parent::deleting(function($model) {
        
            $model->companies()->delete();
            $model->exams()->delete();
            $model->dealings()->delete();
            $model->collaborators()->delete();
        });
    }
    
    
    public function companies()
    {
        return $this->hasMany('CompanieJob', 'job_id');
    }
   

    public function exams()
    {
        return $this->hasMany('JobExam', 'job_id');
    } 
     
    public function dealings()
    {
        return $this->hasMany('JobDealing', 'job_id');
    } 

    public function collaborators()
    {
        return $this->hasMany('CollaboratorJob', 'job_id');
    } 
    
    

}