@extends('template')

@section('content')
<section id="content">
    <div class="aui-page-panel center">
        <div class="aui-page-panel-inner">
            <section class="aui-page-panel-content">
            <header class="aui-page-header">
                <div class="aui-page-header-inner">
                    <div class="aui-page-header-main">
                        <h1>Opa, parece que algo deu errado (500)</h1>
                    </div><!-- .aui-page-header-main -->
                </div><!-- .aui-page-header-inner -->
            </header>
            <div class="aui-message warning">
                <p class="title">
                    <strong>{{ URL::getRequest()->getPathInfo() }}</strong>
                </p>
                <p>Por favor, contate um administrador. Talvez você queira <a href="#">relatar um problema</a>.</p>
                <p>Você pode <a href="{{ URL::previous() }}">Voltar</a> ou ir para a <a href="/">Página Inicial</a></p>
                <span class="aui-icon icon-warning"></span>
            </div>
            </section><!-- .aui-page-panel-content -->
        </div><!-- .aui-page-panel-inner -->
    </div>
</section>
@endsection