@extends('template')

@section('styles')
{{ HTML::style('/styles/global.css') }}
{{ HTML::style('/styles/fullcalendar.css') }}
@endsection

@section('scripts')
{{ HTML::script('/scripts/libs/jquery-ui.custom.min.js') }}
{{ HTML::script('/scripts/libs/fullcalendar.min.js') }}
{{ HTML::script('/scripts/calendar.js') }}

@endsection

<style>
.calendario {
    height: 500px;
    
}
</style>


@section('content')
<h3>Seja Bem Vindo a Opus,	{{ Auth::user()->username }}  !</h3>
<br/>
<div class="calendario"><div id='calendar'></div></div>

@endsection