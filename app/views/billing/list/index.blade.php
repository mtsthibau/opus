@extends('entries/template')

@section('styles')
{{ HTML::style('/styles/global.css') }}
@endsection

@section('scripts')
{{ HTML::script('/scripts/apps/companies/list/list.js') }}
@endsection

@section('backendContent')
<section class="aui-page-panel-content">
    <header class="aui-page-header">
        <div class="aui-page-header-inner">
            <div class="aui-page-header-main">
                <h2><span class="glyphicon glyphicon-tower"></span> Empresas</h2>
                <hr/>
            </div>
        </div>
    </header>

    <table class="table table-hover">
        <thead>
        <tr>
            <th>Nome da Empresa</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($context->companies as $companie)
        <tr>
            <td><a href="{{ URL::action('BillingController@view', array($companie->companie)) }}">{{ $companie->companie }}</a></td>
        </tr>
        @endforeach
        </tbody>
    </table>
</section>
@endsection
