@extends('entries/template')

@section('styles')
{{ HTML::style('/styles/global.css') }}
@endsection

@section('backendContent')
<div class="page-header">
    <h1 id="btn-dropdowns">Detalhamento Empresa</h1>
</div>
<blockquote>
    <h3><span class="glyphicon glyphicon-tower"></span> {{ $context->companie->companie }}</h3>
    <div class="right">
 <a href="{{ URL::action('CompaniesController@edit', array($context->companie->id))}}"><button name="submit" class="btn btn-info">Editar Dados da Empresa</button></a>
</div>
    </br>
     <dl class="dl-horizontal">
          <h4>Dados</h4>
          <div class="center">
              <dt>Código</dt>
              <dd>{{ $context->companie->codigo }}</dd>
              <dt>Nome</dt>
              <dd>{{ $context->companie->companie }}</dd>
              <dt>CNPJ</dt>
              <dd>{{ $context->companie->cnpj }}</dd>
          </div>
          <hr/>
          <h4>Endereço</h4>
          <div class="center">
              <dt>CEP</dt>
              <dd>{{ $context->companie->cep }}</dd>
              <dt>Bairro</dt>
              <dd>{{ $context->companie->district }}</dd>
              <dt>Endereço</dt>
              <dd>{{ $context->companie->address }}</dd>
               <dt>Número/Complemento</dt>
              <dd>{{ $context->companie->number }}<dd>
               <dt>Cidade</dt>
              <dd>{{ $context->companie->city }}</dd>
              <dt>UF</dt>
              <dd>{{ $context->companie->uf }}</dd>
          </div>
          <hr/>
          <h4>Contato</h4>
          <div class="center">
              <dt>Telefone</dt>
              <dd>{{ $context->companie->phone }}</dd>
               <dt>E-mail</dt>
              <dd>{{ $context->companie->email }}</dd>
          </div>
          <h4>Cargos</h4>
          <div class="center">
              <dt>Cargo</dt>
              <dd>{{ $context->companie->cargo }}</dd>
          </div>
    </dl>
</blockquote>
@endsection

