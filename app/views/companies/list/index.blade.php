@extends('entries/template')

@section('styles')
{{ HTML::style('/styles/global.css') }}
@endsection

@section('scripts')
{{ HTML::script('/scripts/apps/companies/list/list.js') }}
@endsection

@section('backendContent')
<div class="page-header">
    <h1 id="btn-dropdowns">Empresas</h1>
</div>

<form role="form" action="{{ URL::action('CompaniesController@index') }}" method="get">
    <h3>Filtrar</h3>

    <div class="row">
         <div class="col-md-4">
            <div class="form-group">
                <label>Código</label>
                <input type="text" class="form-control" placeholder="Código da Empresa" value="{{ $context->filters->codigo }}" name="codigo">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label>Empresa</label>
                <input type="text" class="form-control" placeholder="Nome da Empresa" value="{{ $context->filters->companies }}" name="companies">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label>Endereço</label>
                <input type="text" class="form-control" placeholder="Endereço" value="{{ $context->filters->address }}" name="address">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label>Por Página</label>
                <select class="form-control" name="max">
                    @foreach ($context->paginate as $key => $value)
                    <option value="{{ $key }}" @if ($context->filters->max == $key)selected="selected"@endif>{{ $value }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div>
        <button type="submit" class="btn btn-primary">Filtrar</button>
        <a class="btn btn-link" href="{{ URL::action('CompaniesController@index') }}">Limpar Filtros</a>
        @if (Auth::user()->can('companie.item_edit'))
        <a href="{{ URL::action('CompaniesController@create') }}" class="btn btn-success right" id="add-companies">
            <span class="glyphicon glyphicon-plus"></span> Adicionar empresa
        </a>
        @endif
    </div>
</form>

<div style="margin: 20px 0;">
    Exibindo <strong>{{ $context->companies->getFrom() }}</strong>
    a <strong>{{ $context->companies->getTo() }}</strong>
    de <strong>{{ $context->companies->getTotal() }}</strong> Empresa(s).
</div>

<table class="table table-hover">
    <thead>
    <tr>
        <th>Código</th>
        <th>Empresa</th>
        <th>Endereço</th>
         @if (Auth::user()->can('companie.item_edit'))
        <th style="text-align:right;">Ações</th>
        @endif
    </tr>
    </thead>
    <tbody>
    @foreach ($context->companies as $companie)
    <tr>
        <td>
         <a href="{{ URL::action('CompaniesController@detail', array($companie->id)) }}"> {{ $companie->codigo }} </a>
        </td>
        <td>
          <a href="{{ URL::action('CompaniesController@detail', array($companie->id)) }}"> {{ Str::limit($companie->companie, $limit = 32, $end = '...')  }}</a>
        </td>
         <td>
          <a href="{{ URL::action('CompaniesController@detail', array($companie->id)) }}">{{ Str::limit($companie->address, $limit = 45, $end = '...') }}</a>
        </td>
        @if (Auth::user()->can('companie.item_edit'))
        <td style="text-align:right;">
         <a href="{{ URL::action('CompaniesController@edit', array($companie->id))}}"><button type="button" class="btn btn-info btn-xs" name="btn-edit-companies">Editar</button></a>
        </td>
        @endif
    </tr>
    @endforeach
    </tbody>
</table>
@endsection
