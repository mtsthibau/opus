@extends('entries/template')

@section('styles')
{{ HTML::style('/styles/global.css') }}
{{ HTML::style('/styles/select2.css') }}
{{ HTML::style('/styles/select2-bootstrap.css') }}
@endsection

@section('scripts')
{{ HTML::script('/scripts/apps/entries/collaborator_job.js') }}
{{ HTML::script('/scripts/apps/entries/companies.js') }}


<script>
$("#e1").select2({
    minimumInputLength: 1,
    data: {{ json_encode($context->jobs) }}
    
});    
</script>

@endsection


@section('backendContent')
<div class="page-header">
    <h1 id="btn-dropdowns">Detalhamento Colaborador</h1>
</div>

<blockquote>
    <h3><span class="glyphicon glyphicon-user"></span> {{ $context->collaborator->nome }}</h3>
    <div class="right">
         @if (Auth::user()->can('collaborator.item_edit'))
         <a href="{{ URL::action('CollaboratorController@edit', array($context->collaborator->id))}}"><button name="submit" class="btn btn-info"><span class="glyphicon glyphicon-edit"></span> Editar Dados do Colaborador</button></a>
         @endif
   </div>
    </br>
    <hr/>
     <dl class="dl-horizontal">
          <h4>Dados</h4>
          <div class="center">
              <dt>Código:</dt>
              <dd>{{ $context->collaborator->codigo }}</dd>
              <dt>Nome:</dt>
              <dd>{{ $context->collaborator->nome }}</dd>
              <dt>CPF:</dt>
              <dd>{{ $context->collaborator->cpf }}</dd>
          </div>  
          <hr/>
          <h4>Endereço:</h4>
          <div class="center">
              <dt>CEP:</dt>
              <dd>{{ $context->collaborator->cep }}</dd>
              <dt>Bairro:</dt>
              <dd>{{ $context->collaborator->district }}</dd>
              <dt>Endereço:</dt>
              <dd>{{ $context->collaborator->address }}</dd>
               <dt>Número/Complemento:</dt>
              <dd>{{ $context->collaborator->number }}<dd>
               <dt>Cidade:</dt>
              <dd>{{ $context->collaborator->city }}</dd>
              <dt>UF:</dt>
              <dd>{{ $context->collaborator->uf }}</dd>
         </div>
          <hr/>
          <h4>Contato:</h4>
          <div class="center">
               <dt>Telefone:</dt>
              <dd>{{ $context->collaborator->phone }}</dd>
               <dt>E-mail:</dt>
              <dd>{{ $context->collaborator->email }}</dd>
         </div>
         <hr/>
         
          <div>
            <h4>Cargos:</h4>
              @if (Auth::user()->can('collaborator.item_edit'))
              <a data-toggle="modal" data-target="#modal" href="" class="btn btn-success right">
                   <span class="glyphicon glyphicon-plus"></span> Adicionar
              </a>
              @endif
         </div>
    
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Cargos</th>
            <th>Função</th>
            <th>Data do Vínculo</th>
            @if (Auth::user()->can('collaborator.item_edit'))
            <th style="text-align:right;">Excluir</th>
            @endif
        </tr>
        </thead>

        <tbody>

        @foreach ($context->collaborator->jobs as $job)
        <tr>
            <td>
              {{ Str::limit( $job->job->job, $limit = 32, $end = '...') }} 
            </td>
            <td>
              {{ Str::limit( $job->job->function, $limit = 32, $end = '...') }}
            </td> 
            <td>
              {{ $job->date }}
            </td>
            @if (Auth::user()->can('collaborator.item_edit'))
            <td style="text-align:right;">
                <button type="button" class="btn btn-warning btn-xs" name="btn-del-job" data-url="{{ URL::action('CollaboratorController@delJob', array($job->id)) }}">Excluir</button>
            </td>
            @endif
        </tr>
        @endforeach
        </tbody>
    </table>
         
         
         
         
    </dl>
</blockquote>



<div class="modal fade" id="modal" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Adicionar Cargo ao {{ $context->collaborator->nome }}</h4>
            </div>
            <div class="modal-body">
                <form role="form" method="post" action="{{ URL::action('CollaboratorController@relationJob', array($context->collaborator->id)) }}" name="form-add-job">
                   <div class="form-group">
                        <input type="hidden" data-input="id">
                        <label for="cargo">Cargo</label><span class="required"> *</span>
                        <input type="text" id="e1" name="job" data-input="job" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="cargo">Data de Vinculo</label><span class="required"> *</span>
                        <input type="text" class="form-control" id="data"  value="" data-input="date">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary" name="submit">Salvar Cargo</button>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@endsection

