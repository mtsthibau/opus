@extends('entries/template')

@section('styles')
{{ HTML::style('/styles/global.css') }}
@endsection


@section('backendContent')
<div class="page-header">
    <h2 id="btn-dropdowns">Colaboradores</h2>
</div>

<form role="form" action="{{ URL::action('CollaboratorController@index') }}" method="get">
    <h3>Filtrar</h3>

    <div class="row">
         <div class="col-md-4">
            <div class="form-group">
                <label>Código</label>
                <input type="text" class="form-control" placeholder="Código da Empresa" value="{{ $context->filters->codigo }}" name="codigo">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label>Nome</label>
                <input type="text" class="form-control" placeholder="Nome do Colaborador" value="{{ $context->filters->nome }}" name="nome">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label>CPF</label>
                <input type="text" class="form-control" placeholder="CPF do Colaborador" value="{{ $context->filters->cpf }}" name="cpf">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label>Por Página</label>
                <select class="form-control" name="max">
                    @foreach ($context->paginate as $key => $value)
                    <option value="{{ $key }}" @if ($context->filters->max == $key)selected="selected"@endif>{{ $value }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div>
        <button type="submit" class="btn btn-primary">Filtrar</button>
        <a class="btn btn-link" href="{{ URL::action('CollaboratorController@index') }}">Limpar Filtros</a>
        @if (Auth::user()->can('collaborator.item_edit'))
        <a href="{{ URL::action('CollaboratorController@create') }}" class="btn btn-success right" id="add-companies">
            <span class="glyphicon glyphicon-plus"></span> Adicionar Colaborador
        </a>
        @endif
    </div>
</form>

<div style="margin: 20px 0;">
    Exibindo <strong>{{ $context->collaborators->getFrom() }}</strong>
    a <strong>{{ $context->collaborators->getTo() }}</strong>
    de <strong>{{ $context->collaborators->getTotal() }}</strong> Colaborador(s).
</div>

<table class="table table-hover">
    <thead>
    <tr>
        <th>Código</th>
        <th>Nome</th>
        <th>CPF</th>
        @if (Auth::user()->can('collaborator.item_edit'))
        <th style="text-align:right;">Ações</th>
        @endif
    </tr>
    </thead>
    <tbody>
    @foreach ($context->collaborators as $collaborator)
    <tr>
        <td>
         <a href="{{ URL::action('CollaboratorController@detail', array($collaborator->id)) }}"> {{ $collaborator->codigo }} </a>
        </td>
        <td>
          <a href="{{ URL::action('CollaboratorController@detail', array($collaborator->id)) }}"> {{ $collaborator->nome  }}</a>
        </td>
         <td>
          <a href="{{ URL::action('CollaboratorController@detail', array($collaborator->id)) }}">{{ $collaborator->cpf }}</a>
        </td>
        @if (Auth::user()->can('collaborator.item_edit'))
        <td style="text-align:right;">
            <a href="{{ URL::action('CollaboratorController@edit', array($collaborator->id))}}"><button type="button" class="btn btn-info btn-xs" name="btn-edit-companies">Editar</button></a>
        </td>
        @endif
    </tr>
    @endforeach
    </tbody>
</table>
@endsection
