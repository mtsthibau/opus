@extends('entries/template')

@section('styles')
{{ HTML::style('/styles/global.css') }}
@endsection

@section('scripts')
{{ HTML::script('/scripts/apps/entries/companies.js') }}
@endsection

@section('backendContent')
<div class="page-header">
    <h1 id="btn-dropdowns">Colaboradores - Cadastro</h1>
</div>

<form class="form-horizontal" role="form" action="" method="post">
<h3>Dados do Colaborador</h3>
 <br/>
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Código<span class="required">*</span></label>
    <div class="codigo">
      <input type="text" class="form-control" id="inputEmail3" placeholder="Código do Colaborador" name="codigo" value="{{ $context->codigo }}">
       @if ($context->field_errors && $context->field_errors->has('codigo'))
          <p class="text-danger">{{ $context->field_errors->first('codigo') }}</p>
       @endif
   </div>
    <label for="inputEmail3" class="col-sm-2 control-label">CPF<span class="required">*</span></label>
    <div class="nome">
      <input type="text" class="form-control" id="cpf" placeholder="CPF do Colaborador" name="cpf" value="{{ $context->cpf }}" >
        @if ($context->field_errors && $context->field_errors->has('cpf'))
          <p class="text-danger">{{ $context->field_errors->first('cpf') }}</p>
       @endif
    </div>
  </div>
   <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Nome<span class="required">*</span></label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="inputEmail3" placeholder="Nome do Colaborador" name="nome" value="{{ $context->nome }}">
        @if ($context->field_errors && $context->field_errors->has('nome'))
          <p class="text-danger">{{ $context->field_errors->first('nome') }}</p>
       @endif
    </div>
  </div>
  <hr/>
  <h3>Endereço</h3>
 <br/>
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">CEP<span class="required">*</span></label>
    <div class="codigo">
      <input type="text" class="form-control" id="cep" placeholder="CEP da Rua" name="cep" value="{{ $context->cep }}">
        @if ($context->field_errors && $context->field_errors->has('cep'))
          <p class="text-danger">{{ $context->field_errors->first('cep') }}</p>
       @endif
    </div>
    <label for="inputEmail3" class="col-sm-2 control-label">Bairro<span class="required">*</span></label>
    <div class="nome">
      <input type="text" class="form-control" id="inputEmail3" placeholder="Bairro" name="district" value="{{ $context->district }}">
        @if ($context->field_errors && $context->field_errors->has('district'))
          <p class="text-danger">{{ $context->field_errors->first('district') }}</p>
       @endif
    </div>
  </div>
 
  
 <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Endereço<span class="required">*</span></label>
    <div class="codigo">
      <input type="text" class="form-control" id="inputEmail3" placeholder="Endereço" name="address" value="{{ $context->address }}">
        @if ($context->field_errors && $context->field_errors->has('address'))
          <p class="text-danger">{{ $context->field_errors->first('address') }}</p>
       @endif
    </div>
    <label for="inputEmail3" class="col-sm-2 control-label">Numero/Comp. <span class="required">*</span></label>
    <div class="nome">
      <input type="text" class="form-control" id="inputEmail3" placeholder="Número / Complemento" name="number" value="{{ $context->number }}">
        @if ($context->field_errors && $context->field_errors->has('number'))
          <p class="text-danger">{{ $context->field_errors->first('number') }}</p>
       @endif
    </div>
  </div>
  <div class="city">
    <label for="inputPassword3" class="col-sm-2 control-label">Cidade<span class="required">*</span></label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="inputPassword3" placeholder="Cidade" name="city" value="{{ $context->city }}">
        @if ($context->field_errors && $context->field_errors->has('city'))
          <p class="text-danger">{{ $context->field_errors->first('city') }}</p>
       @endif
    </div>
  </div>
  <div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label">UF<span class="required">*</span></label>
    <div class="uf">     
        <select class="form-control" name="uf">
              <option selected></option>
              @foreach($context->estados as $estado)
                @if ($estado == $context->uf)
                <option selected="selected">{{ $estado }}</option>
                @else
                <option>{{ $estado }}</option>
                @endif
              @endforeach
         </select>
           @if ($context->field_errors && $context->field_errors->has('uf'))
          <p class="text-danger">{{ $context->field_errors->first('uf') }}</p>
       @endif
        </div>
</div>
 <hr/>
  <h3>Contato</h3>
 <br/>
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Telefone<span class="required">*</span></label>
    <div class="codigo">
      <input type="text" class="form-control" id="telefone" placeholder="Telefone" name="phone" value="{{ $context->phone }}">
        @if ($context->field_errors && $context->field_errors->has('phone'))
          <p class="text-danger">{{ $context->field_errors->first('phone') }}</p>
       @endif
    </div>
    <label for="inputEmail3" class="col-sm-2 control-label">E-mail<span class="required">*</span></label>
    <div class="nome">
      <input type="email" class="form-control" id="inputEmail3" placeholder="E-mail" name="email" value="{{ $context->email }}">
        @if ($context->field_errors && $context->field_errors->has('email'))
          <p class="text-danger">{{ $context->field_errors->first('email') }}</p>
       @endif
    </div>
  </div>
    <hr/>
    <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <input type="submit" name="submit" class="btn btn-primary" value="Cadastrar Colaborador">
      <a href="{{ URL::action('CollaboratorController@create') }}"><button type="button" class="btn btn-link">Limpar Campos</button></a>
    </div>
  </div>
</form>

@endsection
