@extends('entries/template')

@section('styles')
{{ HTML::style('/styles/global.css') }}
{{ HTML::style('/styles/select2.css') }}
{{ HTML::style('/styles/select2-bootstrap.css') }}
@endsection

@section('scripts')
{{ HTML::script('/scripts/apps/relations/dealing.js') }}
{{ HTML::script('/scripts/apps/relations/exam.js') }}
{{ HTML::script('/scripts/apps/entries/companies.js') }}
{{ HTML::script('/scripts/apps/relations/collaborators.js') }}

<script>
$("#e1").select2({
    minimumInputLength: 1,
    data: {{ json_encode($context->exams) }}
    
});   

 $("#e2").select2({
    minimumInputLength: 1,
    data: {{ json_encode($context->dealings) }}
    
    
});    

$("#e3").select2({
    minimumInputLength: 1,
    data: {{ json_encode($context->collaborators) }}
    
    
});    
</script>


@endsection




@section('backendContent')
<div class="page-header">
    <h1 id="btn-dropdowns">Detalhamento Empresa</h1>
</div>

<blockquote>
<div>
    <h3><span class="glyphicon glyphicon-tower"></span> {{ $context->companie->companie }}</h3>
    <div class="right">
    @if (Auth::user()->can('companie.item_edit'))
    <a href="{{ URL::action('CompaniesController@edit', array($context->companie->id))}}"><button name="submit" class="btn btn-info">  <span class="glyphicon glyphicon-edit"></span> Editar Dados da Empresa</button></a>
    @endif
    </div>
</div>
<br/>
<hr/>

     <dl class="dl-horizontal">
          <h4>Dados</h4>
          <div class="center">
              <dt>Código:</dt>
              <dd>{{ $context->companie->codigo }}</dd>
              <dt>Nome:</dt>
              <dd>{{ $context->companie->companie }}</dd>
              <dt>CNPJ:</dt>
              <dd>{{ $context->companie->cnpj }}</dd>
          </div>
          <hr/>
          <h4>Endereço</h4>
          <div class="center">
              <dt>CEP:</dt>
              <dd>{{ $context->companie->cep }}</dd>
              <dt>Bairro:</dt>
              <dd>{{ $context->companie->district }}</dd>
              <dt>Endereço:</dt>
              <dd>{{ $context->companie->address }}</dd>
               <dt>Número/Complemento:</dt>
              <dd>{{ $context->companie->number }}<dd>
               <dt>Cidade:</dt>
              <dd>{{ $context->companie->city }}</dd>
              <dt>UF:</dt>
              <dd>{{ $context->companie->uf }}</dd>
          </div>
          <hr/>
          <h4>Contato</h4>
          <div class="center">
              <dt>Telefone:</dt>
              <dd>{{ $context->companie->phone }}</dd>
               <dt>E-mail:</dt>
              <dd>{{ $context->companie->email }}</dd>
          </div>     
             <hr/>
          <div>
            <h4>Cargos da Empresa</h4>
            <h5>    Área de cadastro e exclusão de cargos existentes na empresa.<br/> Cargos servem para verificação de exames necessários para cada tipo de profissional.</h5>
          <div class="right">
                <!--CARGOS-->
                <a href="{{ URL::action('RelationjobsController@index', array($context->companie->id)) }}" class="btn btn-primary">
                   <span class="glyphicon glyphicon-briefcase"> </span> Cargos da Empresa
                </a>
                </div>
            <br/>
          </div>
          <hr/>
          <div>
            <h4>Exames</h4>
             @if (Auth::user()->can('companie.item_edit'))
              <a data-toggle="modal"  data-target="#modal-adicionar" href="" class="btn btn-success right">
                   <span class="glyphicon glyphicon-plus"></span> Adicionar
              </a>
              @endif
         </div>
<br/>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Exames</th>
            <th>Preço</th>
            @if (Auth::user()->can('companie.item_edit'))
            <th style="text-align:right;">Ações</th>
            @endif
        </tr>
        </thead>
        <tbody>
            @foreach ($context->companie->exams as $exam)
            <tr>
                <td>
                  {{ $exam->exam->exam }} 
                </td>
                <td>
                  R${{ $exam->price }}
                </td>
                @if (Auth::user()->can('companie.item_edit'))
                <td style="text-align:right;">
                    <button type="button" class="btn btn-info btn-xs" name="btn-edit-exam" data-url="{{ URL::action('CompaniesController@editRelation') }}" data-id="{{ $exam->id }}"  data-exam_id="{{ $exam->exam_id }}" data-price="{{ $exam->price }}" data-exam="{{ $exam->exam->exam }}">Editar</button>
                    <button type="button" class="btn btn-warning btn-xs" name="btn-del-exam" data-url="{{ URL::action('CompaniesController@delRelation', array($exam->id)) }}">Excluir</button>
                </td>
                @endif    
            </tr>
            @endforeach
        </tbody>
    </table>
    <br/>
    <hr/>
     <div>
            <h4>Procedimentos</h4>
            @if (Auth::user()->can('companie.item_edit'))
              <a data-toggle="modal" data-target="#modal-dealing" href="" class="btn btn-success right">
                   <span class="glyphicon glyphicon-plus"></span> Adicionar
              </a>
            @endif
         </div>
    
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Procedimentos</th>
            <th>Preço</th>
            @if (Auth::user()->can('companie.item_edit'))
            <th style="text-align:right;">Ações</th>
            @endif
        </tr>
        </thead>
        <tbody>
            @foreach ($context->companie->dealings as $dealing)
            <tr>
                <td>
                  {{ $dealing->dealing->dealing }} 
                </td>
                <td>
                  R${{ $dealing->price }}
                </td>
                @if (Auth::user()->can('companie.item_edit'))
                <td style="text-align:right;">
                    <button type="button" class="btn btn-info btn-xs" name="btn-edit-dealing" data-url="{{ URL::action('CompaniesController@editDealing') }}" data-id="{{ $dealing->id }}", data-price="{{ $dealing->price }}", data-dealing="{{ $dealing->dealing->dealing }}">Editar</button>
                    <button type="button" class="btn btn-warning btn-xs" name="btn-del-dealing" data-url="{{ URL::action('CompaniesController@delDealing', array($dealing->id)) }}">Excluir</button>
                </td>
                @endif 
            </tr>
            @endforeach
        </tbody>
    </table>
    <br/>
    <hr/>
    <div>
            <h4>Colaboradores</h4>
              @if (Auth::user()->can('companie.item_edit'))
              <a data-toggle="modal" data-target="#modal-collaborator" href="" class="btn btn-success right">
                   <span class="glyphicon glyphicon-plus"></span> Adicionar
              </a>
              @endif
         </div>
    
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Colaboradores</th>
            <th>Data de Vinculação</th>
            @if (Auth::user()->can('companie.item_edit'))
            <th style="text-align:right;">Ações</th>
            @endif
        </tr>
        </thead>

        <tbody>

        @foreach ($context->companie->collaborators as $collaborator)
        <tr>
            <td>
              {{ $collaborator->collaborator->nome }} 
            </td>
            <td>
              {{ $collaborator->date }}
            </td>
            @if (Auth::user()->can('companie.item_edit'))
            <td style="text-align:right;">
                <button type="button" class="btn btn-info btn-xs" name="btn-edit-collaborator" data-url="{{ URL::action('CompaniesController@editCollaborator') }}" data-id="{{ $collaborator->id }}", data-nome="{{ $collaborator->nome }}", data-date="{{ $collaborator->date }}", data-collaborator="{{ $collaborator->collaborator->nome }}" >Editar</button>
                <button type="button" class="btn btn-warning btn-xs" name="btn-del-collaborator" data-url="{{ URL::action('CompaniesController@delCollaborator', array($collaborator->id)) }}">Excluir</button>
            </td>
            @endif
        </tr>
        @endforeach
        </tbody>
    </table>
    </dl>
</blockquote>


<!--MODAL'S------->

<div class="modal fade" id="modal-adicionar" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Adicionar Exame</h4>
            </div>
            <div class="modal-body">
                <form role="form" method="post" action="{{ URL::action('CompaniesController@relationExam', array($context->companie->id)) }}" name="form-add-exam">
                   <div class="form-group">
                        <input type="hidden" data-input="id">
                        <label for="cargo">Exame</label><span class="required"> *</span>
                           <input type="text" class="form-control" id="e1" data-input="exam" name="exam">
                    </div>
                    <div class="form-group">
                        <label for="cargo">Preço do Exame</label><span class="required"> *</span>
                        <input type="text" class="form-control"  value="" data-input="price" name="price">
                    </div>
                  
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary" name="submit">Salvar Exame</button>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" id="modal-edit-exam" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Editar Preço do Exame  <input type="button" class="btn btn-link" data-input="exam"></h4>
            </div>
            <div class="modal-body">
                <form role="form" method="post" action="{{ URL::action('CompaniesController@editRelation')}}" name="form-edit-exam">
                    <input type="hidden" data-input="id">
                    <div class="form-group">
                        <label for="cargo">Preço do Exam</label><span class="required"> *</span>
                        <input type="text" class="form-control"  value="R$" data-input="price">
                    </div>                  
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary" name="submit">Salvar Procedimento</button>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" id="modal-dealing" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Adicionar Procedimento</h4>
            </div>
            <div class="modal-body">
                <form role="form" method="post" action="{{ URL::action('CompaniesController@relationDealing', array($context->companie->id)) }}" name="form-add-dealing">
                    <div class="form-group">
                        <input type="hidden" data-input="id">
                        <label for="cargo">Procedimento</label><span class="required"> *</span>
                        <input type="text" class="form-control" id="e2" data-input="dealing" name="dealing">
                    </div>
                    <div class="form-group">
                        <label for="cargo">Preço do Procedimento</label><span class="required"> *</span>
                        <input type="text" class="form-control"  value="" data-input="price" name="price">
                    </div>
                  
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary" name="submit">Salvar Procedimento</button>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" id="modal-edit-dealing" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Editar Preço do Procedimento <input type="button" class="btn btn-link" data-input="dealing"></h4></h4>
            </div>
            <div class="modal-body">
                <form role="form" method="post" action="{{ URL::action('CompaniesController@editDealing') }}" name="form-edit-dealing">                  
                    <div class="form-group">
                        <input type="hidden" data-input="id">
                        <label for="cargo">Preço do Procedimento</label><span class="required"> *</span>
                        <input type="text" class="form-control"  value="R$" data-input="price">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary" name="submit">Salvar Procedimento</button>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<div class="modal fade" id="modal-collaborator" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Adicionar Colaborador</h4>
            </div>
            <div class="modal-body">
                <form role="form" method="post" action="{{ URL::action('CompaniesController@relationCollaborator', array($context->companie->id)) }}" name="form-add-collaborator">
                    <div class="form-group">
                        <input type="hidden" data-input="id">
                        <label for="cargo">Colaborador</label><span class="required"> *</span>
                        <input type="text" class="form-control" id="e3" data-input="collaborator_id" name="collaborator_id">
                    </div>
                    <div class="form-group">
                        <label for="cargo">Data do Vinculo</label><span class="required"> *</span>
                        <input type="text" class="form-control" id="data" value="" data-input="date" name="date">
                    </div>
                  
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary" name="submit">Salvar Colaborador</button>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" id="modal-edit-collaborator" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Editar Colaborador <input type="button" class="btn btn-link" data-input="collaborator"></h4>
            </div>
            <div class="modal-body">
                <form role="form" method="post" action="{{ URL::action('CompaniesController@editCollaborator') }}" name="form-edit-collaborator">                  
                    <div class="form-group">
                        <input type="hidden" data-input="id">
                        <label for="cargo">Data do Vinculo</label><span class="required"> *</span>
                        <input type="text" class="form-control" id="date" value="" data-input="date" name="date">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary" name="submit">Salvar Procedimento</button>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection

