@extends('entries/template')

@section('styles')
{{ HTML::style('/styles/global.css') }}
@endsection

@section('backendContent')
<div class="page-header">
    <h1 id="btn-dropdowns">Detalhamento Profissional</h1>
</div>
<blockquote>
    <h3><span class="glyphicon glyphicon-user"></span> {{ $context->professional->nome }}</h3>
    <div class="right">
    @if (Auth::user()->can('professional.item_edit'))
     <a href="{{ URL::action('ProfessionalController@edit', array($context->professional->id))}}"><button name="submit" class="btn btn-info"><span class="glyphicon glyphicon-edit"></span> Editar Dados do Profissional</button></a>
    @endif
    </div>
    <br/>
    <hr/>
     <dl class="dl-horizontal">
          <h4>Dados</h4>
          <div class ="center">
              <dt>Código:</dt>
              <dd>{{ $context->professional->codigo }}</dd>
              <dt>Nome:</dt>
              <dd>{{ $context->professional->nome }}</dd>
              <dt>Tipo de Conselho:</dt>
              <dd>{{ $context->professional->tipo_conselho }}</dd>
               <dt>Nímero do Conselho:</dt>
              <dd>{{ $context->professional->numero_conselho }}</dd>  
         </div>
              <h4>Contato</h4>
              <div class ="center">
                  <dt>Telefone Fixo:</dt>
                  <dd>{{ $context->professional->telefone }}</dd>  
                  <dt>Celular:</dt>
                  <dd>{{ $context->professional->celular }}</dd>  
                  <dt>E-mail:</dt>
                  <dd>{{ $context->professional->email }}</dd>
             </div>
     </dl>
</blockquote>

@endsection

