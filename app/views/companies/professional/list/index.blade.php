@extends('entries/template')

@section('styles')
{{ HTML::style('/styles/global.css') }}
@endsection

@section('scripts')
{{ HTML::script('/scripts/apps/companies/list/list.js') }}
@endsection

@section('backendContent')
<div class="page-header">
    <h1 id="btn-dropdowns">Profissionais</h1>
</div>

<form role="form" action="{{ URL::action('ProfessionalController@index') }}" method="get">
    <h3>Filtrar</h3>

    <div class="row">
         <div class="col-md-4">
            <div class="form-group">
                <label>Código</label>
                <input type="text" class="form-control" placeholder="Código da Empresa" value="{{ $context->filters->codigo }}" name="codigo">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label>Nome</label>
                <input type="text" class="form-control" placeholder="Nome do Profissional" value="{{ $context->filters->nome }}" name="nome">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label>Tipo de Conselho</label>
                <input type="text" class="form-control" placeholder="Tipo de Conselho do Profissional" value="{{ $context->filters->tipo_conselho }}" name="tipo_conselho">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label>Por Página</label>
                <select class="form-control" name="max">
                    @foreach ($context->paginate as $key => $value)
                    <option value="{{ $key }}" @if ($context->filters->max == $key)selected="selected"@endif>{{ $value }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div>
        <button type="submit" class="btn btn-primary">Filtrar</button>
        <a class="btn btn-link" href="{{ URL::action('CompaniesController@index') }}">Limpar Filtros</a>
        @if (Auth::user()->can('professional.item_edit'))
        <a href="{{ URL::action('ProfessionalController@create') }}" class="btn btn-success right" id="add-companies">
            <span class="glyphicon glyphicon-plus"></span> Adicionar Profissional
        </a>
        @endif
    </div>
</form>

<div style="margin: 20px 0;">
    Exibindo <strong>{{ $context->professionals->getFrom() }}</strong>
    a <strong>{{ $context->professionals->getTo() }}</strong>
    de <strong>{{ $context->professionals->getTotal() }}</strong> Profissional(s).
</div>

<table class="table table-hover">
    <thead>
    <tr>
        <th>Código</th>
        <th>Nome Profissional</th>
        <th>Tipo de Conselho</th>
        @if (Auth::user()->can('professional.item_edit'))
        <th style="text-align:right;">Ações</th>
        @endif
    </tr>
    </thead>
    <tbody>
    @foreach ($context->professionals as $professional)
    <tr>
        <td>
         <a href="{{ URL::action('ProfessionalController@detail', array($professional->id)) }}"> {{ $professional->codigo }} </a>
        </td>
        <td>
          <a href="{{ URL::action('ProfessionalController@detail', array($professional->id)) }}"> {{ $professional->nome  }}</a>
        </td>
         <td>
          <a href="{{ URL::action('ProfessionalController@detail', array($professional->id)) }}">{{ $professional->tipo_conselho }}</a>
        </td>
        @if (Auth::user()->can('professional.item_edit'))
        <td style="text-align:right;">
            <a href="{{ URL::action('ProfessionalController@edit', array($professional->id))}}"><button type="button" class="btn btn-info btn-xs" name="btn-edit-companies">Editar</button></a>
        </td>
        @endif
    </tr>
    @endforeach
    </tbody>
</table>
@endsection
