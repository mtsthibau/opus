@extends('entries/template')

@section('styles')
{{ HTML::style('/styles/global.css') }}
@endsection

@section('scripts')
{{ HTML::script('/scripts/apps/entries/companies.js') }}
@endsection

@section('backendContent')
<div class="page-header">
    <h1 id="btn-dropdowns">Profissionais - Cadastro</h1>
</div>

<form class="form-horizontal" role="form" action="" method="post">
<h3>Dados do Profissional</h3>
 <br/>
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Código<span class="required">*</span></label>
    <div class="codigo">
      <input type="text" class="form-control" id="inputEmail3" placeholder="Código do Profissional" name="codigo" value="{{ $context->codigo }}">
       @if ($context->field_errors && $context->field_errors->has('codigo'))
          <p class="text-danger">{{ $context->field_errors->first('codigo') }}</p>
       @endif
   </div>
  </div>
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Nome<span class="required">*</span></label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="inputEmail3" placeholder="Nome do Profissional" name="nome" value="{{ $context->nome }}">
        @if ($context->field_errors && $context->field_errors->has('nome'))
          <p class="text-danger">{{ $context->field_errors->first('nome') }}</p>
       @endif
    </div>
  </div>
    <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">N° do Conselho<span class="required">*</span></label>
    <div class="codigo">
      <input type="text" class="form-control" id="inputEmail3" placeholder="Número do Conselho" name="numero_conselho" value="{{ $context->numero_conselho }}">
       @if ($context->field_errors && $context->field_errors->has('numero_conselho'))
          <p class="text-danger">{{ $context->field_errors->first('numero_conselho') }}</p>
       @endif
   </div>
    <label for="inputEmail3" class="col-sm-2 control-label">Conselho<span class="required">*</span></label>
    <div class="nome">
      <input type="text" class="form-control" id="inputEmail3" placeholder="Tipo de Conselho do Profissional" name="tipo_conselho" value="{{ $context->tipo_conselho }}" >
        @if ($context->field_errors && $context->field_errors->has('tipo_conselho'))
          <p class="text-danger">{{ $context->field_errors->first('tipo_conselho') }}</p>
       @endif
    </div>  
  </div>
  <hr/>
    <h3>Contato</h3>
    <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Telefone Fixo<span class="required">*</span></label>
        <div class="codigo">
            <input type="text" class="form-control" id="telefone" placeholder="(31)3333-3333" name="telefone" value="{{ $context->telefone }}">
            @if ($context->field_errors && $context->field_errors->has('telefone'))
            <p class="text-danger">{{ $context->field_errors->first('telefone') }}</p>
            @endif
        </div>  
         <label for="inputEmail3" class="col-sm-2 control-label">E-mail<span class="required">*</span></label>
        <div class="nome">
            <input type="email" class="form-control" id="inputEmail3" placeholder="exemplo@gmail.com" name="email" value="{{ $context->email }}">
            @if ($context->field_errors && $context->field_errors->has('email'))
            <p class="text-danger">{{ $context->field_errors->first('email') }}</p>
            @endif
        </div>  
    </div>  
    <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Celular<span class="required">*</span></label>
        <div class="codigo">
            <input type="text" class="form-control" id="celular" placeholder="(31)9999-9999" name="celular" value="{{ $context->celular }}">
            @if ($context->field_errors && $context->field_errors->has('celular'))
            <p class="text-danger">{{ $context->field_errors->first('celular') }}</p>
            @endif
        </div>  
    </div>  
  <hr/>
    <br/>
    <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <input type="submit" name="submit" class="btn btn-primary" value="Cadastrar Profissional">
      <a href="{{ URL::action('ProfessionalController@create') }}"><button type="button" class="btn btn-link">Limpar Campos</button></a>
    </div>
  </div>
</form>

@endsection
