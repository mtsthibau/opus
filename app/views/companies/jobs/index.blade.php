@extends('entries/template')

@section('styles')
{{ HTML::style('/styles/global.css') }}
{{ HTML::style('/styles/select2.css') }}
{{ HTML::style('/styles/select2-bootstrap.css') }}

@endsection

@section('scripts')
{{ HTML::script('/scripts/apps/relations/jobs.js') }}

<script>
$("#e5").select2({
    minimumInputLength: 1,
    data: {{ json_encode($context->jobs) }}
    
});    
</script>

@endsection


@section('backendContent')
    <div class="page-header">
        <h1 id="btn-dropdowns">{{ $context->companie->companie }} - Cargos</h1>
    </div>   
    <br/>
            <h3><span class="glyphicon glyphicon-briefcase"></span> Tabela de Cargos da Empresa</h3>
    <div class="right">
    @if (Auth::user()->can('companie.item_edit'))
        <a data-toggle="modal"  data-target="#modal-adicionar" href="" class="btn btn-success right">
            <span class="glyphicon glyphicon-plus"></span> Adicionar Cargo
        </a>
    @endif    
    </div> 
    <br/>
            <hr/>
<br/>
    <blockquote>
    <br/>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Código</th>
            <th>Cargo</th>
            <th>Função</th>
            @if (Auth::user()->can('companie.item_edit'))
            <th style="text-align:right;">Excluir</th>
            @endif
        </tr>
        </thead>
        <tbody>
        @foreach ($context->companie->jobs as $job)
        <tr>
            <td>
              {{ $job->job->codigo }}
            </td>
            <td>
              {{ Str::limit($job->job->job, $limit = 32, $end = '...') }}
            </td>
            <td>
              {{ Str::limit($job->job->function, $limit = 45, $end = '...') }}
            </td>
            @if (Auth::user()->can('companie.item_edit'))
            <td style="text-align:right;">
                <button type="button" class="btn btn-warning btn-xs" name="btn-del-job" data-url="{{ URL::action('RelationjobsController@delete', array($job->id)) }}" data-job="{{ $job->job }}">Excluir</button>
            </td>
            @endif
        </tr>
        @endforeach
        </tbody>
    </table>
</blockquote>


<div class="modal fade" id="modal-adicionar" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Adicionar Cargo</h4>
            </div>
            <div class="modal-body">
                <form role="form" method="post" action="{{ URL::action('RelationjobsController@create', array($context->companie->id)) }}" name="form-add-job">
                    <div class="form-group">
                        <input type="hidden" data-input="id">
                        <label for="cargo">Cargo</label><span class="required"> *</span>
                           <input type="text" class="form-control" id="e5" data-input="job" name="job">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary" name="submit">Salvar Cargo</button>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


@endsection
