@extends('entries/template')

@section('styles')
{{ HTML::style('/styles/global.css') }}
@endsection

@section('scripts')
{{ HTML::script('/scripts/apps/entries/exams.js') }}
@endsection

@section('backendContent')
<div class="page-header">
    <h1 id="btn-dropdowns">Exames</h1>
</div>

<form role="form" action="{{ URL::action('ExamsController@index') }}" method="get">
    <h3>Filtrar</h3>

    <div class="row">
         <div class="col-md-4">
            <div class="form-group">
                <label>Código</label>
                <input type="text" class="form-control" placeholder="Código do Exame" value="{{ $context->filters->codigo }}" name="codigo">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label>Exame</label>
                <input type="text" class="form-control" placeholder="Exame" value="{{ $context->filters->exam }}" name="exam">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label>Por Página</label>
                <select class="form-control" name="max">
                    @foreach ($context->paginate as $key => $value)
                    <option value="{{ $key }}" @if ($context->filters->max == $key)selected="selected"@endif>{{ $value }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div>
        <button type="submit" class="btn btn-primary">Filtrar</button>
        <a class="btn btn-link" href="{{ URL::action('ExamsController@index') }}">Limpar Filtros</a>
        @if (Auth::user()->can('exam.item_edit'))
        <a data-toggle="modal"  data-target="#modal" href="" class="btn btn-success right" id="add-exam">
            <span class="glyphicon glyphicon-plus"></span> Adicionar Exame
        </a>
        @endif
    </div>
        
</form>

<div style="margin: 20px 0;">
    Exibindo <strong>{{ $context->exams->getFrom() }}</strong>
    a <strong>{{ $context->exams->getTo() }}</strong>
    de <strong>{{ $context->exams->getTotal() }}</strong> Exame(s).
</div>

<table class="table table-hover">
    <thead>
    <tr>
        <th>Código</th>
        <th>Exame</th>
        <th>Descrição</th>
        @if (Auth::user()->can('exam.item_edit'))
        <th style="text-align:right;">Ações</th>
        @endif
    </tr>
    </thead>
    <tbody>
    @foreach ($context->exams as $exam)
    <tr>
        <td>
          {{ $exam->codigo }}
        </td>
        <td>
          {{ Str::limit($exam->exam, $limit = 32, $end = '...') }}
        </td>
        <td>
          {{ Str::limit($exam->description, $limit = 40, $end = '...') }}
        </td>
        @if (Auth::user()->can('exam.item_edit'))
        <td style="text-align:right;">
            <button type="button" class="btn btn-info btn-xs" name="btn-edit-exam" data-id="{{ $exam->id }}" data-codigo="{{ $exam->codigo }}" data-exam="{{ $exam->exam }}" data-description="{{ $exam->description }}">Editar</button>
            <button type="button" class="btn btn-warning btn-xs" name="btn-del-exam" data-url="{{ URL::action('ExamsController@delete', array($exam->id)) }}" data-exam="{{ $exam->exam }}">Excluir</button>
        </td>
        @endif
    </tr>
    @endforeach
    </tbody>
</table>

<div class="modal fade" id="modal" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Novo Exame</h4>
            </div>
            <div class="modal-body">
                <form role="form" method="post" action="{{ URL::action('ExamsController@create') }}" name="form-add-exam">
                     <input type="hidden" data-input="id">
                     <div class="form-group">
                        <label for="cargo">Código</label><span class="required"> *</span>
                        <input type="text" class="form-control"  value="" data-input="codigo">
                    </div>
                     <div class="form-group">
                        <label for="cargo">Exame</label><span class="required"> *</span>
                        <input type="text" class="form-control"  value="" data-input="exam">
                    </div>

                    <div class="form-group">
                        <label for="function">Descrição</label><span class="required"> *</span>
                        <input type="text" class="form-control" value="" data-input="description">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Salvar Exame</button>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<div class="modal fade" id="modal-edit-exam" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Editar Exame</h4>
            </div>
            <div class="modal-body">
                <form role="form" method="post" action="{{ URL::action('ExamsController@edit') }}" name="form-edit-exam">
                    <input type="hidden" data-input="id">
                    <div class="form-group">
                        <label for="cargo">Código</label><span class="required"> *</span>
                        <input type="text" disabled class="form-control"  value="" data-input="codigo">
                    </div>
                    <div class="form-group">
                        <label for="cargo">Exame</label><span class="required"> *</span>
                        <input type="text" class="form-control"  value="" data-input="exam">
                    </div>
                    <div class="form-group">
                        <label for="function">Descrição</label><span class="required"> *</span>
                        <input type="text" class="form-control" value="" data-input="description">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Salvar Alteração</button>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@endsection
