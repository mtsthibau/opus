@extends('entries/template')

@section('styles')
{{ HTML::style('/styles/global.css') }}
@endsection

@section('scripts')
{{ HTML::script('/scripts/apps/entries/jobs.js') }}
@endsection

@section('backendContent')
<div class="page-header">
    <h1 id="btn-dropdowns">Cargos e Funções</h1>
</div>

<form role="form" action="{{ URL::action('JobController@index') }}" method="get">
    <h3>Filtrar</h3>

    <div class="row">

        <div class="col-md-4">
            <div class="form-group">
                <label>Código</label>
                <input type="text" class="form-control" placeholder="Código" value="{{ $context->filters->codigo }}" name="codigo">
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label>Cargo</label>
                <input type="text" class="form-control" placeholder="Cargo" value="{{ $context->filters->job }}" name="job">
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label>Por Página</label>
                <select class="form-control" name="max">
                    @foreach ($context->paginate as $key => $value)
                    <option value="{{ $key }}" @if ($context->filters->max == $key)selected="selected"@endif>{{ $value }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div>
        <button type="submit" class="btn btn-primary">Filtrar</button>
        <a class="btn btn-link" href="{{ URL::action('JobController@index') }}">Limpar Filtros</a>
        @if (Auth::user()->can('job.item_edit'))
        <a data-toggle="modal"  data-target="#modal" href="javascript:void(0);" class="btn btn-success right" id="add-job">
            <span class="glyphicon glyphicon-plus"></span> Adicionar Cargo
        </a>
        @endif
    </div>
    
</form>

<div style="margin: 20px 0;">
    Exibindo <strong>{{ $context->jobs->getFrom() }}</strong>
    a <strong>{{ $context->jobs->getTo() }}</strong>
    de <strong>{{ $context->jobs->getTotal() }}</strong> Cargo(s).
</div>
<table class="table table-hover">
    <thead>
    <tr>
        <th>Código</th>
        <th>Cargo</th>
        <th>Função</th>
        @if (Auth::user()->can('job.item_edit'))
        <th style="text-align:right;">Ações</th>
        @endif
    </tr>
    </thead>
    <tbody>
    @foreach ($context->jobs as $job)
    <tr>
        <td>
            <a href="{{ URL::action('JobController@listExam', array($job->id)) }}">{{ $job->codigo }}</a>
        </td>
        <td>
             <a href="{{ URL::action('JobController@listExam', array($job->id)) }}">{{ $job->job }}</a>
        </td>
        <td>
           <a href="{{ URL::action('JobController@listExam', array($job->id)) }}">{{Str::limit($job->function , $limit = 60, $end = '...') }}</a>
        </td>
        @if (Auth::user()->can('job.item_edit'))
        <td style="text-align:right;">
            <button type="button" class="btn btn-info btn-xs" name="btn-edit-job" data-codigo="{{ $job->codigo }}" data-id="{{ $job->id }}"  data-job="{{ $job->job }}" data-function="{{ $job->function }}">Editar</button>
            <button type="button" class="btn btn-warning btn-xs" name="btn-del-job" data-url="{{ URL::action('JobController@delete', array($job->id)) }}" data-job="{{ $job->job }}">Excluir</button>
        </td>
        @endif
    </tr>
    @endforeach
    </tbody>
</table>

<div class="modal fade" id="modal" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Novo Cargo</h4>
            </div>
            <div class="modal-body">
                <form role="form" method="post" action="{{ URL::action('JobController@create') }}" name="form-add-job">
                    <div class="form-group">
                        <label for="cargo">Código</label><span class="required"> *</span>
                        <input type="text" class="form-control"  value="" data-input="codigo">
                    </div>

                    <div class="form-group">
                        <label for="cargo">Cargo</label><span class="required"> *</span>
                        <input type="text" class="form-control"  value="" data-input="job">
                    </div>

                    <div class="form-group">
                        <label for="function">Função</label><span class="required"> *</span>
                        <input type="text" class="form-control" value="" data-input="function">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Salvar Cargo</button>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<div class="modal fade" id="modal-edit-job" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Editar Cargo</h4>
            </div>
            <div class="modal-body">
                <form role="form" method="post" action="{{ URL::action('JobController@edit') }}" name="form-edit-job">
                    <input type="hidden" data-input="id" value="">
                    <div class="form-group">
                        <label for="cargo">Código</label><span class="required"> *</span>
                        <input id="disabledInput" disabled class="form-control"  value="" data-input="codigo">
                    </div>

                    <div class="form-group">
                        <label for="cargo">Cargo</label><span class="required"> *</span>
                        <input type="text" class="form-control"  value="" data-input="job">
                    </div>

                    <div class="form-group">
                        <label for="function">Função</label><span class="required"> *</span>
                        <input type="text" class="form-control" value="" data-input="function">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Salvar Alteração</button>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@endsection
