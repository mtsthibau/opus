@extends('entries/template')

@section('styles')
{{ HTML::style('/styles/global.css') }}
{{ HTML::style('/styles/select2.css') }}
{{ HTML::style('/styles/select2-bootstrap.css') }}
@endsection

@section('scripts')
{{ HTML::script('/scripts/apps/entries/job_exam.js') }}
{{ HTML::script('/scripts/apps/entries/job_dealing.js') }}



<script>
$("#e1").select2({
    minimumInputLength: 1,
    data: {{ json_encode($context->exams) }}
    
});   

 $("#e2").select2({
    minimumInputLength: 1,
    data: {{ json_encode($context->dealings) }}
    
    
});    
</script>
@endsection


@section('backendContent')
<div class="page-header">
    <h1 id="btn-dropdowns">{{ $context->job->job }} - Detalhamento</h1>
</div>
<br/>
    <div>
        @if (Auth::user()->can('job.item_edit'))
        <a data-toggle="modal"  data-target="#modal" href="javascript:void(0);" class="btn btn-success right" id="add-exam">
            <span class="glyphicon glyphicon-plus"></span> Adicionar
        </a>
        @endif
    </div>
    <h4>Exames</h4>
    <br/>
    
    <table class="table table-hover">
    <thead>
    <tr>
        <th>Código</th>
        <th>Exame</th>
        <th>Descrição</th>
        @if (Auth::user()->can('job.item_edit'))
        <th style="text-align:right;">Ações</th>
        @endif
    </tr>
    </thead>
    <tbody>
    @foreach ($context->job->exams as $exam)
    <tr>
        <td>
            {{ $exam->exam->codigo }}
        </td>
        <td>
            {{ $exam->exam->exam }}
        </td>
        <td>
           {{Str::limit($exam->exam->description , $limit = 60, $end = '...') }}
        </td>
        @if (Auth::user()->can('job.item_edit'))
        <td style="text-align:right;">
            <button type="button" class="btn btn-warning btn-xs" name="btn-del-exam" data-url="{{ URL::action('JobController@delRelationExam', array($exam->id)) }}">Excluir</button>
        </td>
        @endif
    </tr>
    @endforeach
    </tbody>
</table>
<br/>
<hr/>

<div class="modal fade" id="modal" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Novo Exame</h4>
            </div>
            <div class="modal-body">
                <form role="form" method="post" action="{{ URL::action('JobController@relationExam', array($context->job->id)) }}" name="form-add-exam">
                   <input type="hidden" data-input="id" value="">
                   <div class="form-group">
                    <input type="text" class="form-control" id="e1" data-input="exam" name="exam">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary" name="submit">Salvar Exame</button>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<br/>

    <div>
        @if (Auth::user()->can('job.item_edit'))
        <a data-toggle="modal"  data-target="#modal-dealing" href="javascript:void(0);" class="btn btn-success right" id="add-dealing">
            <span class="glyphicon glyphicon-plus"></span> Adicionar
        </a>
        @endif
    </div>
    <h4>Procedimentos</h4>
    <br/>
    
    <table class="table table-hover">
    <thead>
        <tr>
            <th>Código</th>
            <th>Exame</th>
            <th>Descrição</th>
            @if (Auth::user()->can('job.item_edit'))
            <th style="text-align:right;">Ações</th>
            @endif
        </tr>
    </thead>
    <tbody>
    @foreach ($context->job->dealings as $dealing)
    <tr>
        <td>
            {{ $dealing->dealing->codigo }}
        </td>
        <td>
            {{ $dealing->dealing->dealing }}
        </td>
        <td>
           {{Str::limit($dealing->dealing->description , $limit = 40, $end = '...') }}
        </td>
        @if (Auth::user()->can('job.item_edit'))
        <td style="text-align:right;">
            <button type="button" class="btn btn-warning btn-xs" name="btn-del-dealing" data-url="{{ URL::action('JobController@delRelationDealing', array($dealing->id)) }}">Excluir</button>
        </td>
        @endif
    </tr>
    @endforeach
    </tbody>
</table>

<div class="modal fade" id="modal-dealing" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Novo Procedimento</h4>
            </div>
            <div class="modal-body">
                <form role="form" method="post" action="{{ URL::action('JobController@relationDealing', array($context->job->id)) }}" name="form-add-dealing">
                   <input type="hidden" data-input="id" value="">
                   <div class="form-group">
                        <input type="text" class="form-control" id="e2" data-input="dealing" name="dealing">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary" name="submit">Salvar Procedimento</button>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@endsection
