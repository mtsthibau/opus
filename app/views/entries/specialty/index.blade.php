@extends('entries/template')

@section('styles')
{{ HTML::style('/styles/global.css') }}
@endsection

@section('scripts')
{{ HTML::script('/scripts/apps/entries/specialty.js') }}
@endsection

@section('backendContent')
<div class="page-header">
    <h1 id="btn-dropdowns">Especialidades</h1>
</div>

<form role="form" action="{{ URL::action('SpecialtyController@index') }}" method="get">
    <h3>Filtrar</h3>

    <div class="row">

        <div class="col-md-4">
            <div class="form-group">
                <label>Código</label>
                <input type="text" class="form-control" placeholder="Código" value="{{ $context->filters->codigo }}" name="codigo">
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label>Especialidade</label>
                <input type="text" class="form-control" placeholder="Especialidade" value="{{ $context->filters->nome }}" name="nome">
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label>Por Página</label>
                <select class="form-control" name="max">
                    @foreach ($context->paginate as $key => $value)
                    <option value="{{ $key }}" @if ($context->filters->max == $key)selected="selected"@endif>{{ $value }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div>
        <button type="submit" class="btn btn-primary">Filtrar</button>
        <a class="btn btn-link" href="{{ URL::action('SpecialtyController@index') }}">Limpar Filtros</a>
        @if (Auth::user()->can('specialty.item_edit'))
        <a data-toggle="modal"  data-target="#modal" href="javascript:void(0);" class="btn btn-success right" id="add-specialty">
            <span class="glyphicon glyphicon-plus"></span> Adicionar Especialidade
        </a>
        @endif
    </div>
</form>

<div style="margin: 20px 0;">
    Exibindo <strong>{{ $context->specialties->getFrom() }}</strong>
    a <strong>{{ $context->specialties->getTo() }}</strong>
    de <strong>{{ $context->specialties->getTotal() }}</strong> Especialidade(s).
</div>

<table class="table table-hover">
    <thead>
    <tr>
        <th>Código</th>
        <th>Especialidade</th>
        @if (Auth::user()->can('specialty.item_edit'))
        <th style="text-align:right;">Ações</th>
        @endif
    </tr>
    </thead>
    <tbody>
    @foreach ($context->specialties as $specialty)
    <tr>
        <td>
            {{ $specialty->codigo }}
        </td>
        <td>
            {{ $specialty->nome }}
        </td>
        @if (Auth::user()->can('specialty.item_edit'))
        <td style="text-align:right;">
            <button type="button" class="btn btn-info btn-xs" name="btn-edit-specialty" data-id="{{ $specialty->id }}" data-codigo="{{ $specialty->codigo }}" data-nome="{{ $specialty->nome }}">Editar</button>
            <button type="button" class="btn btn-warning btn-xs" name="btn-del-specialty" data-url="{{ URL::action('SpecialtyController@delete', array($specialty->id)) }}" data-nome="{{ $specialty->nome }}">Excluir</button>
        </td>
        @endif
    </tr>
    @endforeach
    </tbody>
</table>

<div class="modal fade" id="modal" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Nova Especialidade</h4>
            </div>
            <div class="modal-body">
                <form role="form" method="post" action="{{ URL::action('SpecialtyController@create') }}" name="form-add-specialty">
                    <div class="form-group">
                        <label for="cargo">Código</label><span class="required"> *</span>
                        <input type="text" class="form-control"  value="" data-input="codigo">
                    </div>

                    <div class="form-group">
                        <label for="cargo">Especialidade</label><span class="required"> *</span>
                        <input type="text" class="form-control"  value="" data-input="nome">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Salvar Especialidade</button>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<div class="modal fade" id="modal-edit-specialty" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Editar Especialidade</h4>
            </div>
            <div class="modal-body">
                <form role="form" method="post" action="{{ URL::action('SpecialtyController@edit') }}" name="form-edit-specialty">
                    <input type="hidden" data-input="id" value="">
                    <div class="form-group">
                        <label for="cargo">Código</label><span class="required"> *</span>
                        <input type="text" disabled class="form-control"  value="" data-input="codigo" name="codigo">
                    </div>
                    <div class="form-group">
                        <label for="cargo">Especialidade</label><span class="required"> *</span>
                        <input type="text" class="form-control"  value="" data-input="nome" name="nome">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Salvar Alteração</button>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@endsection
