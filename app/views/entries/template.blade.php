@extends('template')

@section('styles')
{{ HTML::style('/styles/global.css') }}
{{ HTML::style('/styles/estilo.css') }}

@endsection

@section('content')

<div class="row">

    <div class="col-md-3">
        <div class="bs-sidebar hidden-print affix" role="complementary">
            <ul class="nav bs-sidenav">
                @if (Auth::user()->can('job.item_view'))
                <li @if ($context->active_menu == 'entries.jobs')class="active"@endif>
                    <a href="{{ URL::action('JobController@index') }}">Cargos</a>
                </li>
                @endif
                
                @if (Auth::user()->can('exam.item_view'))
                <li @if ($context->active_menu == 'entries.exams')class="active"@endif>
                    <a href="{{ URL::action('ExamsController@index') }}">Exames</a>
                </li>
                @endif
                
                @if (Auth::user()->can('dealing.item_view'))
                <li @if ($context->active_menu == 'entries.dealings')class="active"@endif>
                    <a href="{{ URL::action('DealingController@index') }}">Procedimentos</a>
                </li>
                @endif
                
                @if (Auth::user()->can('specialty.item_view'))
                <li @if ($context->active_menu == 'entries.specialty')class="active"@endif>
                    <a href="{{ URL::action('SpecialtyController@index') }}">Especialidades</a>
                </li>
                @endif
                
                @if (Auth::user()->can('companie.item_view'))
                <li @if ($context->active_menu == 'entries.companies')class="active"@endif>
                    <a href="{{ URL::action('CompaniesController@index') }}">Empresas</a>
                </li>
                @endif
                
                @if (Auth::user()->can('professional.item_view'))
                <li @if ($context->active_menu == 'entries.professionals')class="active"@endif>
                    <a href="{{ URL::action('ProfessionalController@index') }}">Profissionais</a>
                </li>
                @endif
                
                @if (Auth::user()->can('collaborator.item_view'))
                <li @if ($context->active_menu == 'entries.collaborators')class="active"@endif>
                    <a href="{{ URL::action('CollaboratorController@index') }}">Colaboradores</a>
                </li>  
                @endif
                
              <!---  <li @if ($context->active_menu == 'entries.billing')class="active"@endif>
                    <a href="{{ URL::action('BillingController@index') }}">Faturamento</a>
                </li>-->

            </ul>
        </div>
    </div>
    <div class="col-md-9" role="main">
        @yield('backendContent')
    </div>
</div>

@endsection
