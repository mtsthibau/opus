@extends('entries/template')

@section('styles')
{{ HTML::style('/styles/global.css') }}
@endsection

@section('scripts')
{{ HTML::script('/scripts/apps/entries/dealing.js') }}
@endsection

@section('backendContent')
<div class="page-header">
    <h1 id="btn-dropdowns">Procedimentos</h1>
</div>

<form role="form" action="{{ URL::action('DealingController@index') }}" method="get">
    <h3>Filtrar</h3>

    <div class="row">

        <div class="col-md-4">
            <div class="form-group">
                <label>Código</label>
                <input type="text" class="form-control" placeholder="Código" value="{{ $context->filters->codigo }}" name="codigo">
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label>Procedimento</label>
                <input type="text" class="form-control" placeholder="Procedimento" value="{{ $context->filters->dealing }}" name="dealing">
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label>Por Página</label>
                <select class="form-control" name="max">
                    @foreach ($context->paginate as $key => $value)
                    <option value="{{ $key }}" @if ($context->filters->max == $key)selected="selected"@endif>{{ $value }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div>
        <button type="submit" class="btn btn-primary">Filtrar</button>
        <a class="btn btn-link" href="{{ URL::action('DealingController@index') }}">Limpar Filtros</a>
        @if (Auth::user()->can('dealing.item_edit'))
        <a data-toggle="modal"  data-target="#modal" href="" class="btn btn-success right" id="add-dealing">
            <span class="glyphicon glyphicon-plus"></span> Adicionar Procedimento
        </a>
        @endif
    </div>
    
</form>

<div style="margin: 20px 0;">
    Exibindo <strong>{{ $context->dealing->getFrom() }}</strong>
    a <strong>{{ $context->dealing->getTo() }}</strong>
    de <strong>{{ $context->dealing->getTotal() }}</strong> Procedimento(s).
</div>

<table class="table table-hover">
    <thead>
    <tr>
        <th>Código</th>
        <th>Procedimento</th>
        <th>Descrição</th>
        @if (Auth::user()->can('dealing.item_edit'))
        <th style="text-align:right;">Ações</th>
        @endif
    </tr>
    </thead>
    <tbody>
    @foreach ($context->dealing as $dealing)
    <tr>
        <td>
            {{ $dealing->codigo }}
        </td>
        <td>
            {{Str::limit($dealing->dealing, $limit = 32, $end = '...')  }}
        </td>
        <td>
           {{ Str::limit($dealing->description, $limit = 45, $end = '...') }}
        </td>
         @if (Auth::user()->can('dealing.item_edit'))
        <td style="text-align:right;">       
            <button type="button" class="btn btn-info btn-xs" name="btn-edit-dealing" data-id="{{ $dealing->id }}" data-codigo="{{ $dealing->codigo }}"  data-dealing="{{ $dealing->dealing }}" data-description="{{ $dealing->description }}">Editar</button>
            <button type="button" class="btn btn-warning btn-xs" name="btn-del-dealing" data-url="{{ URL::action('DealingController@delete', array($dealing->id)) }}" data-dealing="{{ $dealing->dealing }}">Excluir</button>
        </td>
        @endif
    </tr>
    @endforeach
    </tbody>
</table>

<div class="modal fade" id="modal" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Novo Procedimento</h4>
            </div>
            <div class="modal-body">
                <form role="form" method="post" action="{{ URL::action('DealingController@create') }}" name="form-add-dealing">
                    <div class="form-group">
                        <label for="cargo">Código</label><span class="required"> *</span>
                        <input type="text" class="form-control"  value="" data-input="codigo">
                    </div>

                    <div class="form-group">
                        <label for="cargo">Procedimento</label><span class="required"> *</span>
                        <input type="text" class="form-control"  value="" data-input="dealing">
                    </div>

                    <div class="form-group">
                        <label for="function">Descrição</label><span class="required"> *</span>
                        <input type="text" class="form-control" value="" data-input="description">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Salvar Procedimento</button>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<div class="modal fade" id="modal-edit-dealing" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Editar Procedimento</h4>
            </div>
            <div class="modal-body">
                <form role="form" method="post" action="{{ URL::action('DealingController@edit') }}" name="form-edit-dealing">
                    <input type="hidden" data-input="id" value="">
                    <div class="form-group">
                        <label for="cargo">Código</label><span class="required"> *</span>
                        <input type="text" disabled class="form-control"  value="" data-input="codigo">
                    </div>

                    <div class="form-group">
                        <label for="cargo">Procedimento</label><span class="required"> *</span>
                        <input type="text" class="form-control"  value="" data-input="dealing">
                    </div>

                    <div class="form-group">
                        <label for="function">Descrição</label><span class="required"> *</span>
                        <input type="text" class="form-control" value="" data-input="description">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Salvar Alteração</button>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@endsection
