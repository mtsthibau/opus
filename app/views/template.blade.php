<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ Config::get('global.site.title') }}</title>

    <!-- Bootstrap core CSS -->
    <link href="/packages/bootstrap/dist/css/bootstrap.css" rel="stylesheet">

    <!-- Opus CSS -->
    {{ HTML::style('/styles/custom.css') }}    
    @yield('styles')

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

<body>

@if (Auth::check())
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
           <a href="{{ URL::action('UserController@get_login') }}"> <img src="/images/logo.png" alt="Clinca Opus" height="42" style="margin: 4px 14px;"></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li class="dropdown @if (strpos($context->active_menu, 'entries') !== false)active@endif">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Cadastros <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                      @if (Auth::user()->can('job.item_view'))
                <li @if ($context->active_menu == 'entries.jobs')class="active"@endif>
                    <a href="{{ URL::action('JobController@index') }}">Cargos</a>
                </li>
                @endif
                
                @if (Auth::user()->can('exam.item_view'))
                <li @if ($context->active_menu == 'entries.exams')class="active"@endif>
                    <a href="{{ URL::action('ExamsController@index') }}">Exames</a>
                </li>
                @endif
                
                @if (Auth::user()->can('dealing.item_view'))
                <li @if ($context->active_menu == 'entries.dealings')class="active"@endif>
                    <a href="{{ URL::action('DealingController@index') }}">Procedimentos</a>
                </li>
                @endif
                
                @if (Auth::user()->can('specialty.item_view'))
                <li @if ($context->active_menu == 'entries.specialty')class="active"@endif>
                    <a href="{{ URL::action('SpecialtyController@index') }}">Especialidades</a>
                </li>
                @endif
                
                @if (Auth::user()->can('companie.item_view'))
                <li @if ($context->active_menu == 'entries.companies')class="active"@endif>
                    <a href="{{ URL::action('CompaniesController@index') }}">Empresas</a>
                </li>
                @endif
                
                @if (Auth::user()->can('professional.item_view'))
                <li @if ($context->active_menu == 'entries.professionals')class="active"@endif>
                    <a href="{{ URL::action('ProfessionalController@index') }}">Profissionais</a>
                </li>
                @endif
                
                @if (Auth::user()->can('collaborator.item_view'))
                <li @if ($context->active_menu == 'entries.collaborators')class="active"@endif>
                    <a href="{{ URL::action('CollaboratorController@index') }}">Colaboradores</a>
                </li>  
                @endif
                        <!---<li><a href="{{ URL::action('BillingController@index') }}">Faturamento</a></li>-->

                    </ul>
                </li>
            </ul>
          <!--<form class="navbar-form navbar-left" role="search">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search">
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
          </form>
           -->   <ul class="nav navbar-nav navbar-right">
                @if (Auth::user()->can('admin'))
                <li @if (strpos($context->active_menu, 'admin') !== false)class="active"@endif><a href="{{ URL::action('BackendUserController@index') }}">Administração</a></li>
                @endif
                <ul class="nav navbar-nav">
                        <li><a href="{{ URL::action('UserController@logout') }}">Sair</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div>
</nav>
@else
<div class="jumbotron">
    <a href="/"> <img src = "/images/logo.png" alt="Clinca Opus" style="margin: -10px 100px;" ></a>
</div>
@endif

<div class="container content">
    @yield('content')
</div><!-- /.container -->

<!-- JavaScript
==================================================-->
<script type="text/javascript">
//<![CDATA[
@if (isset($json))
    var _context = {{ $json }};
@else
    var _context = [];
@endif
//]]>
</script>

<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="/scripts/libs/jquery.maskedinput-1.3.min.js"></script>
<script type="text/javascript" src="/scripts/libs/select2.js"></script>
<script src="/packages/bootstrap/dist/js/bootstrap.min.js"></script>

<script data-main="/scripts/bootstrap" src="/scripts/libs/require-2.1.8.js" type="text/javascript"></script>
@yield('scripts')

</body>
</html>