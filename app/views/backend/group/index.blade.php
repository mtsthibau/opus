@extends('backend/template')

@section('backendContent')
<section class="aui-page-panel-content">
    <header class="aui-page-header">
        <div class="aui-page-header-inner">
            <div class="aui-page-header-main">
                <h2><span class="glyphicon glyphicon-th-large"></span> Grupos</h2>
                <hr/>
            </div>
        </div>
    </header>

    <table class="table table-hover">
        <thead>
        <tr>
            <th>Nome do Grupo</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($context->groups as $group)
        <tr>
            <td><a href="{{ URL::action('BackendGroupController@view', array($group->tag)) }}">{{ $group->name }}</a></td>
        </tr>
        @endforeach
        </tbody>
    </table>
</section>

@endsection