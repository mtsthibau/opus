@extends('backend/template')

@section('scripts')
{{ HTML::script('/scripts/apps/backend/users.js') }}
{{ HTML::script('/scripts/apps/backend/groups.js') }}
@endsection

@section('backendContent')
<section class="aui-page-panel-content">

    <header class="aui-page-header">
        <div class="aui-page-header-inner">
            <div class="aui-page-header-main">
                <a href="{{ URL::action('BackendGroupController@index') }}">
                    <span class="glyphicon glyphicon-arrow-left"></span> Grupos
                </a>
                <h2>{{ $context->group->name }}</h2>
                <hr/>
            </div>
        </div>
    </header>

    @if (!$context->group->can("sys_admin"))
    <div class="module" style="margin-top: 50px;">
        <form action="{{ URL::action('BackendPermissionController@permission_set', array($context->group->tag)) }}" class="aui" method="post" name="set_permission">
            <h3>Adicionar Permissão</h3>
            <fieldset>
                <div class="field-group">
                    <label for="permission">Permissão</label>
                    <select class="form-control" name="selected-permission" style="max-width: 100% !important;">
                        <option value="-1">Por favor, selecione uma permissão</option>
                        @foreach ($context->another_scopes as $id => $scope)
                            @if (count($scope['permissions']))
                            <optgroup label="{{ $scope['name'] }}">
                            @foreach ($scope['permissions'] as $permission)
                                @if (Auth::user()->can('sys_admin') || $permission->tag != "sys_admin")
                                <option value="{{ $permission->id }}" data-description="{{ $permission->description }}" data-name="{{ $permission->name }}">{{ $scope['name'] }}: {{ $permission->name }}</option>
                                @endif
                            @endforeach
                            </optgroup>
                            @endif
                        @endforeach
                    </select>
                </div>
            </fieldset>
            <br/>
            <div class="buttons-container">
                <div class="buttons">
                    <input class="btn btn-primary" type="button" value="Adicionar" name="bt_set_permission">
                </div>
            </div>
            <div class="hidden">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            </div>
        </form>
    </div>
    @endif



    @foreach ($context->group_scopes as $scope)
    <table class="table table-hover">
        <thead>
        <tr>
            <th><h3>{{ $scope['name'] }}</h3></th>
            <th width="10%" class="align-right">Ações</th>
        </tr>
        </thead>
        <tbody>
            @foreach ($scope['permissions'] as $id => $permission)
            <tr>
                <td>
                    <b>{{ $permission->name }}</b>
                    <div class="description">{{ $permission->description }}</div>
                </td>
                <td class="align-right">
                    @if (Auth::user()->can('sys_admin') || $permission->tag != "sys_admin")
                    <a href="{{ URL::action('BackendPermissionController@permission_unset', array($context->group->tag, $permission->tag)) }}?_token={{ csrf_token() }}">Remover</a>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @endforeach


<div class="modal fade" id="modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      <div class="modal-body">
        <p>One fine body&hellip;</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


</section>

@endsection