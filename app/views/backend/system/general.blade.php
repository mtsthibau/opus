@extends('backend/template')

@section('backendContent')
<section class="aui-page-panel-content">
    <header class="aui-page-header">
        <div class="aui-page-header-inner">
            <div class="aui-page-header-main">
                <h2>Configurações Gerais</h2>
            </div>
        </div>
    </header>
</section>
@endsection