@extends('backend/template')

@section('scripts')
{{ HTML::script('/scripts/apps/backend/users.js') }}
{{ HTML::script('/scripts/apps/backend/groups.js') }}
@endsection

@section('backendContent')
<section class="aui-page-panel-content">
    <header class="aui-page-header">
        <div class="aui-page-header-inner">
            <div class="aui-page-header-main">
                <h2><span class="glyphicon glyphicon-lock"></span>  Permissões</h2>
                <hr/>
            </div>
        </div>
    </header>

    @foreach ($context->permissions as $scope)
    <table class="table table-hover" style="margin-top: 30px;">
        <thead>
        <tr>
            <th><h3>{{ $scope->name }}</h3></th>
            <th style="width: 260px;">Grupos</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($scope->permissions as $permission)
        <tr>
            <td><b>{{ $permission->name }}</b>
                <div class="description">{{ $permission->description }}</div>
            </td>
            <td>
                <ul class="group-list">
                    @foreach($permission->roles as $group)
                    <li>{{ $group->name }}
                        @if (Auth::user()->can('sys_admin') || !$group->can("sys_admin"))
                        (<a href="{{ URL::action('BackendPermissionController@permission_unset', array($group->tag, $permission->tag)) }}?_token={{ csrf_token() }}">Remover</a>)
                        @endif
                    </li>
                    @endforeach
                </ul>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
    @endforeach

</section>
@endsection