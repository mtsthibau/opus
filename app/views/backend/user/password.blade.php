<form action="{{ URL::action('BackendUserController@password') }}" class="aui" id="user-password" method="post">
    <div class="form-body">
        <div class="field-group">
            <label>Senha</label>
            <input autocomplete="off" class="text" name="password" type="password">
            @if ($context->field_errors && $context->field_errors->has('password'))
            <div class="error">{{ $context->field_errors->first('password') }}</div>
            @endif
        </div>
        <div class="field-group">
            <label>Confirme</label>
            <input autocomplete="off" class="text" name="confirm" type="password">
            @if ($context->field_errors && $context->field_errors->has('confirm'))
            <div class="error">{{ $context->field_errors->first('confirm') }}</div>
            @endif
        </div>
    </div>
    <!-- // .group -->
    <div class="hidden">
        <input name="submit" type="hidden" value="true">
        <input name="username" type="hidden" value="{{ $context->user->username }}"/>
        <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
    </div>
</form>