@extends('backend/template')

@section('scripts')
{{ HTML::script('/scripts/libs/bootbox.min.js') }}
{{ HTML::script('/scripts/apps/backend/users.js') }}
@endsection

@section('backendContent')

<div class="page-header">
    <h1 id="btn-dropdowns"><span class="glyphicon glyphicon-user"></span> Lista de Usuários</h1>
</div>

<form role="form" action="{{ URL::action('BackendUserController@index') }}" method="get">
    <h3>Filtrar</h3>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label>&nbsp;</label>
                <input type="text" class="form-control" placeholder="Nome, usuário ou email" value="{{ $context->filters->search }}" name="search">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label>Grupo</label>
                <select class="form-control" name="group">
                    <option value="">Qualquer</option>
                    @foreach ($context->roles as $role)
                    <option value="{{ $role->id }}" @if ($context->filters->group == $role->id)selected="selected"@endif>{{ $role->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label>Por Página</label>
                <select class="form-control" name="max">
                    @foreach ($context->paginate as $key => $value)
                    <option value="{{ $key }}" @if ($context->filters->max == $key)selected="selected"@endif>{{ $value }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div>
        <button type="submit" class="btn btn-primary">Filtrar</button>
        <a class="btn btn-link" href="{{ URL::action('BackendUserController@index') }}">Limpar Filtros</a>
        <a data-toggle="modal" class="btn btn-success right" id="add-user">
            <span class="glyphicon glyphicon-plus"></span> Adicionar Usuário
        </a>
    </div>
</form>

<div style="margin: 20px 0;">
    Exibindo <strong>{{ $context->users->getFrom() }}</strong>
    a <strong>{{ $context->users->getTo() }}</strong>
    de <strong>{{ $context->users->getTotal() }}</strong> usuário(s).
</div>

<table class="table table-hover">
    <thead>
    <tr>
        <th>Usuário</th>
        <th>Nome Completo</th>
        <th>Grupos</th>
        <th style="text-align:right;">Ações</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($context->users as $user)
    <tr>
        <td>
            @if ($user->disabled)
            <del>{{ strtolower($user->username) }}</del><br />
            (Inativo)
            @else
            {{ strtolower($user->username) }}
            @endif
        </td>
        <td>
            @if ($user->disabled)
            <del>{{ $user->profile->fullname }}</del>
            @else
            {{ $user->profile->fullname }}
            @endif
            <br />
            <a href="mailto:{{ $user->email }}"><span class="email">{{ $user->email }}</span></a>
        </td>
        <td>
            <ul>
                @foreach ($user->roles()->orderBy('level', 'desc')->get() as $group)
                <li><a href="{{ URL::action('BackendGroupController@view', array($group->tag)) }}">{{ $group->name }}</a></li>
                @endforeach
            </ul>
        </td>
        <td style="text-align:right;">
            <div data-username="{{ $user->username }}" data-name="{{ $user->profile->fullname }}" data-groups="@foreach ($user->roles()->orderBy('level', 'desc')->get() as $group){{ $group->id }},@endforeach">
                <button type="button" class="btn btn-info btn-xs" name="btn-group-user">Grupos</button>
                <button type="button" class="btn btn-warning btn-xs" name="btn-edit-user" data-username="{{ $user->username }}" data-fullname="{{ $user->profile->fullname }}" data-email="{{ $user->email }}" data-id="{{ $user->id }}" data-active="{{ !$user->disabled }}">Editar</button>
            </div>
        </td>
    </tr>
    @endforeach
    </tbody>
</table>

<div>
    <div style="float:right;">{{ $context->users->links() }}</div>
    <div class="clear"></div>
</div>

<div class="modal fade" id="group-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form action="{{ URL::action('BackendUserController@group') }}" method="post">
                <input type="hidden" name="username" value="">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Grupos Disponíveis</label>
                            <select class="form-control" multiple="multiple" size="10" name="groupsToJoin[]" id="groupsToJoin"></select>
                            <div class="clear">&nbsp;</div>
                            <button type="submit" name="join" class="btn btn-default" value="1">Participar dos grupos selecionados</button>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Grupos Atuais</label>
                            <select class="form-control" multiple="multiple" size="10" name="groupsToLeave[]" id="groupsToLeave"></select>
                            <div class="clear">&nbsp;</div>
                            <input type="submit" name="leave" class="btn btn-default" value="Sair dos grupos selecionados">
                        </div>
                    </div>
                </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal">Cancelar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="modal-add-user" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Novo Usuário</h4>
            </div>
            <div class="modal-body">
                <form role="form" method="post" action="{{ URL::action('BackendUserController@create') }}" name="form-new-user">
                    <div class="form-group">
                        <label for="username">Nome de Usuário</label><span class="required"> *</span>
                        <input type="text" class="form-control" value="" data-input="username">

                    </div>

                    <div class="form-group">
                        <label for="password">Senha</label><span class="required"> *</span>
                        <input type="password" class="form-control" value="" data-input="password">
                    </div>

                    <div class="form-group">
                        <label for="ocnfirm">Confirme</label><span class="required"> *</span>
                        <input type="password" class="form-control" value="" data-input="confirm">
                    </div>

                    <div class="form-group">
                        <label for="email">E-mail</label><span class="required"> *</span>
                        <input type="email" class="form-control" value="" data-input="email">
                    </div>

                    <div class="form-group">
                        <label for="fullname">Nome Completo</label><span class="required"> *</span>
                        <input type="text" class="form-control" value="" data-input="fullname">
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Criar Usuário</button>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" id="modal-edit-user" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Editar Usuário</h4>
            </div>
            <div class="modal-body">
                <form role="form" method="post" action="{{ URL::action('BackendUserController@edit') }}" name="form-edit-user">
                    <input type="hidden" data-input="user_id">
                    <div class="form-group">
                        <label for="username">Nome de Usuário</label><span class="required"> *</span>
                        <input type="text" class="form-control" id="username" value="" data-input="username">
                    </div>

                    <div class="form-group">
                        <label for="email">E-mail</label><span class="required"> *</span>
                        <input type="email" class="form-control" id="email" value="" data-input="email">
                    </div>

                    <div class="form-group">
                        <label for="fullname">Nome Completo</label><span class="required"> *</span>
                        <input type="text" class="form-control" id="fullname" value="" data-input="fullname">
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" data-input="active"> Ativo
                        </label>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Salvar Usuário</button>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@endsection