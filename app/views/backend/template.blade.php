@extends('template')

@section('styles')
{{ HTML::style('/styles/global.css') }}
{{ HTML::style('/styles/estilo.css') }}

@endsection

@section('content')

<div class="row">

    <div class="col-md-3">
        <div class="bs-sidebar hidden-print affix" role="complementary">
            <ul class="nav bs-sidenav">
                <li @if ($context->active_menu == 'admin.users')class="active"@endif>
                    <a href="{{ URL::action('BackendUserController@index') }}">Usuários</a>
                </li>
                <li @if ($context->active_menu == 'admin.groups')class="active"@endif>
                    <a href="{{ URL::action('BackendGroupController@index') }}">Grupos</a>
                </li>
                <li @if ($context->active_menu == 'admin.permissions')class="active"@endif>
                    <a href="{{ URL::action('BackendPermissionController@index') }}">Permissões</a>
                </li>

            </ul>
        </div>
    </div>
    <div class="col-md-9" role="main">
        @yield('backendContent')
    </div>
</div>


@endsection
