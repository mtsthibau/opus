@extends('backend/template')

@section('scripts')
{{ HTML::script('/scripts/apps/backend/mail.js') }}
@endsection

@section('backendContent')
<section class="aui-page-panel-content">

    <header class="aui-page-header">
        <div class="aui-page-header-inner">
            <div class="aui-page-header-main">
                <h2>Enviar Email</h2>
            </div>
        </div>
    </header>
    <p class="item-description">
    @if ($context->sending)
        Sua mensagem será enviada para os seguintes usuários:
    @else
        Você pode enviar um e-mail para os usuários a partir do formulário abaixo.
    @endif
    </p>

    @if (count($context->form_errors) > 0)
    <div class="aui-message error">
        <span class="aui-icon icon-error"></span>

        <p>@foreach ($context->form_errors as $e)
            {{ $e }}<br />
            @endforeach
        </p>
    </div>
    @endif

    @if (!count($context->servers))
    <div class="aui-message info">
        <span class="aui-icon icon-info"></span>
        <p>Para enviar um e-mail você precisa <a href="{{ URL::action('BackendMailController@create') }}">configurar</a> um servidor de e-mail.</p>
    </div>
    @else

    @if ($context->sending)

    <form class="aui">
        <div class="buttons-container buttons-separator">
            <div class="buttons">
                <input type="button" name="ok-sending-mail" value="OK" class="aui-button" data-location="{{ URL::action('BackendMailController@send') }}">
            </div>
        </div>
    </form>

    <table class="aui aui-table-sortable aui-table-rowhover">
        <thead>
        <tr>
            <th>Usuário</th>
            <th>Email</th>
            <th>Nome Completo</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($context->sending as $sending)
        <tr>
            <td>
                <span><a href="{{ URL::action('BackendUserController@view', array($sending->username)) }}">{{ $sending->username }}</a></span>
            </td>
            <td>
                <span>{{ $sending->email }}</span>
            </td>
            <td>
                <span>{{ $sending->profile->fullname }}</span>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>

    @else
    <form action="{{ URL::action('BackendMailController@send') }}" method="post" class="aui">
        <fieldset>
            <div class="field-group">
                <label for="groups">Para<span class="aui-icon icon-required"> required</span></label>
                <select class="multi-select long-field" size="8" multiple="multiple" id="groups" name="groups[]">
                    @foreach($context->groups as $group)
                    <option value="{{ $group->id }}" @if (in_array($group->id, $context->form->groups)) selected="selected" @endif>{{ $group->name }}</option>
                    @endforeach
                </select>
                @if ($context->field_errors && $context->field_errors->has('groups'))
                <div class="error">{{ $context->field_errors->first('groups') }}</div>
                @endif
                <div class="description">
                    [ <a href="javascript: void(0);" name="select-all">selecionar todos</a> ]
                    [ <a href="javascript: void(0);" name="remove-all">remover todos</a> ]
                </div>
                <div class="description">Por favor, selecione um ou mais grupos a partir da lista acima. A mensagem de e-mail será enviada a todos os membros dos grupos escolhidos.</div>
                <div class="description">Nota: Um usuário receberá o e-mail apenas uma vez, mesmo se ele for membro de mais de um grupo.</div>
            </div>

            <div class="field-group">
                <label for="subject">Assunto<span class="aui-icon icon-required"> required</span></label>
                <input class="text long-field" type="text" name="subject" value="{{ $context->form->subject }}" id="subject" />
                @if ($context->field_errors && $context->field_errors->has('subject'))
                <div class="error">{{ $context->field_errors->first('subject') }}</div>
                @endif
            </div>

            <div class="field-group">
                <label for="type">Tipo da Mensagem</label>
                <select class="select" name="type" id="type">
                    <option value="plain" @if ($context->form->type == "plain") selected="selected" @endif>Texto</option>
                    <option value="html" @if ($context->form->type == "html") selected="selected" @endif>HTML</option>
                </select>
                @if ($context->field_errors && $context->field_errors->has('type'))
                <div class="error">{{ $context->field_errors->first('type') }}</div>
                @endif
            </div>

            <div class="field-group">
                <label for="message">Mensagem<span class="aui-icon icon-required"> required</span></label>
                <textarea class="textarea long-field" rows="8" name="message" id="message" style="max-width: 80%;">{{ $context->form->message }}</textarea>
                @if ($context->field_errors && $context->field_errors->has('message'))
                <div class="error">{{ $context->field_errors->first('message') }}</div>
                @endif
            </div>

            <div class="field-group">
                <label for="mode">Modo de Envio</label>
                <select class="select" name="mode" id="mode">
                    <option data-desc="Os emails serão encaminhados individualmente." value="1" @if ($context->form->mode == "1") selected="selected" @endif>Individual</option>
                    <option data-desc="Os emails serão enviados em lotes de 20 destinatários." value="2" @if ($context->form->mode == "2") selected="selected" @endif>Cópia</option>
                    <option data-desc="Os emails serão enviados em lotes de 20 destinatários." value="3" @if ($context->form->mode == "3") selected="selected" @endif>Cópia Oculta</option>
                </select>
                @if ($context->field_errors && $context->field_errors->has('mode'))
                <div class="error">{{ $context->field_errors->first('mode') }}</div>
                @endif
                <div class="description"></div>
            </div>

        </fieldset>

        <div class="hidden">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        </div>

        <div class="buttons-container buttons-separator">
            <div class="buttons">
                <input type="submit" name="submit" value="Enviar" class="aui-button">
                <a href="{{ URL::action('BackendMailController@server') }}" class="aui-button aui-button-link">Cancelar</a>
            </div>
        </div>
    </form>
    @endif
    @endif

</section>
@endsection