@extends('backend/template')

@section('scripts')
{{ HTML::script('/scripts/apps/backend/mail.js') }}
@endsection

@section('backendContent')
<section class="aui-page-panel-content">

    @if (count($context->form_errors) > 0)
    <div class="aui-message error">
        <span class="aui-icon icon-error"></span>
        <p>@foreach ($context->form_errors as $e)
            {{ $e }}<br />
           @endforeach
        </p>
    </div>
    @endif

    <header class="aui-page-header">
        <div class="aui-page-header-inner">
            <div class="aui-page-header-main">
                <h2>
                @if ($context->mserver)
                    Editar servidor de e-mail
                @else
                    Adicionar novo servidor de e-mail
                @endif
                </h2>
            </div>
        </div>
    </header>
    <p class="item-description">Use esta página para adicionar/editar um servidor de email SMTP. Este servidor poderá ser usado para enviar as mensagens de saída do sistema.</p>

    <form action="{{ $context->form->action }}" class="aui" method="post" name="mail-form">
        <fieldset>
            <div class="field-group">
                <label>Nome<span class="aui-icon icon-required"> required</span></label>
                <input class="text" type="text" name="name" title="nome do servidor" value="{{ $context->form->name }}" />
                @if ($context->field_errors && $context->field_errors->has('name'))
                <div class="error">{{ $context->field_errors->first('name') }}</div>
                @endif
                <div class="description">O nome do servidor no sistema.</div>
            </div>
            <div class="field-group">
                <label>Descrição</label>
                <input class="text long-field" type="text" name="description" title="descrição do servidor" value="{{ $context->form->description }}" />
                @if ($context->field_errors && $context->field_errors->has('description'))
                <div class="error">{{ $context->field_errors->first('description') }}</div>
                @endif
            </div>
            <div class="field-group">
                <label>Remetente<span class="aui-icon icon-required"> required</span></label>
                <input class="text" type="text" name="from" title="email de saída" value="{{ $context->form->from }}" />
                @if ($context->field_errors && $context->field_errors->has('from'))
                <div class="error">{{ $context->field_errors->first('from') }}</div>
                @endif
                <div class="description">O endereço de email que o servidor irá utilizar para enviar as mensagens.</div>
            </div>
            <div class="field-group">
                <label>Prefixo<span class="aui-icon icon-required"> required</span></label>
                <input class="text" type="text" name="prefix" title="prefixo dos assuntos" value="{{ $context->form->prefix }}" />
                @if ($context->field_errors && $context->field_errors->has('prefix'))
                <div class="error">{{ $context->field_errors->first('prefix') }}</div>
                @endif
                <div class="description">Este prefixo será prefixado para todos os assuntos de e-mail de saída.</div>
            </div>
        </fieldset>

        <h3 >Servidor SMTP</h3>
        <fieldset>
            <div class="field-group">
                <label>Servidor<span class="aui-icon icon-required"> required</span></label>
                <input class="text" type="text" name="host" title="nome do host smtp" value="{{ $context->form->host }}" />
                @if ($context->field_errors && $context->field_errors->has('host'))
                <div class="error">{{ $context->field_errors->first('host') }}</div>
                @endif
                <div class="description">O nome do host SMTP do seu servidor de email.</div>
            </div>
            <div class="field-group">
                <label>Porta SMTP<span class="aui-icon icon-required"> required</span></label>
                <input class="text" type="text" name="port" title="porta do servidor smtp" value="{{ $context->form->port }}" />
                @if ($context->field_errors && $context->field_errors->has('port'))
                <div class="error">{{ $context->field_errors->first('port') }}</div>
                @endif
            </div>
            <fieldset class="group">
                <legend><span>TLS</span></legend>
                <div class="checkbox">
                    <input class="checkbox" type="checkbox" name="tls" id="tls" @if ($context->form->tls) checked="checked" @endif />
                    <label for="tls">Este servidor SMTP requer o uso de segurança TLS.</label>
                </div>
            </fieldset>
            <div class="field-group">
                <label>Usuário<span class="aui-icon icon-required"> required</span></label>
                <input class="text" type="text" name="username" title="usuario do host smtp" value="{{ $context->form->username }}" />
                @if ($context->field_errors && $context->field_errors->has('username'))
                <div class="error">{{ $context->field_errors->first('username') }}</div>
                @endif
                <div class="description">Usuário para conexão com o seu servidor de email.</div>
            </div>
            @if ($context->mserver)
            <fieldset class="group">
                <legend><span>Alterar Senha</span></legend>
                <div class="checkbox">
                    <input class="checkbox" type="checkbox" name="cpass" id="cpass" @if ($context->form->cpass) checked="checked" @endif />
                    <div class="description">&nbsp;</div>
                </div>
            </fieldset>
            @endif
            <div class="field-group" name="field-password" style="@if (!$context->form->cpass)display:none; @endif">
                <label>Senha<span class="aui-icon icon-required"> required</span></label>
                <input class="text" type="password" name="password" title="senha do host smtp" value="{{ $context->form->password }}" />
                @if ($context->field_errors && $context->field_errors->has('password'))
                <div class="error">{{ $context->field_errors->first('password') }}</div>
                @endif
            </div>
        </fieldset>

        <div class="clear">&nbsp;</div>
        <div id="test-container"></div>

        <div class="buttons-container buttons-separator">
            <div class="buttons">
                <input type="button" class="aui-button" id="test-connection" data-url="{{ URL::action('BackendMailController@connection') }}" value="Testar Conexão">&nbsp;
                <input type="submit" name="submit" value="@if ($context->mserver)Salvar @else Adicionar @endif" class="aui-button">
                <a href="{{ URL::action('BackendMailController@server') }}" class="aui-button aui-button-link">Cancelar</a>
            </div>
        </div>

        <div class="hidden">
            @if ($context->mserver)
            <input type="hidden" name="mserver" value="{{ $context->mserver->id }}" />
            @endif
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        </div>
    </form>

</section>
@endsection