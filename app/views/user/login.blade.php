@extends('template')

@section('styles')
{{ HTML::style('/styles/login.css') }}
@endsection


@section('content')
<div class="row">
    <div class="span6">
        <h1>Bem vindo à Clínica Opus!</h1>
        <hr>
          <h4>  Promover e resguardar a saúde de cada trabalhador é um benefício que se estende não só a este, mas também à organização.
          <br/>Ao proporcionar aos empregados um ambiente saudável, com qualidade de vida, a empresa terá um funcionário mais motivado e, portanto, mais produtivo, haja vista que não haverá influências do ambiente de trabalho que interrompam ou prejudiquem seu desempenho.</h4>
    </div>
    <div class="offset1 span5">
        <div class="clear-form">
            <form action="" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <div class="form-heading">
                    <h3>Entrar</h3>
                </div>

                @if ($context->form_errors)
                <div class="alert alert-danger" style="margin: 0 35px 20px;">
                    @foreach ($context->form_errors as $e)
                    {{ $e }}<br />
                    @endforeach
                </div>
                @endif

                <div class="form-body">
                    <input type="text" class="form-control" placeholder="Usuário ou Endereço de Email" name="username" value="{{ $context->username }}">
                    @if ($context->field_errors && $context->field_errors->has('username'))
                    <p class="text-danger">{{ $context->field_errors->first('username') }}</p>
                    @endif
                    <input type="password" class="form-control" placeholder="Senha" style="margin-top: 20px;" name="password">
                    @if ($context->field_errors && $context->field_errors->has('password'))
                    <p class="text-danger">{{ $context->field_errors->first('password') }}</p>
                    @endif
                    <div class="body-split">
                        <div class="left-col">
                            <label class="checkbox">
                                <input type="checkbox" value="remember-me"> Lembrar
                            </label>
                        </div>
                        <div class="right-col">
                            <button type="submit" class="btn btn-primary">Entrar</button>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="form-footer">
                    <hr>
                    <p class="center">
                        <a href="#">Esqueceu sua senha?</a>
                    </p>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection