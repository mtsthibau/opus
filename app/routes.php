<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'DashboardController@index');


/*
|--------------------------------------------------------------------------
| User Routes
|--------------------------------------------------------------------------
*/

Route::group(array('prefix' => '/user'), function() {
    Route::get('login', 'UserController@get_login');
    Route::get('logout', 'UserController@logout');

    // Necessita de uma chave csrf válida
    Route::group(array('before' => 'csrf'), function() {
        Route::post('login', 'UserController@post_login');
    });
});

/*
|--------------------------------------------------------------------------
| Dashboard Routes
|--------------------------------------------------------------------------
*/

Route::group(array('before' => 'auth'), function() {
    Route::get('dashboard', 'DashboardController@index');
});

/*
|--------------------------------------------------------------------------
| Empresas
|--------------------------------------------------------------------------
*/

//Empresas
Route::group(array('prefix' => '/companies', 'before' => 'auth'), function() {
    Route::group(array('prefix' => 'companies'), function() {
        Route::get('/', 'CompaniesController@index');
        Route::any('create', 'CompaniesController@create');
        Route::any('edit/{id}', 'CompaniesController@edit');
        Route::any('detail/{id}', 'CompaniesController@detail');
        Route::post('relexam/{id}', 'CompaniesController@relationExam');
        Route::get('delrelation/{id}', 'CompaniesController@delRelation');
        Route::post('editrelation/', 'CompaniesController@editRelation');
        Route::post('editdealing/', 'CompaniesController@editDealing');
        Route::post('companiedealing/{id}', 'CompaniesController@relationDealing');
        Route::get('deldealing/{id}', 'CompaniesController@delDealing');    
        Route::post('editcollaborator/', 'CompaniesController@editCollaborator');
        Route::post('companiecollaborator/{id}', 'CompaniesController@relationCollaborator');
        Route::get('delcollaborator/{id}', 'CompaniesController@delCollaborator');
    });
    
    // Profissionais
    Route::group(array('prefix' => 'professionals'), function() {
        Route::get('/', 'ProfessionalController@index');
        Route::any('create', 'ProfessionalController@create');
        Route::any('detail/{id}', 'ProfessionalController@detail');
        Route::any('edit/{id}', 'ProfessionalController@edit');
        
    });
    // Colaboradores/Collaborators
    Route::group(array('prefix' => 'collaborator'), function() {
        Route::get('/', 'CollaboratorController@index');
        Route::any('create', 'CollaboratorController@create');
        Route::any('detail/{id}', 'CollaboratorController@detail');
        Route::any('edit/{id}', 'CollaboratorController@edit');
        Route::post('relationjob/{id}', 'CollaboratorController@relationJob');
        Route::get('deljob/{id}', 'CollaboratorController@delJob');
       
    });
    
    // Faturamento/Billing
     Route::get('billing', 'BillingController@index');
     Route::get('billing/view/{id}', 'BillingController@view');
    
    //Empresas->Cargos 
    Route::group(array('prefix' => 'relationjobs'), function() {
    Route::any('index/{id}', 'RelationjobsController@index');
    Route::any('create/{id}', 'RelationjobsController@create');
    Route::any('delete/{id}', 'RelationjobsController@delete');

 });
});

/*
|--------------------------------------------------------------------------
| Cadastros
|--------------------------------------------------------------------------
*/

//Cargos
Route::group(array('prefix' => '/entries', 'before' => 'auth'), function() {
    Route::group(array('prefix' => 'jobs'), function() {
        Route::get('/', 'JobController@index');
        Route::post('create', 'JobController@create'); 
        Route::get('delete/{id}', 'JobController@delete');
        Route::post('edit', 'JobController@edit');
        Route::get('listexam/{id}', 'JobController@listExam');
        Route::any('relationexam/{id}', 'JobController@relationExam');
        Route::get('delrelationexam/{id}', 'JobController@delRelationExam');
        Route::any('relationdealing/{id}', 'JobController@relationDealing');
        Route::get('delrelationdealing/{id}', 'JobController@delRelationDealing');
    });
//Exames
    Route::group(array('prefix' => 'exams'), function() {
        Route::get('/', 'ExamsController@index');
        Route::post('create', 'ExamsController@create');
        Route::post('edit', 'ExamsController@edit');
        Route::get('delete/{id}', 'ExamsController@delete');
    });
//Especialidade
    Route::group(array('prefix' => 'specialty'), function() {
        Route::get('/', 'SpecialtyController@index');
        Route::post('create', 'SpecialtyController@create');
        Route::post('edit', 'SpecialtyController@edit');
        Route::get('delete/{id}', 'SpecialtyController@delete');
    });
//Procedimentos    
    Route::group(array('prefix' => 'dealing'), function() {
        Route::get('/', 'DealingController@index');
        Route::post('create', 'DealingController@create');
        Route::post('edit', 'DealingController@edit');
        Route::get('delete/{id}', 'DealingController@delete');

    });

});
    
/*
|--------------------------------------------------------------------------
| Backend Routes
|--------------------------------------------------------------------------
*/

Route::group(array('prefix' => '/admin', 'before' => 'auth|can:admin'), function() {
    Route::get('/', 'BackendController@overview');

    // Usuários
    Route::get('user', 'BackendUserController@index');
    Route::get('user/view/{username}', 'BackendUserController@view');

    // Necessita de uma chave csrf válida
    Route::group(array('before' => 'csrf'), function() {
        Route::post('user/create', 'BackendUserController@create');
        Route::post('user/edit', 'BackendUserController@edit');
        Route::post('user/password', 'BackendUserController@password');
        Route::post('user/group', 'BackendUserController@group');
    });
    
    // Grupos
    Route::get('group', 'BackendGroupController@index');
    Route::get('group/view/{group}', 'BackendGroupController@view');

    Route::get('permission', 'BackendPermissionController@index');

    // Permissões de Grupo
    // Necessita de uma chave csrf válida
    Route::group(array('before' => 'csrf'), function() {
        Route::post('permission/{group}/set', 'BackendPermissionController@permission_set');
        Route::get('permission/{group}/unset/{permission}', 'BackendPermissionController@permission_unset');
    });

    // System
    Route::get('system/general', 'BackendSystemController@general');

    // E-Mail
    Route::get('mail/send', 'BackendMailController@send');
    Route::get('mail/queue', 'BackendMailController@queue');
    Route::get('mail/server', 'BackendMailController@server');
    Route::get('mail/server/create', 'BackendMailController@create');

    // Necessita de uma chave csrf válida
    Route::group(array('before' => 'csrf'), function() {
        Route::post('mail/send', 'BackendMailController@send');
        Route::get('mail/queue/repair', 'BackendMailController@repair');
        Route::get('mail/server/connection', 'BackendMailController@connection');
        Route::post('mail/server/create', 'BackendMailController@create');
        Route::any('mail/server/test/{server}', 'BackendMailController@test');
        Route::any('mail/server/edit/{server}', 'BackendMailController@edit');
        Route::any('mail/server/delete/{server}', 'BackendMailController@delete');
    });
});
