<?php


class DealingController extends BaseController {

    /**
     * Operações com os Cargos 
     *
     * @
     */

     public function index(){
     
      $this->set_context(array(
            'active_menu' => 'entries.dealings',
            'filters' => (object) array(
                'codigo' => Input::get('codigo', ''),
                'dealing' => Input::get('dealing', ''),
                'max' => Input::get('max', 20)
            ),
            'paginate' => array(
                '10000' => 'Tudo', '10' => '10', '20' => '20', '50' => '50', '100' => '100'
            )
        ));

        $dealings = Dealing::select();

         if (Input::get('codigo')) {
             $dealings->where('codigo', 'like', '%'. Input::get('codigo') .'%');
         }

         if (Input::get('dealing')) {
             $dealings->where('dealing', 'like', '%'. Input::get('dealing') .'%');
         }

        $dealings = $dealings->orderBy('dealing')->paginate(Input::get('max', 20));

        $this->set_context(array(
            'dealing' => $dealings
        ));

        return $this->view_make('entries/dealing/index');
    }
    
    public function create()
    {
        $validate = array(
            'codigo' => 'required|integer',
            'dealing' => 'required|min:3|max:128',
            'description' => 'required|min:3|max:256'
        );

        $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            return Response::json(array(
                'success' => false,
                'errors' => $validated->messages()->toArray()
            ));
        }
        
         $valida = Dealing::where('codigo', '=' , Input::get('codigo'))->get();
         $valida1 = Dealing:: where('dealing', '=' , Input::get('dealing'))->get();
         
         if(count($valida))
         {
            return Response::json(array(
                    'success' => false,
                    'errors' => array('codigo' => 'Desculpe mas esse código já faz referência a outro procedimento.')
            ));
         }
         else if(count($valida1))
         {
          return Response::json(array(
                    'success' => false,
                    'errors' => array('dealing' => 'Desculpe mas esse procedimento já existe.')
            ));
         }
        

        $dealing = new Dealing;
        $dealing->codigo = Input::get('codigo');
        $dealing->dealing = Input::get('dealing');
        $dealing->description = Input::get('description');
        $dealing->save();

        return Response::json(array(
            'success' => true,
            'redirectTo' => URL::action(
                'DealingController@index'
            )
        ));
    }
    
    public function delete($id)
    {
        $dealing = Dealing::find($id);
        $dealing->delete();
        
        return Redirect::back();
    }
    
    public function edit()
    {
        $dealing = Dealing::where('id', '=', Input::get('id'))->firstOrFail();

        $validate = array(
            'codigo' => 'required|integer',
            'dealing' => 'required|min:3|max:128',
            'description' => 'required|min:3|max:256'
        );
       
       $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            return Response::json(array(
                'success' => false,
                'errors' => $validated->messages()->toArray()
            ));
        }
        
         $valida = Dealing:: where('dealing', '=' , Input::get('dealing'))->get();
         
        
         if(count($valida))
         {
          return Response::json(array(
                    'success' => false,
                    'errors' => array('dealing' => 'Desculpe mas esse procedimento já existe.')
            ));
         }
        

        $dealing->codigo = Input::get('codigo');
        $dealing->dealing = Input::get('dealing');
        $dealing->description = Input::get('description');
        $dealing->save();
               
        return Response::json(array(
            'success' => true,
            'redirectTo' => URL::action(
                'DealingController@index'
             )
        ));
    }

 }