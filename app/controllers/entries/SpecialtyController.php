<?php


class SpecialtyController extends BaseController {

    /**
     * Operações com os Cargos 
     *
     * @
     */

     public function index(){
     
      $this->set_context(array(
            'active_menu' => 'entries.specialty',
            'filters' => (object) array(
                'codigo' => Input::get('codigo', ''),
                'nome' => Input::get('nome', ''),
                'max' => Input::get('max', 20)
            ),
            'paginate' => array(
                '10000' => 'Tudo', '10' => '10', '20' => '20', '50' => '50', '100' => '100'
            )
        ));

         $specialties = Specialty::select();

         if (Input::get('codigo')) {
             $specialties->where('codigo', 'like', '%'. Input::get('codigo') .'%');
         }

         if (Input::get('nome')) {
             $specialties->where('nome', 'like', '%'. Input::get('nome') .'%');
         }

         $specialties = $specialties->orderBy('nome')->paginate(Input::get('max', 20));

        $this->set_context(array(
            'specialties' => $specialties
        ));

        return $this->view_make('entries/specialty/index');
    }
    
    public function create()
    {
        $validate = array(
            'codigo' => 'required|integer',
            'nome' => 'required|min:3|max:128'
        );

        $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            return Response::json(array(
                'success' => false,
                'errors' => $validated->messages()->toArray()
            ));
        }
        
         $valida = Specialty::where('codigo', '=' , Input::get('codigo'))->get();
         $valida1 = Specialty:: where('nome', '=' , Input::get('nome'))->get();
         
         if(count($valida))
         {
            return Response::json(array(
                    'success' => false,
                    'errors' => array('codigo' => 'Desculpe mas esse código já faz referência a outra especialidade.')
            ));
         }
         else if(count($valida1))
         {
          return Response::json(array(
                    'success' => false,
                    'errors' => array('nome' => 'Desculpe mas essa especialidade já existe.')
            ));
         }

        $specialty = new Specialty;
        $specialty->codigo = Input::get('codigo');
        $specialty->nome = Input::get('nome');
        $specialty->save();

        return Response::json(array(
            'success' => true,
            'redirectTo' => URL::action(
                'SpecialtyController@index'
            )
        ));
    }
    
    public function delete($id)
    {
        Specialty::find($id)->delete();
        return Redirect::back();
    }
    
    public function edit()
    {
        $specialty = Specialty::where('id', '=', Input::get('id'))->firstOrFail();

        $validate = array(
            'codigo' => 'required|integer',
            'nome' => 'required|min:3|max:128'
        );
       
       $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            return Response::json(array(
                'success' => false,
                'errors' => $validated->messages()->toArray()
            ));
        }
        
          // validação (cadastro de exames repetidos)
        
         $valida = specialty::where('nome', '=' , Input::get('nome'))->get();
                        
         if(count($valida))
         {
            return Response::json(array(
                    'success' => false,
                    'errors' => array('nome' => 'Desculpe mas esta especialidade já existe')
                ));
         }

        $specialty->codigo = Input::get('codigo');
        $specialty->nome = Input::get('nome');
        $specialty->save();
               
        return Response::json(array(
            'success' => true,
            'redirectTo' => URL::action(
                'SpecialtyController@index'
             )
        ));
    }

 }