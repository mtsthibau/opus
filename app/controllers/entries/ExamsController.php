<?php


class ExamsController extends BaseController {

    /**
     * Operações com os Cargos 
     *
     * @
     */

     public function index(){
     
      $this->set_context(array(
            'active_menu' => 'entries.exams',
            'filters' => (object) array(
                'exam' => Input::get('exam', ''),
                'codigo' => Input::get('codigo', ''),
                'max' => Input::get('max', 20)
            ),
            'paginate' => array(
                '10000' => 'Tudo', '10' => '10', '20' => '20', '50' => '50', '100' => '100'
            )
        ));

        $exams = Exam::select();
        
        if (Input::get('codigo')) {
            $exams->where('codigo', 'like', '%'. Input::get('codigo') .'%');
        }    
         
        if (Input::get('exam')) {
            $exams->where('exam', 'like', '%'. Input::get('exam') .'%');
        }

        $exams = $exams->orderBy('exam')->paginate(Input::get('max', 20));

        $this->set_context(array(
            'exams' => $exams
        ));

        return $this->view_make('entries/exams/index');
    }
    
    public function create()
    {
        $input = Input::all();

        $validate = array(
            'codigo' => 'required|integer',
            'exam' => 'required|min:3|max:128',
            'description' => 'required|min:3|max:256'
        );

        $validated = Validator::make($input, $validate);

        if ($validated->fails()) {
            return Response::json(array(
                'success' => false,
                'errors' => $validated->messages()->toArray()
            ));
        }
        
        
         $valida = Exam::where('codigo', '=' , Input::get('codigo'))->get();
         $valida1 = Exam:: where('exam', '=' , Input::get('exam'))->get();
         
         if(count($valida))
         {
            return Response::json(array(
                    'success' => false,
                    'errors' => array('codigo' => 'Desculpe mas esse Código já faz referência a outro exame.')
            ));
         }
         else if(count($valida1))
         {
          return Response::json(array(
                    'success' => false,
                    'errors' => array('exam' => 'Desculpe mas esse exame já existe.')
            ));
         }
        
        
        // model Exam
        $exam = new Exam; 
        $exam->codigo = $input['codigo'];
        $exam->exam = $input['exam'];
        $exam->description = $input['description'];
        $exam->save();

        return Response::json(array(
            'success' => true,
            'redirectTo' => URL::action(
                'ExamsController@index')
        ));
    }
    
    public function delete($id){
    
        $exam = Exam::find($id);
        $exam->delete();
        return Redirect::back();
    }
    
    public function edit(){
        
        $exam = Exam::where('id', '=', Input::get('id'))->firstOrFail();

                       
        $validate = array(
            'codigo' => 'required|integer',
            'exam' => 'required|min:3|max:128',
            'description' => 'required|min:3|max:256'
        );
       
       $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            return Response::json(array(
                'success' => false,
                'errors' => $validated->messages()->toArray()
            ));
        }
        
        
        $valida = Exam:: where('exam', '=' , Input::get('exam'))->get();
                 
        if(count($valida))
         {
          return Response::json(array(
                    'success' => false,
                    'errors' => array('exam' => 'Desculpe mas esse exame já existe.')
            ));
         }
        $exam->codigo = Input::get('codigo');
        $exam->exam = Input::get('exam');
        $exam->description = Input::get('description');
        $exam->save();
               
        return Response::json(array(
            'success' => true,
            'redirectTo' => URL::action(
                'ExamsController@index')
            ));

       }
       
 }