<?php


class JobController extends BaseController {

    /**
     * Operações com os Cargos 
     *
     * @
     */

     public function index(){
     
      $this->set_context(array(
            'active_menu' => 'entries.jobs',
            'filters' => (object) array(
                'codigo' => Input::get('codigo', ''),
                'job' => Input::get('job', ''),
                'max' => Input::get('max', 20)
            ),
            'paginate' => array(
                '10000' => 'Tudo', '10' => '10', '20' => '20', '50' => '50', '100' => '100'
            )
            
        ));

        $jobs = Job::select();

         if (Input::get('codigo')) {
             $jobs->where('codigo', 'like', '%'. Input::get('codigo') .'%');
         }

         if (Input::get('job')) {
             $jobs->where('job', 'like', '%'. Input::get('job') .'%');
         }

        $jobs = $jobs->orderBy('job')->paginate(Input::get('max', 20));

        $this->set_context(array(
            'jobs' => $jobs
        ));

        return $this->view_make('entries/jobs/index');
    }
    
    public function create()
    {     
      
        $validate = array(
            'codigo' => 'required|integer',
            'job' => 'required|min:3|max:128',
            'function' => 'required|min:3|max:256',
        );

        $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            return Response::json(array(
                'success' => false,
                'errors' => $validated->messages()->toArray()
            ));
        }

           
         $valida = Job::where('codigo', '=' , Input::get('codigo'))->get();
         $valida1 = Job:: where('job', '=' , Input::get('job'))->get();
         
         if(count($valida))
         {
            return Response::json(array(
                    'success' => false,
                    'errors' => array('codigo' => 'Desculpe mas esse Código já faz referência a outro cargo.')
            ));
         }
         else if(count($valida1))
         {
          return Response::json(array(
                    'success' => false,
                    'errors' => array('job' => 'Desculpe mas esse cargo já existe.')
            ));
         }
        
        
        $job = new Job;
        $job->codigo = Input::get('codigo');
        $job->job = Input::get('job');
        $job->function = Input::get('function');
        $job->save();

        return Response::json(array(
            'success' => true,
            'redirectTo' => URL::action(
                'JobController@index'
            )
        ));
    }
    
    public function delete($id)
    {
        $job = Job::find($id);
        $job->delete();
        
        return Redirect::back();
    }
    
    public function edit()
    {
        $job = Job::where('id', '=', Input::get('id'))->firstOrFail();

        $validate = array(
            'codigo' => 'required|integer',
            'job' => 'required|min:3|max:128',
            'function' => 'required|min:3|max:256'
        );
       
       $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            return Response::json(array(
                'success' => false,
                'errors' => $validated->messages()->toArray()
            ));
        }
         
       

        $job->codigo = Input::get('codigo');
        $job->job = Input::get('job');
        $job->function = Input::get('function');
        $job->save();
               
        return Response::json(array(
            'success' => true,
            'redirectTo' => URL::action(
                'JobController@index'
             )
        ));
    }
    
      public function listExam($id)
      {
          $job = Job::find($id);
        
         $this->set_context(array(
            'active_menu' => 'entries.jobs',
             'job'  => $job,
             //relacionamento entre cargos e seus exames e procedimentos
             'exams' => Exam::all(),
             'dealings' => Dealing::all()

         ));
          // Select 2 para adicionar exames
         $exames = array();
         
         foreach(Exam::all() as $exam) {
            array_push($exames, array(
                'id' => $exam->id,
                'text' => $exam->exam
            ));
         }
         
         $this->set_context(array(
            'exams' => $exames
         ));
         
         // select2 para adicionar procedimentos
          $procedimentos = array();
         
         foreach(Dealing::all() as $dealing) {
            array_push($procedimentos, array(
                'id' => $dealing->id,
                'text' => $dealing->dealing
            ));
         }
         
         $this->set_context(array(
            'dealings' => $procedimentos
         ));
         
         return $this->view_make('entries/jobs/relations');
      
      }
    
    public function relationExam($id)
    {     
         $job = Job::find($id);
        
         $validate = array(
            'exam' => 'required'
            );

        $validated = Validator::make(Input::all(), $validate);
        
        if ($validated->fails()) {
            return Response::json(array(
                'success' => false,
                'errors' => $validated->messages()->toArray()
            ));
        }
       
        // validação (cadastro de exames repetidos)
        
         $valida = JobExam::where('job_id', '=' , $id)->where('exam_id', '=' , Input::get('exam'))->get();
         
         if(count($valida))
         {
            return Response::json(array(
                    'success' => false,
                    'errors' => array('exam' => 'Impossivel cadastrar o mesmo exame para o mesmo cargo.')
            ));
         }
    
        $exam = new JobExam;
        $exam->id = Input::get('id');
        $exam->exam_id = Input::get('exam');
        $exam->job_id = $job->id;
        
        $exam->save();
        
        return Response::json(array(
            'success' => true,
            'redirectTo' => URL::action(
                'JobController@listExam', array($job->id)
            )
        ));
      
  }
  
    public function delRelationExam($id)
   {
        $job = JobExam::find($id);
        $job->delete();
        
        return Redirect::back();
   
   }

   public function relationDealing($id)
    {     
         $job = Job::find($id);      
        
         $validate = array(
            'dealing' => 'required'
            );

        $validated = Validator::make(Input::all(), $validate);
        
        if ($validated->fails()) {
            return Response::json(array(
                'success' => false,
                'errors' => $validated->messages()->toArray()
            ));
        }
        
       
        // validação (cadastro de procedimentos repetidos)
        
         $valida = JobDealing::where('job_id', '=' , $id)->where('dealing_id', '=' , Input::get('dealing'))->get();
         
         if(count($valida))
         {
            return Response::json(array(
                    'success' => false,
                    'errors' => array('dealing' => 'Impossivel cadastrar o mesmo procedimento para o mesmo cargo.')
            ));
         }
    
        $dealing = new JobDealing;
        $dealing->id = Input::get('id');
        $dealing->dealing_id = Input::get('dealing');
        $dealing->job_id = $job->id;
        
        $dealing->save();
        
        return Response::json(array(
            'success' => true,
            'redirectTo' => URL::action(
                'JobController@listExam', array($job->id)
            )
        ));
      
  }
  
    public function delRelationDealing($id)
   {
        $job = JobDealing::find($id);
        $job->delete();
        
        return Redirect::back();
   
   }

   }