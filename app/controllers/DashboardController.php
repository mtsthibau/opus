<?php


class DashboardController extends BaseController {

    /**
     * Exibe a pagina inicial
     *
     * @return Illuminate\Support\Facades\View
     */
    public function index()
    {
        if (!Auth::check()) {
            return Redirect::action("UserController@get_login");
        }

        $this->set_context(array(
            'active_menu' => ''
        ));

        return $this->view_make('dashboard/index');
    }
}