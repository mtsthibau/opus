<?php


class UserController extends BaseController {

    /**
     * Exibe a página de login
     *
     * @return Illuminate\Support\Facades\View
     */
    public function get_login()
    {
        if (Auth::check()) {
            return Redirect::action("DashboardController@index");
        }

        $this->set_context(array(
            'username' => null,
            'remember' => Input::get('remember', 'off')
        ));

        if (Input::get('inline', false) == true) {
            sleep(1);
            return $this->view_make('user/login-form');
        }

        return $this->view_make('user/login');
    }

    /**
     * Faz autenticação do usuário
     *
     * @return Illuminate\Support\Facades\Response
     */
    public function post_login()
    {
        $this->set_context(array(
            'username' => Input::get('username', ''),
            'remember' => Input::get('remember', 'off')
        ));

        $validate = array(
            "username" => "required",
            "password" => "required"
        );

        $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            $this->set_context(array(
                'field_errors' => $validated->messages()
            ));
            return $this->view_make('user/login');
        }

        $errors = array();
        $login_failed_message = "Desculpe, seu nome de usuário e senha estão incorretos.";
        try {
            Auth::attempt(array(
                'identifier' => Input::get('username'),
                'password' => Input::get('password')
            ));
        }
        catch (JCS\Auth\UserPasswordIncorrectException $e) {
            // Atualizamos as informações de falha na autenticação
            $user = User::where('username', '=', Input::get('username'))->first();
            $user->profile->increment('count_login_failed');
            $user->profile->last_login_failed = date("Y-m-d H:i:s");
            $user->profile->save();

            array_push($errors, $login_failed_message);
        }
        catch (Exception $e) {
            array_push($errors, $login_failed_message);
        }

        if (Auth::user() && !count(Auth::user()->roles)) {
            array_push($errors, "Este usuário não pode autenticar por que não pertence a nenhum grupo. ".
                                "Por favor contate um administrador.");
            Auth::logout();
        }

        if (count($errors) > 0) {
            $this->set_context(array(
                'form_errors' => $errors
            ));
            return $this->view_make('user/login');
        }

        // Atualizamos as informações de último login do usuário
        $user = Auth::user();
        $user->profile->increment('count_login');
        $user->profile->previous_login = $user->profile->last_login;
        $user->profile->last_login = date("Y-m-d H:i:s");
        $user->profile->save();

        return Redirect::action('DashboardController@index');
    }

    /**
     * Faz logout do usuário
     *
     * @return Illuminate\Support\Facades\Redirect
     */
    public function logout()
    {
        Auth::logout();
        return Redirect::action('DashboardController@index');
    }

}