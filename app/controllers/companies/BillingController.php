<?php



class BillingController extends BaseController {

    /**
     * Relacionamento entre a empresa e seus respectivos procedimentos e exames.
     *
     * @
     */

     public function index(){
       $this->set_context(array(
            'active_menu' => 'entries.billing',
            'companies' => Companie::orderBy('id', 'companie')->get()      

      ));
        
        return $this->view_make('billing/list/index');
    }

    }
   
       