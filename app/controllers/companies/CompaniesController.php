<?php

class CompaniesController extends BaseController {

    /**
     * Operações com os Empresas 
     *
     * @
     */

     public function index(){
     
      $this->set_context(array(
            'active_menu' => 'entries.companies',//
            'filters' => (object) array(
                'companies' => Input::get('companies', ''),
                'codigo' => Input::get('codigo', ''),
                'address' => Input::get('address', ''),
                'max' => Input::get('max', 20)
            ),
            'paginate' => array(
                '10000' => 'Tudo', '10' => '10', '20' => '20', '50' => '50', '100' => '100'
            ),
            
             
        ));

        $companies = Companie::select();
        
        if (Input::get('codigo')) {
            $companies->where('codigo', 'like', '%'. Input::get('codigo') .'%');
        }    
         
        if (Input::get('companies')) {
            $companies->where('companie', 'like', '%'. Input::get('companies') .'%');
        }
        if (Input::get('address')) {
            $companies->where('address', 'like', '%'. Input::get('address') .'%');
        }    
        $companies = $companies->orderBy('companie')->paginate(Input::get('max', 20));

        $this->set_context(array(
            'companies' => $companies
        ));

        return $this->view_make('companies/list/index');
    }
    
    public function create()
    {
    
        $this->set_context(array(
            'jobs' => Job::all()
            
        ));

        
        $estados = array(
            "AC",
            "AL",
            "AP",
            "AM",
            "BA",
            "CE",
            "DF",
            "ES",
            "GO",
            "MA",
            "MT",
            "MS",
            "MG",
            "PA",
            "PB",
            "PR",
            "PE",
            "PI",
            "RJ",
            "RN",
            "RS",
            "RR",
            "SC",
            "SP",
            "SE",
            "TO"
        );

        $this->set_context(array(
            'active_menu' => 'entries.companies'
        ));
        
        //validação do Form
            
        $this->set_context(array(
            'codigo' => Input::get('codigo', ''),
            'cnpj'   => Input::get('cnpj', ''),
            'companie' => Input::get('companie', ''),
            'cep' => Input::get('cep', ''),
            'number' => Input::get('number', ''),
            'city' => Input::get('city', ''),     
            'uf'   => Input::get('uf', ''),
            'district' => Input::get('district', ''), 
            'phone'   => Input::get('phone', ''),
            'address'  => Input::get('address', ''),
            'email' => Input::get('email', ''),
            'estados' => $estados
        ));
        
        if (!Input::has('submit')) {
            return $this->view_make('companies/create/index');
        }
            
        $validate = array(
            'codigo' => 'required|integer|unique:companies',
            'cnpj'   => 'required|min:18|max:19',  
            'companie' => 'required|max:128|unique:companies',
            'cep' => 'required|max:9|min:9',
            'number' => 'required|max:8',   
            'city' => 'required|max:128',    
            'uf'   => 'required',     
            'district' => 'required|max:128', 
            'phone'   => 'required|min:13|max:13', 
            'address'  => 'required|max:248',
            'email' => 'required|email'
       );
      
        $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            $this->set_context(array(
                'field_errors' => $validated->messages()
            ));
            return $this->view_make('companies/create/index');
        }
               
        
        $companie = new Companie;
        $companie->codigo = Input::get('codigo');
        $companie->companie = Input::get('companie');
        $companie->cnpj = Input::get('cnpj');
        $companie->cep = Input::get('cep');
        $companie->number = Input::get('number');
        $companie->city = Input::get('city');
        $companie->uf = Input::get('uf');
        $companie->district = Input::get('district');
        $companie->phone = Input::get('phone');
        $companie->address = Input::get('address');
        $companie->email = Input::get('email');
        $companie->save();
     
        return Redirect::action('CompaniesController@index');

       }
       
     public function detail($id){
     
      $companie = Companie::find($id);
      $this->set_context(array(
            'active_menu' => 'entries.companies', 
             'companie'  => $companie,
             //relacionamento entre Empresas e seus exames e procedimentos
             'exams' => Exam::all(),
             'dealings' => Dealing::all(),
             'collaborators' => Collaborator::all()
         ));
         // Select 2 para adicionar exames
         $exames = array();
         
         foreach(Exam::all() as $exam) {
            array_push($exames, array(
                'id' => $exam->id,
                'text' => $exam->exam
            ));
         }
         
         $this->set_context(array(
            'exams' => $exames
         ));
         
         // select2 para adicionar procedimentos
          $procedimentos = array();
         
         foreach(Dealing::all() as $dealing) {
            array_push($procedimentos, array(
                'id' => $dealing->id,
                'text' => $dealing->dealing
            ));
         }
         
         $this->set_context(array(
            'dealings' => $procedimentos
         ));  
         
         
          $colaboradores = array();
         
         foreach(Collaborator::all() as $col) {
            array_push($colaboradores, array(
                'id' => $col->id,
                'text' => $col->nome
            ));
         }
         
         $this->set_context(array(
            'collaborators' => $colaboradores
         ));  
         

        return $this->view_make('companies/detail/index');

    }
    
    public function edit($id){
     $this->set_context(array(
            'jobs' => Job::all()
            
        ));
    $companie = Companie::find($id);
      $this->set_context(array(
            'active_menu' => 'entries.companies', 
             'companie'  => $companie  ));
                            
   $estados = array(
            "AC",
            "AL",
            "AP",
            "AM",
            "BA",
            "CE",
            "DF",
            "ES",
            "GO",
            "MA",
            "MT",
            "MS",
            "MG",
            "PA",
            "PB",
            "PR",
            "PE",
            "PI",
            "RJ",
            "RN",
            "RS",
            "RR",
            "SC",
            "SP",
            "SE",
            "TO"
        );

        $this->set_context(array(
            'active_menu' => 'entries.companies'
        ));
        
        //validação do Form
        //busca campos do form para leva-los de volta caso a alteração falhe    
        $this->set_context(array(
            'codigo' => Input::get('codigo', $companie->codigo),
            'cnpj'   => Input::get('cnpj', $companie->cnpj),
            'companie' => Input::get('companie', $companie->companie),
            'cep' => Input::get('cep', $companie->cep),
            'number' => Input::get('number', $companie->number),
            'city' => Input::get('city', $companie->city),  
            'uf'   => Input::get('uf', $companie->uf),
            'district' => Input::get('district', $companie->district),
            'phone'   => Input::get('phone', $companie->phone),
            'address'  => Input::get('address', $companie->address),
            'email' => Input::get('email', $companie->email),
            'estados' => Input::get('estados', $estados)
        ));
        
        if (!Input::has('submit')) {
            return $this->view_make('companies/edit/index');
        }
            
        $validate = array(
            'codigo' => 'required|integer',
            'cnpj'   => 'required|max:18',  
            'companie' => 'required|max:128',
            'cep' => 'required|max:9|min:9',
            'number' => 'required|max:8',   
            'city' => 'required|max:128',    
            'uf'   => 'required|max:2|min:2',     
            'district' => 'required|max:128', 
            'phone'   => 'required|min:13|max:13', 
            'address'  => 'required|max:248',
            'email' => 'required|email',
       );
      
        $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            $this->set_context(array(
                'field_errors' => $validated->messages()
            ));
            return $this->view_make('companies/edit/index');
        }
        
        $companie->codigo = Input::get('codigo');
        $companie->companie = Input::get('companie');
        $companie->cnpj = Input::get('cnpj');
        $companie->cep = Input::get('cep');
        $companie->number = Input::get('number');
        $companie->city = Input::get('city');
        $companie->uf = Input::get('uf');
        $companie->district = Input::get('district');
        $companie->phone = Input::get('phone');
        $companie->address = Input::get('address');
        $companie->email = Input::get('email');
        
        $companie->save();
     
        return Redirect::action('CompaniesController@index');

        
    }  
    
    public function relationExam($empresa)
    {
      $companie = Companie::find($empresa);



        $validate = array(
            'exam' => 'required',
            'price'   => 'required|max:15,2'

            );

        $validated = Validator::make(Input::all(), $validate);
        
        if ($validated->fails()) {
            return Response::json(array(
                'success' => false,
                'errors' => $validated->messages()->toArray()
            ));
        }
       
        // validação (cadastro de exames repetidos)
        
         $valida = CompanieExam::where('companie_id', '=' , $empresa)->where('exam_id', '=' , Input::get('exam'))->get();
                        
         if(count($valida))
         {
            return Response::json(array(
                    'success' => false,
                    'errors' => array('exam' => 'Impossivel cadastrar o mesmo exame novamente.')
                ));
         }

        $exam = new CompanieExam;
        $exam->id = Input::get('id');
        $exam->exam_id = Input::get('exam');
        $exam->price = Input::get('price');
        $exam->companie_id = $companie->id;
        
        $exam->save();

        
        return Response::json(array(
            'success' => true,
            'redirectTo' => URL::action(
                'CompaniesController@detail', array($companie->id)
            )
        ));
    }
        
    public function delRelation($exam_id)
    {
        $exam = CompanieExam::where('id', '=', $exam_id)->delete();
        return Redirect::back();
    }    
    
    public function editRelation()
    {        
        
        $validate = array(
            'price'   => 'required|max:15, 2',
            'exam'   => 'required'
       );
      
         $validated = Validator::make(Input::all(), $validate);
        
        if ($validated->fails()) {
            return Response::json(array(
                'success' => false,
                'errors' => $validated->messages()->toArray()
            ));
        }
        
        $exam = CompanieExam::where('id', '=' , Input::get('id'))->firstOrFail();

        $exam->price = Input::get('price');
              
        $exam->save();
     
        return Response::json(array(
            'success' => true,
            'redirectTo' => URL::action(
            'CompaniesController@detail', array($exam->companie_id))
            ));
    }
    
    
     public function relationDealing($empresa)
    {
      $companie = Companie::find($empresa);

        $validate = array(
            'dealing' => 'required',
            'price'   => 'required|max:15,2',      

            );

        $validated = Validator::make(Input::all(), $validate);
        
        if ($validated->fails()) {
            return Response::json(array(
                'success' => false,
                'errors' => $validated->messages()->toArray()
            ));
        }
                 
           // validação (cadastro de procedimentos repetidos)
        
         $valida = CompanieDealing::where('companie_id', '=' , $empresa)->where('dealing_id', '=' , Input::get('dealing'))->get();
                        
         if(count($valida))
         {
            return Response::json(array(
                    'success' => false,
                    'errors' => array('dealing' => 'Impossivel cadastrar o mesmo procedimento novamente.')
                ));
         }
         
             
        $dealing = new CompanieDealing;
        $dealing->id = Input::get('id');
        $dealing->dealing_id = Input::get('dealing');
        $dealing->price = Input::get('price');
        $dealing->companie_id = $companie->id;
        
        $dealing->save();
        
        
        return Response::json(array(
            'success' => true,
            'redirectTo' => URL::action(
            'CompaniesController@detail', array($companie->id)
            )));
        }
        
    public function delDealing($dealing_id)
    {
        CompanieDealing::find($dealing_id)->delete();
        return Redirect::back();
    }    
    
    public function editDealing()
    {        
       $validate = array(
            'price'   => 'required|max:15, 2'
       );
      
         $validated = Validator::make(Input::all(), $validate);
        
        if ($validated->fails()) {
            return Response::json(array(
                'success' => false,
                'errors' => $validated->messages()->toArray()
            ));
        }
        
        $dealing = CompanieDealing::where('id', '=' , Input::get('id'))->firstOrFail();

        $dealing->price = Input::get('price');
        $dealing->save();
     
        return Response::json(array(
            'success' => true,
            'redirectTo' => URL::action(
            'CompaniesController@detail', array($dealing->companie_id))
            ));
    }
    
      public function relationCollaborator($empresa)
    {
      $companie = Companie::find($empresa);

        $validate = array(
            'collaborator_id' => 'required',
            'date'   => 'required|min:10|max:10'      

            );

        $validated = Validator::make(Input::all(), $validate);
        
        if ($validated->fails()) {
            return Response::json(array(
                'success' => false,
                'errors' => $validated->messages()->toArray()
            ));
        }
         
          $valida = CompanieCollaborators::where('companie_id', '=' , $empresa)->where('collaborator_id', '=' , Input::get('collaborator_id'))->get();
                        
         if(count($valida))
         {
            return Response::json(array(
                    'success' => false,
                    'errors' => array('collaborator_id' => 'Impossivel cadastrar o mesmo colaborador novamente.')
                ));
         }
             
        $collaborator = new CompanieCollaborators;
        $collaborator->id = Input::get('id');
        $collaborator->collaborator_id = Input::get('collaborator_id');
        $collaborator->date = Input::get('date');
        $collaborator->companie_id = $companie->id;
        
        $collaborator->save();
        
        
        return Response::json(array(
            'success' => true,
            'redirectTo' => URL::action(
            'CompaniesController@detail', array($companie->id)
            )));
        }
        
    public function delCollaborator($collaborator_id)
    {
        $collaborator = CompanieCollaborators::where('id', '=', $collaborator_id)->delete();
        return Redirect::back();
    }    
    
    public function editCollaborator()
    {        
       $validate = array(
            'date'   => 'required|min:10|max:10'
       );
      
         $validated = Validator::make(Input::all(), $validate);
        
        if ($validated->fails()) {
            return Response::json(array(
                'success' => false,
                'errors' => $validated->messages()->toArray()
            ));
        }
        
        $collaborator = CompanieCollaborators::where('id', '=' , Input::get('id'))->firstOrFail();
        $collaborator->date = Input::get('date');
        $collaborator->save();
     
        return Response::json(array(
            'success' => true,
            'redirectTo' => URL::action(
            'CompaniesController@detail', array($collaborator->companie_id))
            ));
    }
    
}