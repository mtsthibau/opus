<?php


class ProfessionalController extends BaseController {

    /**
     * Exibe a pagina inicial
     *
     * @return Illuminate\Support\Facades\View
     */
    public function index()
    {
       $this->set_context(array(
            'active_menu' => 'entries.professionals',
            'filters' => (object) array(
                'codigo' => Input::get('codigo', ''),
                'nome' => Input::get('nome', ''),
                'tipo_conselho' => Input::get('tipo_conselho', ''),
                'max' => Input::get('max', 20)
            ),
            'paginate' => array(
                '10000' => 'Tudo', '10' => '10', '20' => '20', '50' => '50', '100' => '100'
            )
        ));

        $professinals = Professional::select();
        
        if (Input::get('codigo')) {
            $professinals->where('codigo', 'like', '%'. Input::get('codigo') .'%');
        }    
         
        if (Input::get('nome')) {
            $professinals->where('nome', 'like', '%'. Input::get('nome') .'%');
        }
        if (Input::get('tipo_conselho')) {
            $professinals->where('tipo_conselho', 'like', '%'. Input::get('tipo_conselho') .'%');
        }    
        $professionals = $professinals->orderBy('nome')->paginate(Input::get('max', 20));

        $this->set_context(array(
            'professionals' => $professionals
        ));

        return $this->view_make('companies/professional/list/index');
    }
     
    
    public function create()
    {
        $this->set_context(array(
            'active_menu' => 'entries.professionals'
        ));
        
        //validação do Form
            
        $this->set_context(array(
            'codigo' => Input::get('codigo', ''),
            'nome'   => Input::get('nome', ''),
            'tipo_conselho' => Input::get('tipo_conselho', ''),
            'numero_conselho' => Input::get('numero_conselho', ''),
            'telefone' => Input::get('telefone', ''),
            'celular' => Input::get('celular', ''),
            'email' => Input::get('email', ''),
          
        ));
        
        if (!Input::has('submit')) {
            return $this->view_make('/companies/professional/create/index');
        }
            
        $validate = array(
            'codigo' => 'required|integer',
            'nome'   => 'required|max:128',  
            'tipo_conselho' => 'required|max:5',
            'numero_conselho' => 'required|max:10',
            'telefone' => 'required|max:14',
            'celular' => 'required|max:14',
            'email' => 'required'
            
       );
      
        $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            $this->set_context(array(
                'field_errors' => $validated->messages()
            ));
            return $this->view_make('/companies/professional/create/index');
        }
        $professional = new Professional;
        $professional->codigo = Input::get('codigo');
        $professional->nome = Input::get('nome');
        $professional->tipo_conselho = Input::get('tipo_conselho');
        $professional->numero_conselho = Input::get('numero_conselho');
        $professional->telefone = Input::get('telefone');
        $professional->celular = Input::get('celular');
        $professional->email = Input::get('email');
        $professional->save();
     
        return Redirect::action('ProfessionalController@index');

       }
       
     public function detail($id){
     
     $professional = Professional::find($id);
      $this->set_context(array(
            'active_menu' => 'entries.professionals', 
             'professional'  => $professional
        ));
        return $this->view_make('companies/professional/detail/index');
     }
     
        public function edit($id){
                 
    $professional = Professional::find($id);
      $this->set_context(array(
            'active_menu' => 'entries.professionals', 
             'professional'  => $professional  ));
                            
        //validação do Form
            
        $this->set_context(array(
            'codigo' => Input::get('codigo', $professional->codigo),
            'nome'   => Input::get('nome', $professional->nome),
            'tipo_conselho' => Input::get('tipo_conselho', $professional->tipo_conselho),
            'numero_conselho' => Input::get('numero_conselho', $professional->numero_conselho),   
            'telefone' => Input::get('telefone', $professional->telefone),   
            'celular' => Input::get('celular', $professional->celular),  
            'email' => Input::get('email', $professional->email)   
                 ));
        
        if (!Input::has('submit')) {
            return $this->view_make('companies/professional/edit/index');
        }
            
        $validate = array(
            'codigo' => 'required|integer',
            'nome'   => 'required|max:128',  
            'tipo_conselho' => 'required|max:5',
            'numero_conselho' => 'required|max:10',
            'telefone' => 'required|max:14',
            'celular' => 'required|max:14',
            'email' => 'required'

       );
      
        $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            $this->set_context(array(
                'field_errors' => $validated->messages()
            ));
            return $this->view_make('companies/professional/edit/index');
        }
        var_dump($professional->codigo);
        
        $professional->codigo = Input::get('codigo');
        $professional->nome = Input::get('nome');
        $professional->tipo_conselho = Input::get('tipo_conselho');
        $professional->numero_conselho = Input::get('numero_conselho');
        $professional->telefone = Input::get('telefone');
        $professional->celular = Input::get('celular');
        $professional->email = Input::get('email');

        $professional->save();
     
        return Redirect::action('ProfessionalController@index');

        
    }
   
    
}