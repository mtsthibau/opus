<?php


class RelationjobsController extends BaseController {

    /**
     * Operações com os Empresas e cargos
     *
     * @
     */

     public function index($empresa){
     
      $companie = Companie::find($empresa);
      
      $this->set_context(array(
            'active_menu' => 'entries.companies',
            'companie'  => $companie,
            //relacionamento entre Empresas e seus exames
            'jobs' => Job::all()
           
        ));
        
         $cargos = array();
         
         foreach(Job::all() as $job) {
            array_push($cargos, array(
                'id' => $job->id,
                'text' => $job->job
            ));
         }
         
         $this->set_context(array(
            'jobs' => $cargos
         ));
         
        
        
        
        return $this->view_make('companies/jobs/index');
    }
    
    public function create($empresa){
    
        $companie = Companie::find($empresa);
        
        $validate = array(
            'job' => 'required',
        );

        $validated = Validator::make(Input::all(), $validate);
        
        if ($validated->fails()) {
            return Response::json(array(
                'success' => false,
                'errors' => $validated->messages()->toArray()
            ));
        }
        
        // validação (cadastro de cargos repetidos)
        
         $valida = CompanieJob::where('companie_id', '=' , $empresa)->where('job_id', '=' , Input::get('job'))->get();
                        
         if(count($valida))
         {
            return Response::json(array(
                    'success' => false,
                    'errors' => array('job' => 'Impossivel cadastrar um cargo ja existente para a mesma empresa.')
                ));
         }
                 
        $job = new CompanieJob;
        $job->job_id = Input::get('job');
        $job->companie_id = $companie->id;
        
        $job->save();
        
        
         return Response::json(array(
            'success' => true,
            'redirectTo' => URL::action(
            'RelationjobsController@index', array($companie->id)
            )));
        
        }
        
   
    
    public function delete($id){
    
        $job = CompanieJob::where('id', '=', $id)->delete();
        return Redirect::back();
        
    }
 }