<?php


class CollaboratorController extends BaseController {

    /**
     * Operações com os Empresas 
     *
     * @
     */

     public function index(){
     
      $this->set_context(array(
            'active_menu' => 'entries.collaborators',
            'filters' => (object) array(
                'codigo' => Input::get('codigo', ''),
                'nome' => Input::get('nome', ''),
                'cpf' => Input::get('cpf', ''),
                'max' => Input::get('max', 20)
            ),
            'paginate' => array(
                '10000' => 'Tudo', '10' => '10', '20' => '20', '50' => '50', '100' => '100'
            )
        ));

        $collaborators = Collaborator::select();
        
        if (Input::get('codigo')) {
            $collaborators->where('codigo', 'like', '%'. Input::get('codigo') .'%');
        }    
         
        if (Input::get('nome')) {
            $collaborators->where('nome', 'like', '%'. Input::get('nome') .'%');
        }
        if (Input::get('cpf')) {
            $collaborators->where('cpf', 'like', '%'. Input::get('cpf') .'%');
        }    
        $collaborators = $collaborators->orderBy('nome')->paginate(Input::get('max', 20));

        $this->set_context(array(
            'collaborators' => $collaborators
        ));

        return $this->view_make('companies/collaborator/list/index');
    }
           
    
    public function create()
    {
        $estados = array(
            "AC",
            "AL",
            "AP",
            "AM",
            "BA",
            "CE",
            "DF",
            "ES",
            "GO",
            "MA",
            "MT",
            "MS",
            "MG",
            "PA",
            "PB",
            "PR",
            "PE",
            "PI",
            "RJ",
            "RN",
            "RS",
            "RR",
            "SC",
            "SP",
            "SE",
            "TO"
        );

        $this->set_context(array(
            'active_menu' => 'entries.collaborators',
        ));
        
        //validação do Form
            
        $this->set_context(array(
            'codigo' => Input::get('codigo', ''),
            'cpf'   => Input::get('cpf', ''),
            'nome' => Input::get('nome', ''),
            'cep' => Input::get('cep', ''),
            'number' => Input::get('number', ''),
            'city' => Input::get('city', ''),     
            'uf'   => Input::get('uf', ''),
            'district' => Input::get('district', ''), 
            'phone'   => Input::get('phone', ''),
            'address'  => Input::get('address', ''),
            'email' => Input::get('email', ''),
            'estados' => $estados
        ));
        
        if (!Input::has('submit')) {
            return $this->view_make('companies/collaborator/create/index');
        }
            
        $validate = array(
            'codigo' => 'required|integer',
            'cpf'   => 'required|max:12',  
            'nome' => 'required|max:128',
            'cep' => 'required|max:9|min:9',
            'number' => 'required|max:8',   
            'city' => 'required|max:128',    
            'uf'   => 'required',     
            'district' => 'required|max:248', 
            'phone'   => 'required|min:13|max:14', 
            'address'  => 'required|max:248',
            'email' => 'required|email'
       );
      
        $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            $this->set_context(array(
                'field_errors' => $validated->messages()
            ));
            return $this->view_make('companies/collaborator/create/index');
        }
        $collaborator = new Collaborator;
        $collaborator->codigo = Input::get('codigo');
        $collaborator->nome = Input::get('nome');
        $collaborator->cpf = Input::get('cpf');
        $collaborator->cep = Input::get('cep');
        $collaborator->number = Input::get('number');
        $collaborator->city = Input::get('city');
        $collaborator->uf = Input::get('uf');
        $collaborator->district = Input::get('district');
        $collaborator->phone = Input::get('phone');
        $collaborator->address = Input::get('address');
        $collaborator->email = Input::get('email');
        $collaborator->save();
     
        return Redirect::action('CollaboratorController@index');

       }
       
       public function detail($id){
     
     $collaborator = Collaborator::find($id);
      $this->set_context(array(
            'active_menu' => 'entries.collaborators',
             'collaborator'  => $collaborator,
             'jobs' => Job::all()
        ));

          $cargos = array();
         
         foreach(Job::all() as $job) {
            array_push($cargos, array(
                'id' => $job->id,
                'text' => $job->job
            ));
         }
         
         $this->set_context(array(
            'jobs' => $cargos
         ));
         
        
        
        return $this->view_make('companies/collaborator/detail/index');
    }
    
   public function edit($id){
                 
    $collaborator = Collaborator::find($id);
      $this->set_context(array(
            'active_menu' => 'entries.collaborators',
             'collaborator'  => $collaborator  
      ));
                            
   $estados = array(
            "AC",
            "AL",
            "AP",
            "AM",
            "BA",
            "CE",
            "DF",
            "ES",
            "GO",
            "MA",
            "MT",
            "MS",
            "MG",
            "PA",
            "PB",
            "PR",
            "PE",
            "PI",
            "RJ",
            "RN",
            "RS",
            "RR",
            "SC",
            "SP",
            "SE",
            "TO"
        );

        $this->set_context(array(
            'active_menu' => 'entries.collaborators'
        ));
        
        //validação do Form
            
        $this->set_context(array(
            'codigo' => Input::get('codigo', $collaborator->codigo),
            'cpf'   => Input::get('cpf', $collaborator->cpf),
            'nome' => Input::get('nome', $collaborator->nome),
            'cep' => Input::get('cep', $collaborator->cep),
            'number' => Input::get('number', $collaborator->number),
            'city' => Input::get('city', $collaborator->city),  
            'uf'   => Input::get('uf', $collaborator->uf),
            'district' => Input::get('district', $collaborator->district),
            'phone'   => Input::get('phone', $collaborator->phone),
            'address'  => Input::get('address', $collaborator->address),
            'email' => Input::get('email', $collaborator->email),
            'estados' => Input::get('estados', $estados)
        ));
        
        if (!Input::has('submit')) {
            return $this->view_make('companies/collaborator/edit/index');
        }
            
        $validate = array(
            'codigo' => 'required|integer',
            'cpf'   => 'required|max:12',  
            'nome' => 'required|max:128',
            'cep' => 'required|max:9|min:9',
            'number' => 'required|max:8',   
            'city' => 'required|max:248',    
            'uf'   => 'required|max:2|min:2',     
            'district' => 'required|max:128', 
            'phone'   => 'required|min:13|max:14', 
            'address'  => 'required|max:248',
            'email' => 'required|email',
       );
      
        $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            $this->set_context(array(
                'field_errors' => $validated->messages()
            ));
            return $this->view_make('companies/collaborator/edit/index');
        }
        
        $collaborator->codigo = Input::get('codigo');
        $collaborator->nome = Input::get('nome');
        $collaborator->cpf = Input::get('cpf');
        $collaborator->cep = Input::get('cep');
        $collaborator->number = Input::get('number');
        $collaborator->city = Input::get('city');
        $collaborator->uf = Input::get('uf');
        $collaborator->district = Input::get('district');
        $collaborator->phone = Input::get('phone');
        $collaborator->address = Input::get('address');
        $collaborator->email = Input::get('email');
        $collaborator->save();
     
        return Redirect::action('CollaboratorController@index');

        
    }   
    
    public function relationJob($id)
    {
    
        $collaborator = CollaboratorJob::find($id);
        
        $validate = array(
            'job' => 'required',
            'date' => 'required|min:10|max:10'
            );

        $validated = Validator::make(Input::all(), $validate);
        
        if ($validated->fails()) {
            return Response::json(array(
                'success' => false,
                'errors' => $validated->messages()->toArray()
            ));
        }
       
        // validação (cadastro de jobs repetidos)
        
         $valida = CollaboratorJob::where('collaborator_id', '=' , $id)->where('job_id', '=' , Input::get('job'))->get();
                        
         if(count($valida))
         {
            return Response::json(array(
                    'success' => false,
                    'errors' => array('job' => 'Impossivel cadastrar novamente o mesmo cargo para este colaborador')
                ));
         }

        $job = new CollaboratorJob;
        $job->id = Input::get('id');
        $job->date = Input::get('date');
        $job->job_id = Input::get('job');
        $job->collaborator_id = $id;
        
        $job->save();

        
        return Response::json(array(
            'success' => true,
            'redirectTo' => URL::action(
                'CollaboratorController@detail', array($id)
            )
        ));
    
    
    }
    
    
    public function delJob($id)
    {
     $job = CollaboratorJob::where('id', '=', $id)->delete();
        return Redirect::back();
    
    }
    
    
    
    }
   
       