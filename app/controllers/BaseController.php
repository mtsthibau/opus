<?php


class BaseController extends Controller {

    /**
     * Context base
     *
     * @var array
     */
    private $context = array(
        'template' => array(),
        'json' => array(),
        'cookie' => array()
    );

    /**
     * Definimos algumas variáveis retornadas em todas requisicoes
     *
     */
    public function __construct()
    {
        $this->context['template'] = array(
            'inline' => false,
            'field_errors' => array(),
            'form_errors' => array()
        );
    }

    /**
     * Prepara o context enviado para o javascript
     *
     * @param array $contexts
     */
    public function set_json_context($contexts = array())
    {
        foreach ($contexts as $key => $value) {
            $this->context['json'][$key] = $value;
        }
    }

    /**
     * Prepara o context enviado para o template
     *
     * @param array $contexts
     */
    public function set_context($contexts = array())
    {
        foreach ($contexts as $key => $value) {
            $this->context['template'][$key] = $value;
        }
    }

    /**
     * Constroi o template
     *
     * @param string $template
     * @return \Illuminate\View\View
     */
    public function view_make($template)
    {
        return View::make($template)
            ->with('alertToHeader', Session::get('alertToHeader', null))
            ->with('json', json_encode($this->context['json']))
            ->with('context', (object) $this->context['template']);
    }
}