<?php

use JCS\Auth\Models\Role,
    JCS\Auth\Models\Permission,
    JCS\Auth\Models\PermissionScope;


class BackendGroupController extends BaseController {

    /**
     * Exibe a lista de Grupos
     *
     * @return Illuminate\Support\Facades\View
     */
    public function index()
    {
        $this->set_context(array(
            'active_menu' => 'admin.groups',
            'groups' => Role::orderBy('level', 'desc')->get()
        ));

        return $this->view_make('backend/group/index');
    }
    /**
     * Página com detalhes do grupo selecionado
     *
     * @param $group
     * @return Illuminate\Support\Facades\View
     */
    public function view($group)
    {
        $group = Role::where('tag', '=', $group)->firstOrFail();
        $group_scopes = array();
        $another_scopes = array();

        foreach ($group->permissions()->orderBy('scope_id', 'asc')->get() as $permission) {
            if (!array_key_exists($permission->scope->id, $group_scopes)) {
                $group_scopes[$permission->scope->id] = array(
                    'name' => $permission->scope->name,
                    'permissions' => array()
                );
            }
            $group_scopes[$permission->scope->id]['permissions'][$permission->id] = $permission;
        }

        foreach (Permission::orderBy('scope_id', 'asc')->get() as $permission) {
            if (!array_key_exists($permission->scope->id, $another_scopes)) {
                $another_scopes[$permission->scope->id] = array(
                    'name' => $permission->scope->name,
                    'permissions' => array()
                );
            }
            if (!array_key_exists($permission->scope->id, $group_scopes) ||
                !array_key_exists($permission->id, $group_scopes[$permission->scope->id]['permissions'])) {
                array_push($another_scopes[$permission->scope->id]['permissions'], $permission);
            }
        }

        $this->set_context(array(
            'active_menu' => 'user.groups',
            'group' => $group,
            'permissions' => PermissionScope::all(),
            'group_scopes' => $group_scopes,
            'another_scopes' => $another_scopes
        ));

        return $this->view_make('backend/group/view');
    }
}