<?php


class BackendSystemController extends BaseController {

    /**
     * Exibe página com as configurações gerais do servidor
     *
     * @return Illuminate\Support\Facades\View
     */
    public function general()
    {
        $this->set_context(array(
            'active_menu' => 'system.general'
        ));
        return $this->view_make('backend/system/general');
    }
}
