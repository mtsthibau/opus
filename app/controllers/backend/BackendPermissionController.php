<?php

use JCS\Auth\Models\Role,
    JCS\Auth\Models\Permission,
    JCS\Auth\Models\PermissionScope;


class BackendPermissionController extends BaseController {

    /**
     * Exibe a lista de Grupos
     *
     * @return Illuminate\Support\Facades\View
     */
    public function index()
    {
        $this->set_context(array(
            'active_menu' => 'admin.permissions',
            'permissions' => PermissionScope::all()
        ));

        return $this->view_make('backend/permission/index');
    }

    /**
     * Associa uma permissão a um grupo
     *
     * @param $group
     * @return Illuminate\Support\Facades\Redirect
     */
    public function permission_set($group)
    {
        if (Input::get('selected-permission') != -1) {
            $group = Role::where('tag', '=', $group)->firstOrFail();
            $permResult = array(Input::get('selected-permission'));

            foreach ($group->permissions as $gperm) {
                array_push($permResult, $gperm->id);
            }

            $group->permissions()->sync($permResult);
        }

        return Redirect::back();
    }

    /**
     * Remove uma permissão de um grupo
     *
     * @param $group
     * @param $permission
     * @return Illuminate\Support\Facades\Redirect
     */
    public function permission_unset($group, $permission)
    {
        // Precisamos verificar se há outro grupo com a permissão
        // de administrador de sistema, pois sem o mesmo o sistema
        // não teria mais administrador.
        if ($permission == "sys_admin") {
            $another_sys = false;
            foreach (Role::all() as $dbgroup) {
                if ($dbgroup->can('sys_admin') && $dbgroup->tag != $group) {
                    $another_sys = true;
                }
            }

            if (!$another_sys) {
                return Redirect::back()->with("alertToHeader", array(
                    'type' => 'error',
                    'message' => 'Falha ao remover permissão do Grupo. É necessário que pelo menos um grupo '.
                        'tenha a permissão de Administrador do Sistema.'
                ));
            }
        }

        $group = Role::where('tag', '=', $group)->firstOrFail();
        $permission = Permission::where('tag', '=', $permission)->firstOrFail();

        $permResult = array();
        foreach ($group->permissions as $gperm) {
            if ($gperm->id != $permission->id) {
                array_push($permResult, $gperm->id);
            }
        }
        $group->permissions()->sync($permResult);

        return Redirect::back();
    }
}