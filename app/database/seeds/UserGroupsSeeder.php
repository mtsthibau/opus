<?php

class UserGroupsSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command->info('Populando: grupos de usuario');

        $grupos = array(
            array('Administrador do Sistema', 'sys_admin', 10),
            array('Clínica', 'clinical', 6),
            array('Cliente', 'client', 4),
            array('Usuário', 'user', 1)
        );

        foreach ($grupos as $grupo) {
            DB::table('user_roles')->insert(array(
                'name' => $grupo[0],
                'tag' => $grupo[1],
                'level' => $grupo[2],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ));
        }
    }

}

