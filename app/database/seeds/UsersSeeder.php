<?php

use JCS\Auth\Models\Role;


class UsersSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command->info('Populando: administradores');

        $user = new User;
        $user->username = "mcj";
        $user->password = "zsedcx";
        $user->email = strtolower('joselmocs@gmail.com');
        $user->verified = 1;
        $user->save();

        $user->profile->fullname = "MCJ";
        $user->profile->avatar = "avatar";
        $user->profile->save();
        $user->roles()->sync(array(Role::where('tag', '=', 'sys_admin')->firstOrFail()->id));
    }
}