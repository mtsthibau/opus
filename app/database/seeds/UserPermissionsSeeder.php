<?php

use JCS\Auth\Models\Role,
    JCS\Auth\Models\Permission,
    JCS\Auth\Models\PermissionScope;


class UserPermissionsSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command->info('Populando: permissoes');

        $scopes = array(
            // Globais
            'Permissões Globais' => array(
                // Administrador do Sistema
                array('Administrador do Sistema', 'sys_admin',
                    'Capacidade de executar todas as funções de administração. Deve haver pelo menos um grupo ' .
                    'com esta autorização.'
                )
            ),

            'Usuários' => array(
                array('Gerenciar', 'user.manager', 'Capacidade criar, editar e remover usuários do sistema.')
            ),
            'Funcionários' => array(
                array('Leitura', 'changelog.item_view', 'Capacidade de visualizar conteúdo de versão.'),
                array('Criar Item', 'changelog.item_create', 'Capacidade de criar conteúdo de versão a partir de uma OS.'),
                array('Editar Item', 'changelog.item_edit', 'Capacidade de editar conteúdo de versão.')
            ),

            'Cargos/Funções' => array(
                array('Leitura', 'os.item_view', 'Capacidade de visualizar ordem de serviço.'),
                array('Editar Item', 'os.item_edit', 'Capacidade de editar detalhes de uma ordem de serviço.')
            ),

            'Procedimentos' => array(
                array('Leitura', 'os.item_view', 'Capacidade de visualizar ordem de serviço.'),
                array('Editar Item', 'os.item_edit', 'Capacidade de editar detalhes de uma ordem de serviço.')
            ),

            'Exames' => array(
                array('Leitura', 'os.item_view', 'Capacidade de visualizar ordem de serviço.'),
                array('Editar Item', 'os.item_edit', 'Capacidade de editar detalhes de uma ordem de serviço.')
            ),

            'Profissionais' => array(
                array('Leitura', 'os.item_view', 'Capacidade de visualizar ordem de serviço.'),
                array('Editar Item', 'os.item_edit', 'Capacidade de editar detalhes de uma ordem de serviço.')
            )
        );

        foreach ($scopes as $scope => $permissions) {
            $newScope = new PermissionScope;
            $newScope->name = $scope;
            $newScope->save();

            foreach ($permissions as $permission) {
                $newPerm = new Permission;
                $newPerm->scope_id = $newScope->id;
                $newPerm->name = $permission[0];
                $newPerm->tag = $permission[1];
                $newPerm->description = $permission[2];
                $newPerm->save();
            }
        }

        // Atribuímos permissão para Administrador do Sistema
        $group = Role::where('tag', '=', 'sys_admin')->firstOrFail();
        $permission = Permission::where('tag', '=', 'sys_admin')->firstOrFail();
        $group->permissions()->sync(array($permission->id));
    }
}