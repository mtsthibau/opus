<?php

use Illuminate\Database\Migrations\Migration;

class CreateExamTable extends Migration {
     /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exams', function($table){
            $table->increments('id');
            $table->integer('codigo')->nullable();
            $table->string('exam', 128);
            $table->text('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exams'); //se a tabela existir, exclui
    }

}
