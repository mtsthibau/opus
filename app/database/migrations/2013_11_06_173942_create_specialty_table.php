<?php

use Illuminate\Database\Migrations\Migration;

class CreateSpecialtyTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('specialty', function($table){
            $table->increments('id');
            $table->integer('codigo')->nullable();
            $table->string('nome', 128);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('specialty');
    }

}