<?php

use Illuminate\Database\Migrations\Migration;

class CreateCompanieDealing extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('companie_dealing', function($table){
        
            $table->increments('id');
            $table->integer('companie_id')->unsigned()->nullable();
            $table->integer('dealing_id')->unsigned()->nullable();
            $table->decimal('price', 15, 2);
            $table->timestamps();
            
            $table->foreign('companie_id')->references('id')->on('companies');
            $table->foreign('dealing_id')->references('id')->on('dealing');

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('companie_dealing');
	}

}