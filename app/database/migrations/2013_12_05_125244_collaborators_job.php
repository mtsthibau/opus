<?php

use Illuminate\Database\Migrations\Migration;

class CollaboratorsJob extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
			Schema::create('collaborators_job', function($table){
        
            $table->increments('id');
            $table->integer('collaborator_id')->unsigned()->nullable();
            $table->integer('job_id')->unsigned()->nullable();
            $table->string('date', 10);
            $table->timestamps();
            
            $table->foreign('collaborator_id')->references('id')->on('collaborators');
            $table->foreign('job_id')->references('id')->on('jobs');

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('collaborators_job');
	}

}