<?php

use Illuminate\Database\Migrations\Migration;

class CreateDealingTable extends Migration {
     /**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        Schema::create('dealing', function($table){
            $table->increments('id');
            $table->integer('codigo')->nullable();
            $table->string('dealing', 128);
            $table->text('description');
            $table->timestamps();
        });
    }
 
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dealing'); //se a tabela existir, exclui
    }
 
}
