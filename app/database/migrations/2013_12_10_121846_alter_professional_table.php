<?php

use Illuminate\Database\Migrations\Migration;

class AlterProfessionalTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		   Schema::table('professionals', function($table)
        {
            $table->string('telefone')->nullable()->after('numero_conselho');
            $table->string('celular')->nullable()->after('telefone');
            $table->string('email')->nullable()->after('celular');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		 Schema::table('professionals', function($table)
        {
             $table->dropColumn('telefone');
             $table->dropColumn('celular');
             $table->dropColumn('email');        
        });
	}

}