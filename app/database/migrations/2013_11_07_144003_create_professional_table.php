<?php

use Illuminate\Database\Migrations\Migration;


class CreateProfessionalTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('professionals', function($table){
            $table->increments('id');
            $table->integer('codigo')->nullable();
            $table->string('nome', 128);
            $table->string('tipo_conselho', 5);
            $table->string('numero_conselho', 10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('professionals');
    }

}