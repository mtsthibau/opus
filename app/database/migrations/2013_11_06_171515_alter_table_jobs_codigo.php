<?php

use Illuminate\Database\Migrations\Migration;

class AlterTableJobsCodigo extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('jobs', function($table)
        {
            $table->integer('codigo')->nullable()->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jobs', function($table)
        {
            $table->dropColumn('codigo');
        });
    }

}