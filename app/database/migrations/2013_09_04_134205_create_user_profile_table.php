<?php

use Illuminate\Database\Migrations\Migration;

class CreateUserProfileTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create the user profile table
        Schema::create('user_profile', function($table)
        {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('pai_id')->unsigned()->nullable();
            $table->string('fullname', 128)->default('');
            $table->string('avatar', 256)->default('');
            $table->integer('count_login')->default(0);
            $table->integer('count_login_failed')->default(0);
            $table->timestamp('last_login')->default('0000-00-00 00:00:00');
            $table->timestamp('last_login_failed')->default('0000-00-00 00:00:00');
            $table->timestamp('previous_login')->default('0000-00-00 00:00:00');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('pai_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Drop user profile table
        Schema::dropIfExists('user_profile');
    }

}