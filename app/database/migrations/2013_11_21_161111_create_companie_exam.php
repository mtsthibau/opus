<?php

use Illuminate\Database\Migrations\Migration;

class CreateCompanieExam extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('companie_exam', function($table){
        
            $table->increments('id');
            $table->integer('companie_id')->unsigned()->nullable();
            $table->integer('exam_id')->unsigned()->nullable();
            $table->decimal('price', 15, 2);
            $table->timestamps();
            
            $table->foreign('companie_id')->references('id')->on('companies');
            $table->foreign('exam_id')->references('id')->on('exams');

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('companie_exam');
	}

}