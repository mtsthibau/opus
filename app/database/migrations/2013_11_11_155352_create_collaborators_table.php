<?php

use Illuminate\Database\Migrations\Migration;

class CreateCollaboratorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
     Schema::create('collaborators', function($table){
            $table->increments('id');
            $table->integer('codigo')->nullable();
            $table->string('nome', 256);
            $table->string('cpf', 12);
            $table->string('cep', 9);
            $table->string('address', 256);
            $table->string('district', 128);
            $table->string('uf', 2);
            $table->string('number', 16);
            $table->string('city', 128);
            $table->string('phone', 14);
            $table->string('email', 256);
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('collaborators');
	}

}