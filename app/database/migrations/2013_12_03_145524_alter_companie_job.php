<?php

use Illuminate\Database\Migrations\Migration;

class AlterCompanieJob extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('companie_job', function($table){

                $table->increments('id')->before('companie_id');
        });

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
      Schema::table('companie_job', function($table)
        {
            $table->dropColumn('id');
        });
        
	}

}