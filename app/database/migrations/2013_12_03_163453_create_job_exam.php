<?php

use Illuminate\Database\Migrations\Migration;

class CreateJobExam extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('job_exam', function($table){
            
            $table->increments('id');
            $table->integer('job_id')->unsigned()->nullable();
            $table->integer('exam_id')->unsigned()->nullable();
            $table->timestamps();
            
            $table->foreign('job_id')->references('id')->on('jobs');
            $table->foreign('exam_id')->references('id')->on('exams');

        });
        
        Schema::create('job_dealing', function($table){
            
            $table->increments('id');
            $table->integer('job_id')->unsigned()->nullable();
            $table->integer('dealing_id')->unsigned()->nullable();
            $table->timestamps();
            
            $table->foreign('job_id')->references('id')->on('jobs');
            $table->foreign('dealing_id')->references('id')->on('dealing');

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('job_exam');
        Schema::dropIfExists('job_dealing');
	}
    
    

}