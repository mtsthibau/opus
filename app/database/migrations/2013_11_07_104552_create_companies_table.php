<?php

use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		 Schema::create('companies', function($table){
            $table->increments('id');
            $table->integer('codigo')->nullable();
            $table->string('companie', 128);
            $table->string('address', 248);           
            $table->string('cnpj', 18);
            $table->string('city', 128);
            $table->string('uf', 2);
            $table->string('number', 8);
            $table->string('district', 128);//bairro
            $table->string('cep', 9);
            $table->string('phone', 13);
            $table->string('email', 128);
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		 Schema::dropIfExists('companies');
	}

}