<?php

use Illuminate\Database\Migrations\Migration;

class CompaniesCollaboratorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('companie_collaborators', function($table){
            
            $table->increments('id');
            $table->integer('companie_id')->unsigned()->nullable();
            $table->integer('collaborator_id')->unsigned()->nullable();
            $table->string('date', 10);
            $table->timestamps();
            
            $table->foreign('companie_id')->references('id')->on('companies');
            $table->foreign('collaborator_id')->references('id')->on('collaborators');

        });
        
       
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('companie_collaborators');
	}

}