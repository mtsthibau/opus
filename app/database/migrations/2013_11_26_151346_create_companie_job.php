<?php

use Illuminate\Database\Migrations\Migration;

class CreateCompanieJob extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('companie_job', function($table){
        
            $table->integer('companie_id')->unsigned()->nullable();
            $table->integer('job_id')->unsigned()->nullable();
            $table->timestamps();
            
            $table->foreign('companie_id')->references('id')->on('companies');
            $table->foreign('job_id')->references('id')->on('jobs');

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('companie_job');
	}

}